# Python Binding of the Mu Client API

The Python binding allows the client to be written in Python. This binding is
based on the `ctypes` module and is tested on Python 2, Python 3, PyPy and
PyPy3.

**PyPy-on-Mu contributors**: If you are working on the PyPy project and need to
construct Mu IR bundles from the interpreter or the JIT compiler themselves, you
should use the RPython binding provided by
[mu-client-pypy](https://gitlab.anu.edu.au/mu/mu-client-pypy). The RPython
binding can use either the C API or the common instructions counterpart when
appropriate. We are actively working on the PyPy project.

It depends on the C binding. You need to build the C binding in the
[../cbinding](../cbinding) directory before using this Python binding.

# How to use

See the docstring in [libmu.py](libmu.py)

<!--
vim: tw=80 spell
-->
