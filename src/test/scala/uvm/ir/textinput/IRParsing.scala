package uvm.ir.textinput

import uvm.TrantientBundle
import uvm.GlobalBundle
import uvm.utils.IDFactory
import java.io.Reader

trait IRParsing {

  def parseReader(reader: Reader,  globalBundle: GlobalBundle, fac: Option[IDFactory] = None): TrantientBundle = {
    val idf = fac.getOrElse(IDFactory.newClientIDFactory())
    val r = new UIRTextReader(idf)
    val ir = r.read(reader, globalBundle)
    ir
  }
  
  def parseFile(fileName: String, globalBundle: GlobalBundle, fac: Option[IDFactory] = None): TrantientBundle = {
    parseReader(new java.io.FileReader(fileName), globalBundle, fac)
  }

  def parseText(uir: String, globalBundle: GlobalBundle, fac: Option[IDFactory]=None): TrantientBundle = {
    parseReader(new java.io.StringReader(uir), globalBundle, fac)
  }
}