package uvm.refimpl.bootimg

import uvm.refimpl.MicroVM
import uvm.refimpl.VMConf
import uvm.refimpl.UvmBundleTesterBase
import uvm.ir.textinput.ExtraMatchers
import uvm.ir.textinput.TestingBundlesValidators.MagicalOur
import uvm.refimpl.mem.HeaderUtils
import uvm.types.Type
import uvm.refimpl.Word
import uvm.utils.WithUtils._
import uvm.ssavariables.MemoryOrder
import uvm.refimpl.MuUPtrValue
import uvm.refimpl.MuUFPValue

class BootImageWriterTest extends UvmBundleTesterBase with ExtraMatchers {
  preloadBundles("tests/uvm-refimpl-test/transitive-closure.uir")
  preloadHails("tests/uvm-refimpl-test/transitive-closure.hail")

  override def makeMicroVM = MicroVM(new VMConf(automagicReloc = true))
  
  {
    tryWithResource(microVM.newContext()) { ctx => 
      val h_gs = ctx.handleFromGlobal("@gs")
      val h_gs_2 = ctx.getFieldIRef(h_gs, 2)
      val h_gs_2_ptr = ctx.getAddr(h_gs_2)
      val h_gs3 = ctx.handleFromGlobal("@gs3")
      val h_gs3_2 = ctx.getFieldIRef(h_gs3, 2)
      
      ctx.store(MemoryOrder.NOT_ATOMIC, h_gs3_2, h_gs_2_ptr)
    }
  }

  val our = new MagicalOur(microVM.globalBundle)

  behavior of "BootImageWriter"
  
  it should "write everything to files" in {
    val everything = microVM.globalBundle.allTopLevels()
    
    val tcb = new TransitiveClosureBuilder(everything, PrimordialInfo.NO_PRIMORDIAL)(microVM)
    tcb.doTransitiveClosure()

    val biw = new BootImageWriter(tcb, Seq(), Seq(), "target/boot-image-writer-test.muref")(microVM)
    biw.writeFile()
  }
  
  it should "write everything to files and read back" in {
    val filename = "target/boot-image-read-write-test.muref"
    val everything = microVM.globalBundle.allTopLevels()

    tryWithResource(microVM.newContext()) { ctx =>
      val h_gs3 = ctx.handleFromGlobal("@gs3")
      val h_gs3_1 = ctx.getFieldIRef(h_gs3, 1)
      val h_gs3_2 = ctx.getFieldIRef(h_gs3, 2)
      val h_gs3_3 = ctx.getFieldIRef(h_gs3, 3)
      
      val h_gt = ctx.handleFromGlobal("@gt")
      
      val h_tl = ctx.newFixed("@i64")
      val h_tl_i = ctx.getIRef(h_tl)
      val h_fifty_two = ctx.handleFromInt(52, 64)
      ctx.store(MemoryOrder.NOT_ATOMIC, h_tl_i, h_fifty_two)
      
      val h_main = ctx.handleFromFunc("@main")
      
      ctx.makeBootImage(everything.map(_.id), Some(h_main), None, Some(h_tl),
          // manual syms
          Seq(h_gs3_2), Seq("mycustom"),
          // reloc recs
          Seq(h_gs3_3, h_gt), Seq("getchar", "mycustom"),
          filename)
    }
    
    val anotherMicroVM = MicroVM("", Seq("prog", "Hello", "world"))
    anotherMicroVM.loadBootImage(filename)
    
    microVM.globalBundle.allNs("@gd").id shouldEqual anotherMicroVM.globalBundle.allNs("@gd").id

    tryWithResource(anotherMicroVM.newContext()) { ctx => 
      //val h_gr1 = ctx.handleFromGlobal("@gr1")
      //val h_gr1_ptr = ctx.getAddr(h_gr1)

      val h_gs = ctx.handleFromGlobal("@gs")
      val h_gs_2 = ctx.getFieldIRef(h_gs, 2)
      val h_gs_2_ptr = ctx.getAddr(h_gs_2)
      val h_gs3 = ctx.handleFromGlobal("@gs3")
      val h_gs3_1 = ctx.getFieldIRef(h_gs3, 1)
      val h_gs3_1_val = ctx.load(MemoryOrder.NOT_ATOMIC, h_gs3_1).asInstanceOf[MuUPtrValue]
      val h_gs3_2 = ctx.getFieldIRef(h_gs3, 2)
      val h_gs3_2_ptr = ctx.getAddr(h_gs3_2)
      val h_gs3_2_val = ctx.load(MemoryOrder.NOT_ATOMIC, h_gs3_2).asInstanceOf[MuUPtrValue]
      val h_gs3_3 = ctx.getFieldIRef(h_gs3, 3)
      val h_gs3_3_val = ctx.load(MemoryOrder.NOT_ATOMIC, h_gs3_3).asInstanceOf[MuUFPValue]
      val h_gt = ctx.handleFromGlobal("@gt")
      val h_gt_val = ctx.load(MemoryOrder.NOT_ATOMIC, h_gt).asInstanceOf[MuUPtrValue]
      
      //val gr1_ptr = ctx.handleToPtr(h_gr1_ptr)
      val gs_2_ptr = ctx.handleToPtr(h_gs_2_ptr)
      val gs3_1_val = ctx.handleToPtr(h_gs3_1_val)
      val gs3_2_ptr = ctx.handleToPtr(h_gs3_2_ptr)
      val gs3_2_val = ctx.handleToPtr(h_gs3_2_val)
      val gs3_3_val = ctx.handleToFP(h_gs3_3_val)
      val gt_val = ctx.handleToPtr(h_gt_val)
      
      gs3_2_val shouldEqual gs_2_ptr
      
      val getchar_val = anotherMicroVM.nativeLibraryHolder.getSymbolAddress("getchar")
      gs3_3_val shouldEqual getchar_val
      
      gt_val shouldEqual gs3_2_ptr
      
      val allThreads = anotherMicroVM.threadStackManager.threadRegistry.values.toSeq
      allThreads.size shouldBe 1
      allThreads(0).stack.get.frames.toStream(0).curFuncID shouldBe ctx.idOf("@main")
    }
  }
}