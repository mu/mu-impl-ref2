package uvm.refimpl.bootimg

import uvm.refimpl.MicroVM
import uvm.refimpl.VMConf
import uvm.refimpl.UvmBundleTesterBase
import uvm.ir.textinput.ExtraMatchers
import uvm.ir.textinput.TestingBundlesValidators.MagicalOur
import uvm.refimpl.mem.HeaderUtils
import uvm.types.Type
import uvm.refimpl.Word
import uvm.TopLevel
import uvm.refimpl.nat.NativeSupport

class TransitiveClosureTest extends UvmBundleTesterBase with ExtraMatchers {
  preloadBundles("tests/uvm-refimpl-test/transitive-closure.uir")
  preloadHails("tests/uvm-refimpl-test/transitive-closure.hail")

  val our = new MagicalOur(microVM.globalBundle)

  behavior of "TransitiveClosureBuilder"
  
  implicit def nameToTopLevel(name: String): TopLevel = {
    microVM.globalBundle.allNs(name).asInstanceOf[TopLevel]
  }
    
  def makeTCB(whiteList: TopLevel*): TransitiveClosureBuilder = {
    new TransitiveClosureBuilder(whiteList, PrimordialInfo.NO_PRIMORDIAL)(microVM)
  }

  it should "compute transitive closure over types" in {
    val tcb = makeTCB("@refi64")
    tcb.doTransitiveClosure()

    tcb.tls.set should contain(our ty "@i64")
    tcb.tls.set shouldNot contain(our ty "@i32")
  }

  it should "compute transitive closure over function signatures" in {
    val tcb = makeTCB("@s1")
    tcb.doTransitiveClosure()

    tcb.tls.set should contain(our ty "@i32")
    tcb.tls.set should contain(our ty "@i16")
    tcb.tls.set shouldNot contain(our ty "@i64")
  }

  it should "compute transitive closure over constants" in {
    val tcb = makeTCB("@F", "@S")
    tcb.doTransitiveClosure()

    tcb.tls.set should contain(our const "@S0")
    tcb.tls.set should contain(our const "@S1")
    tcb.tls.set should contain(our const "@S2")
    tcb.tls.set should contain(our ty "@i64")
    tcb.tls.set should contain(our ty "@i32")
    tcb.tls.set should contain(our ty "@s")
    tcb.tls.set should contain(our ty "@f")
    tcb.tls.set shouldNot contain(our ty "@d")
  }

  it should "compute transitive closure over global cells" in {
    val tcb = makeTCB("@gfoo")
    tcb.doTransitiveClosure()

    tcb.tls.set should contain(our ty "@foo")
  }

  it should "compute transitive closure over function decls" in {
    val tcb = makeTCB("@fd")
    tcb.doTransitiveClosure()

    tcb.tls.set should contain(our sig "@s1")
    tcb.tls.set should contain(our ty "@i32")
    tcb.tls.set should contain(our ty "@i16")
    tcb.tls.set shouldNot contain(our ty "@i64")
  }

  it should "compute transitive closure over function defs" in {
    val tcb = makeTCB("@gd")
    tcb.doTransitiveClosure()

    tcb.tls.set should contain(our sig "@s2")
    tcb.tls.set should contain(our ty "@i64")
    tcb.tls.set should contain(our ty "@i16")
    tcb.tls.set shouldNot contain(our ty "@i32")
    tcb.tls.set should contain(our const "@S0")
    tcb.tls.set should contain(our const "@S1")
    tcb.tls.set shouldNot contain(our const "@S2")
  }

  it should "compute transitive closure over exposed functions" in {
    val tcb = makeTCB("@ef")
    tcb.doTransitiveClosure()

    tcb.tls.set should contain(our func "@fd")
    tcb.tls.set should contain(our sig "@s1")
    tcb.tls.set should contain(our ty "@i32")
    tcb.tls.set should contain(our ty "@i16")
    tcb.tls.set should contain(our const "@S1")
  }

  def getType(obj: Word): Type = {
    val tag = HeaderUtils.getTag(obj)(microVM.memoryManager.memorySupport)
    val ty = HeaderUtils.getType(microVM, tag)
    ty
  }

  it should "scan the heap for objects" in {
    val tcb = makeTCB("@llhead")
    tcb.doTransitiveClosure()

    tcb.tls.set should contain(our ty "@refll")
    tcb.tls.set should contain(our ty "@ll")
    tcb.tls.set should contain(our ty "@i64")

    val objs = tcb.allocs.set.toSeq
    objs.length shouldBe 4

    for (obj <- objs) {
      val ty = getType(obj)
      Seq(ty) should contain oneOf (our ty "@ll", our ty "@refll")
    }
  }

  it should "scan the types reachable from objects" in {
    val tcb = makeTCB("@llhead_void")
    tcb.doTransitiveClosure()

    tcb.tls.set should contain(our ty "@refll")
    tcb.tls.set should contain(our ty "@ll")
    tcb.tls.set should contain(our ty "@i64")
    tcb.tls.set should contain(our ty "@refvoid")
    tcb.tls.set should contain(our ty "@void")

    val objs = tcb.allocs.set.toSeq
    objs.length shouldBe 4

    for (obj <- objs) {
      val ty = getType(obj)
      Seq(ty) should contain oneOf (our ty "@ll", our ty "@refvoid")
    }
  }

  it should "scan global cells which refer to each other" in {
    val ctx = microVM.newContext()
    val h_gr2 = ctx.handleFromGlobal("@gr2")
    val addr = h_gr2.vb.asIRefLoc
    val content0= NativeSupport.theMemory.getLong(addr)
    val content1= NativeSupport.theMemory.getLong(addr+8)
    ctx.closeContext()
    
    val tcb = makeTCB("@gr2")
    tcb.doTransitiveClosure()

    tcb.tls.set should contain(our ty "@irefi64")
    tcb.tls.set should contain(our ty "@i64")
    tcb.tls.set should contain(our globalCell "@gr1")

    val objs = tcb.allocs.set.toSeq
    objs.length shouldBe 2

    for (obj <- objs) {
      val ty = getType(obj)
      Seq(ty) should contain oneOf (our ty "@i64", our ty "@irefi64")
    }
  }
}