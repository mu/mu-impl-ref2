package uvm.refimpl

import org.scalatest._

import ch.qos.logback.classic.Level._
import uvm._
import uvm.comminsts.CommInsts
import uvm.ir.textinput.ExtraMatchers
import uvm.refimpl._
import uvm.refimpl.RichMuCtx._
import uvm.refimpl.itpr._
import uvm.refimpl.mem._
import uvm.ssavariables._
import uvm.ssavariables.AtomicRMWOptr._
import uvm.ssavariables.MemoryOrder._
import uvm.types._
import scala.collection.mutable.HashMap
import uvm.ir.irbuilder.IRBuilder

object MuCtxIRBuilderTest {
}

class MuCtxIRBuilderTest extends UvmBundleTesterBase with ExtraMatchers {
  import MuCtxIRBuilderTest._

  setLogLevels(ROOT_LOGGER_NAME -> INFO,
    "uvm" -> DEBUG)
    
  behavior of "The IR Builder of MuCtx"

  it should "create an empty bundle" in {
    val ctx = microVM.newContext()
    
    val b = ctx.newIRBuilder()
    b.load()

    ctx.closeContext()
  }

  it should "create a bundle that contains types and sigs" in {
    val ctx = microVM.newContext()

    val b = ctx.newIRBuilder()
    val i1 = b.genSym()
    val i8 = b.genSym(None)
    val i16 = b.genSym()
    val i32 = b.genSym("@b1.i32")
    val i64 = b.genSym(Some("@b1.i64"))
    val f = b.genSym()
    val d = b.genSym()
    val s = b.genSym("@b1.s")
    val h = b.genSym("@b1.h")
    val a = b.genSym("@b1.a")
    val v = b.genSym("@b1.v")
    val sig = b.genSym("@b1.sig")
    val tr = b.genSym("@b1.tr64")
    val st = b.genSym("@b1.stack")
    val th = b.genSym("@b1.thread")
    val fc = b.genSym("@b1.framecursor")
    val ib = b.genSym("@b1.irbuilder")

    b.newTypeInt(i8, 8)
    b.newTypeInt(i16, 16)
    b.newTypeInt(i32, 32)
    b.newTypeInt(i64, 64)

    b.newTypeFloat(f)
    b.newTypeDouble(d)
    b.newTypeStruct(s, Seq(i8, i16, i32, i64))
    b.newTypeHybrid(h, Seq(f, d), i8)
    b.newTypeArray(a, i8, 100)
    b.newTypeVector(v, f, 4)

    b.newFuncSig(sig, Seq(i8, i16), Seq(i32))

    b.newTypeTagRef64(tr)
    b.newTypeStackRef(st)
    b.newTypeThreadRef(th)
    b.newTypeFrameCursorRef(fc)
    b.newTypeIRBuilderRef(ib)
    
    val p = b.genSym("@p")
    val fp = b.genSym("@fp")
    val r = b.genSym()
    val ir = b.genSym()
    val wr = b.genSym()
    val fr = b.genSym()

    b.newTypeUPtr(p, i64)
    b.newTypeUFuncPtr(fp, sig)
    b.newTypeRef(r, i64)
    b.newTypeIRef(ir, i64)
    b.newTypeWeakRef(wr, i64)
    b.newTypeFuncRef(fr, sig)
    
    val r1 = b.genSym()
    val s1 = b.genSym()
    
    b.newTypeRef(r1, s1)
    b.newTypeStruct(s1, Seq(i64, r1))

    b.load()

    val gb = microVM.globalBundle
    
    gb.typeNs(i64) shouldBeA[TypeInt] { its => its.length shouldBe 64 }
    gb.typeNs("@b1.i64") shouldBe gb.typeNs(i64)
    gb.typeNs(r1) shouldBeA[TypeRef] { its => its.ty shouldBe gb.typeNs(s1) }
    gb.typeNs(s1) shouldBeA[TypeStruct] { its => its.fieldTys shouldBe Seq(gb.typeNs(i64), gb.typeNs(r1)) }
    gb.funcSigNs(sig) shouldBeA[FuncSig] { its =>
      its.paramTys shouldBe Seq(gb.typeNs(i8), gb.typeNs(i16))
      its.retTys shouldBe Seq(gb.typeNs(i32))
    }

    ctx.closeContext()
  }

  it should "create a bundle with constants and global cells" in {
    val ctx = microVM.newContext()

    val b = ctx.newIRBuilder()
    
    val i64 = b.genSym()
    val f = b.genSym()
    val d = b.genSym()
    val s = b.genSym()
    val r = b.genSym()
    
    b.newTypeInt(i64, 64)
    b.newTypeFloat(f)
    b.newTypeDouble(d)
    b.newTypeStruct(s, Seq(i64, i64))
    b.newTypeRef(r, i64)
    
    val c1 = b.genSym()
    val c2 = b.genSym()
    val c3 = b.genSym()
    val c4 = b.genSym()
    val c5 = b.genSym()
    val c6 = b.genSym()
    
    b.newConstInt(c1, i64, 0x123456789abcdef0L)
    b.newConstInt(c2, i64, 0xfedcba9876543210L)
    b.newConstSeq(c3, s, Seq(c1, c2))
    b.newConstFloat(c4, f, 3.14F)
    b.newConstDouble(c5, d, 3.14)
    b.newConstNull(c6, r)
    
    val g = b.genSym()
    
    b.newGlobalCell(g, i64)

    b.load()
    
    val gb = microVM.globalBundle
    gb.constantNs(c1) shouldBeA[ConstInt] { its => its.num shouldBe 0x123456789abcdef0L }
    gb.constantNs(c2) shouldBeA[ConstInt] { its => its.num shouldBe 0xfedcba9876543210L }
    gb.constantNs(c3) shouldBeA[ConstSeq] { its =>
      its.elems.map(_.asInstanceOf[ConstInt].num) shouldBe Seq(0x123456789abcdef0L, 0xfedcba9876543210L)
    }
    gb.constantNs(c4) shouldBeA[ConstFloat] { its => its.num shouldBe 3.14f }
    gb.constantNs(c5) shouldBeA[ConstDouble] { its => its.num shouldBe 3.14 }
    gb.constantNs(c6) shouldBeA[ConstNull] thatsIt
    
    gb.globalCellNs(g) shouldBeA[GlobalCell] { its => its.cellTy shouldBeA[TypeInt] { itss => itss.length shouldBe 64 }}
    
    val hc1v = ctx.handleFromConst(c1).asInstanceOf[MuIntValue]
    val hc1vv = ctx.handleToSInt(hc1v)
    hc1vv shouldBe 0x123456789abcdef0L
    
    val hirg = ctx.handleFromGlobal(g)
    val hirg_v0 = ctx.load(NOT_ATOMIC, hirg).asInstanceOf[MuIntValue]
    val hirg_v0v = ctx.handleToSInt(hirg_v0)
    hirg_v0v shouldBe 0

    ctx.store(NOT_ATOMIC, hirg, hc1v) 

    val hirg_v1 = ctx.load(NOT_ATOMIC, hirg).asInstanceOf[MuIntValue]
    val hirg_v1v = ctx.handleToSInt(hirg_v1)
    hirg_v1v shouldBe 0x123456789abcdef0L

    ctx.closeContext()
  }

  it should "create a bundle with functions and can execute the function" in {
    val ctx = microVM.newContext()

    val b = ctx.newIRBuilder()
    val i1 = b.genSym()
    val i8 = b.genSym(None)
    val i16 = b.genSym()
    val i32 = b.genSym("@b2.i32")
    val i64 = b.genSym(Some("@b2.i64"))
    val f = b.genSym()
    val d = b.genSym()
    val s = b.genSym("@b2.s")
    val h = b.genSym("@b2.h")
    val a = b.genSym("@b2.a")
    val v = b.genSym("@b2.v")
    val tr = b.genSym("@b2.tr64")
    val st = b.genSym("@b2.stack")
    val th = b.genSym("@b2.thread")
    val fc = b.genSym("@b2.framecursor")
    val r = b.genSym("@b2.r")

    b.newTypeInt(i8, 8)
    b.newTypeInt(i16, 16)
    b.newTypeInt(i32, 32)
    b.newTypeInt(i64, 64)

    b.newTypeFloat(f)
    b.newTypeDouble(d)
    b.newTypeStruct(s, Seq(i8, i16, i32, i64))
    b.newTypeHybrid(h, Seq(f, d), i8)
    b.newTypeArray(a, i8, 100)
    b.newTypeVector(v, f, 4)

    b.newTypeTagRef64(tr)
    b.newTypeStackRef(st)
    b.newTypeThreadRef(th)
    b.newTypeFrameCursorRef(fc)
    b.newTypeRef(r, i64)
    
    val sig = b.genSym("@b2.sig")
    val func = b.genSym("@b2.func")
    val fv = b.genSym("@fv")

    val entry = b.genSym("@entry")
    val bb1 = b.genSym("@bb1")
    val bb2 = b.genSym("@bb2")
    
    b.newFuncSig(sig, Seq(i64, i64, d, d), Seq())
    b.newFunc(func, sig)
    b.newFuncVer(fv, func, Seq(entry, bb1, bb2))
    
    val p0 = b.genSym("@p0")
    val p1 = b.genSym("@p1")
    val p2 = b.genSym("@p2")
    val p3 = b.genSym("@p3")
    
    val add = b.genSym("@add")
    val x = b.genSym("@x")
    val fadd = b.genSym("@b2.fadd")
    val y = b.genSym("@y")
    val slt = b.genSym("@slt")
    val z = b.genSym("@z")
    val br2 = b.genSym("@br2")

    b.newBB(entry, Seq(p0, p1, p2, p3), Seq(i64, i64, d, d), None, Seq(add, fadd, slt, br2))
    
    b.newBinOp(add, x, BinOptr.ADD, i64, p0, p1, None)
    b.newBinOp(fadd, y, BinOptr.FADD, d, p2, p3, None)
    b.newCmp(slt, z, CmpOptr.SLT, i64, p0, x)
    
    val dest1 = b.genSym()
    val dest2 = b.genSym()
    
    b.newDestClause(dest1, bb1, Seq(x, y))
    b.newDestClause(dest2, bb2, Seq())
    b.newBranch2(br2, z, dest1, dest2)
    
    val bb1p0 = b.genSym("@bb1p0")
    val bb1p1 = b.genSym("@bb1p1")
    
    val trap1 = b.genSym("@trap1")
    val threadexit1 = b.genSym("@threadexit1")

    b.newBB(bb1, Seq(bb1p0, bb1p1), Seq(i64, d), None, Seq(trap1, threadexit1))

    val kas1 = b.genSym()
    b.newKeepaliveClause(kas1, Seq(bb1p0, bb1p1))
    b.newTrap(trap1, Seq(), Seq(), None, Some(kas1))
    b.newCommInst(threadexit1, Seq(), CommInsts("@uvm.thread_exit"), Seq(), Seq(), Seq(), Seq(), None, None)

    
    val trap2 = b.genSym("@trap2")
    val threadexit2 = b.genSym("@threadexit2")

    b.newBB(bb2, Seq(), Seq(), None, Seq(trap2, threadexit2))

    b.newTrap(trap2, Seq(), Seq(), None, None)
    b.newCommInst(threadexit2, Seq(), CommInsts("@uvm.thread_exit"), Seq(), Seq(), Seq(), Seq(), None, None)
    
    b.load()
    
    val hfunc = ctx.handleFromFunc(func)
    val args = Seq(
        ctx.handleFromInt(100, 64),
        ctx.handleFromInt(200, 64),
        ctx.handleFromDouble(3.5),
        ctx.handleFromDouble(4.5)
        )
        
    var trap1Reached = false
    var trap2Reached = false
    
    testFunc(ctx, hfunc, args, None) { (ctx, th, st, wpid) =>
      val cursor = ctx.newCursor(st)
      val cf = ctx.curFunc(cursor)
      val cfv = ctx.curFuncVer(cursor)
      val ci = ctx.curInst(cursor)
      ci match {
        case `trap1` => {
          trap1Reached = true
          
          val Seq(hv0: MuIntValue, hv1: MuDoubleValue) = ctx.dumpKeepalives(cursor)
          ctx.closeCursor(cursor)
          
          ctx.handleToSInt(hv0) shouldBe 300
          ctx.handleToDouble(hv1) shouldBe 8.0
        }
        case `trap2` => {
          trap2Reached = true
        }
      }
      TrapHandlerResult.ThreadExit()
    }
    
    trap1Reached shouldBe true
    trap2Reached shouldBe false

    ctx.closeContext()
  }
  
  class ConvenientNs(b: IRBuilder, prefix: String) {
    val map = new HashMap[Symbol, MuID]()
    
    def muNameOf(sym: Symbol): MuName = "@" + prefix + "." + sym.name
    
    def symLookup(sym: Symbol): MuID = {
      map.getOrElseUpdate(sym, {
        b.genSym(muNameOf(sym))
      })
    }
  }

  it should "create multiple functions and call each other" in {
    val ctx = microVM.newContext()

    val b = ctx.newIRBuilder()
    
    val ns = new ConvenientNs(b, "calleachother")
    implicit def _lookup = ns.symLookup _
    
    b.newTypeInt('i64, 64)
    b.newFuncSig('sig1, Seq(), Seq())
    b.newFuncSig('sig2, Seq('i64), Seq('i64))
    
    b.newFunc('func1, 'sig1)
    b.newFunc('func2, 'sig2)

    b.newConstInt('ci5, 'i64, 5L)

    b.newFuncVer('func1_v1, 'func1, Seq('f1entry))
    
    b.newBB('f1entry, Seq(), Seq(), None, Seq('call, 'trap, 'threadexit))
    
    b.newCall('call, Seq('res), 'sig2, 'func2, Seq('ci5), None, None)
    b.newTrap('trap, Seq(), Seq(), None, Some('trap_ka))
    b.newKeepaliveClause('trap_ka, Seq('res))
  
    b.newCommInst('threadexit, Seq(), CommInsts("@uvm.thread_exit"), Seq(), Seq(), Seq(), Seq(), None, None)

    b.newFuncVer('func2_v1, 'func2, Seq('f2entry, 'f2exit, 'f2rec))

    {
      b.newBB('f2entry, Seq('p0), Seq('i64), None, Seq('eq0, 'br2))
      
      b.newConstInt('const0, 'i64, 0)
      b.newCmp('eq0, 'eq0_r, CmpOptr.EQ, 'i64, 'p0, 'const0)
      b.newBranch2('br2, 'eq0_r, 'br2_iftrue, 'br2_iffalse)
      b.newDestClause('br2_iftrue, 'f2exit, Seq())
      b.newDestClause('br2_iffalse, 'f2rec, Seq('p0))
      
      b.newBB('f2exit, Seq(), Seq(), None, Seq('ret1))
      
      b.newConstInt('const1, 'i64, 1)
      b.newRet('ret1, Seq('const1))

      b.newBB('f2rec, Seq('rp0), Seq('i64), None, Seq('sub, 'reccall, 'mul, 'ret2))
      
      b.newBinOp('sub, 'sub_r, BinOptr.SUB, 'i64, 'rp0, 'const1, None)
      b.newCall('reccall, Seq('reccall_r), 'sig2, 'func2, Seq('sub_r), None, None)
      b.newBinOp('mul, 'mul_r, BinOptr.MUL, 'i64, 'reccall_r, 'rp0, None)
      b.newRet('ret2, Seq('mul_r))
    }
    
    b.load()
    
    val func = ctx.handleFromFunc('func1)
    val args = Seq()
    
    val trap_id = _lookup('trap)
    
    testFunc(ctx, func, args, None) { (ctx, th, st, wpid) =>
      val cursor = ctx.newCursor(st)
      val cf = ctx.curFunc(cursor)
      val cfv = ctx.curFuncVer(cursor)
      val ci = ctx.curInst(cursor)
      ci match {
        case `trap_id` => {
          
          val Seq(hres: MuIntValue) = ctx.dumpKeepalives(cursor)
          ctx.closeCursor(cursor)
          
          ctx.handleToSInt(hres) shouldBe 120
        }
      }
      TrapHandlerResult.Rebind(st, HowToResume.PassValues(Seq()))

    }
    
    ctx.closeContext()
  }
  
  
  it should "create more instructions" in {
    val ctx = microVM.newContext()

    val b = ctx.newIRBuilder()
    val ns = new ConvenientNs(b, "createmoreinsts")
    implicit def _lookup = ns.symLookup _
    
    b.newTypeInt('i1, 1)
    b.newTypeInt('i8, 8)
    b.newTypeInt('i64, 64)
    b.newTypeFloat('f)
    b.newTypeVector('f4, 'f, 4)
    
    b.newConstFloat('cf0, 'f, 0.0f)
    b.newConstFloat('cf1, 'f, 1.0f)
    b.newConstFloat('cf2, 'f, 2.0f)
    b.newConstFloat('cf3, 'f, 3.0f)
    b.newConstSeq('cv0, 'f4, Seq('cf0, 'cf0, 'cf1, 'cf1))
    b.newConstSeq('cv1, 'f4, Seq('cf2, 'cf3, 'cf2, 'cf3))
    
    b.newFuncSig('sig, Seq('f4, 'f4), Seq())
    b.newFunc('func, 'sig)
    b.newFuncVer('fv, 'func, Seq('entry))
    b.newBB('entry, Seq('p0, 'p1), Seq('f4, 'f4), None, Seq('add, 'p00, 'p01, 'folt, 'sel, 'ev, 'iv, 'new, 'newhybrid,
        'alloca, 'allocahybrid, 'getiref, 'getfieldiref, 'alloca2, 'getelemiref, 'shiftiref, 'getvarpartiref, 'load, 'store,
        'cmpxchg, 'atomicrmw, 'fence, 'newstack, 'newthread, 'trap, 'threadexit))
    b.newBinOp('add, 'add_r, BinOptr.FADD, 'f4, 'p0, 'p1, None)
    
    b.newConstInt('const0, 'i64, 0)
    b.newConstInt('const1, 'i64, 1)
    b.newExtractElement('p00, 'p00_r, 'f4, 'i64, 'p0, 'const0)
    b.newExtractElement('p01, 'p01_r, 'f4, 'i64, 'p0, 'const1)
    b.newCmp('folt, 'folt_r, CmpOptr.FOLT, 'f, 'p00_r, 'p01_r)
    b.newSelect('sel, 'sel_r, 'i1, 'f, 'folt_r, 'p00_r, 'p01_r)
    
    b.newTypeStruct('ts, Seq('i64, 'f))
    b.newConstFloat('constf1, 'f, 1.0f)

    b.newConstSeq('consts, 'ts, Seq('const1, 'constf1))
    
    b.newExtractValue('ev, 'ev_r, 'ts, 0, 'consts)
    b.newInsertValue('iv, 'iv_r, 'ts, 0, 'consts, 'const0)
    
    b.newNew('new, 'new_r, 'i64 , None)
    
    b.newTypeHybrid('th, Seq('i64), 'i8)
    b.newNewHybrid('newhybrid, 'newhybrid_r, 'th, 'i64, 'const1, None)
    
    b.newAlloca('alloca, 'alloca_r, 'i64, None)
    b.newAllocaHybrid('allocahybrid, 'allocahybrid_r, 'th, 'i64, 'const1, None)
    
    b.newGetIRef('getiref, 'getiref_r, 'i64, 'new_r)
    b.newGetFieldIRef('getfieldiref, 'getfieldiref_r, false, 'th, 0, 'allocahybrid_r)
    
    b.newTypeArray('ta, 'i64, 10)
    b.newAlloca('alloca2, 'alloca2_r, 'ta, None)
    
    b.newGetElemIRef('getelemiref, 'getelemiref_r, false, 'ta, 'i64, 'alloca2_r, 'const0)
    b.newShiftIRef('shiftiref, 'shiftiref_r, false, 'i64, 'i64, 'getelemiref_r, 'const1)
    b.newGetVarPartIRef('getvarpartiref, 'getvarpartiref_r, false, 'th, 'getiref_r)
    
    b.newLoad('load, 'load_r, false, MemoryOrder.SEQ_CST, 'i64, 'alloca_r, None)
    b.newStore('store, false, MemoryOrder.SEQ_CST, 'i64, 'alloca_r, 'const1, None)
    
    b.newCmpXchg('cmpxchg, 'cmpxchg_r, 'cmpxchg_s, false, false, MemoryOrder.SEQ_CST, MemoryOrder.SEQ_CST,
        'i64, 'alloca_r, 'const1, 'const0, None)
        
    b.newAtomicRMW('atomicrmw, 'atomicrmw_r, false, MemoryOrder.SEQ_CST, AtomicRMWOptr.ADD,
        'i64, 'alloca_r, 'const1, None)

    b.newFence('fence, MemoryOrder.SEQ_CST)
    
    b.newCommInst('newstack, Seq('newstack_r), CommInsts("@uvm.new_stack"), Seq(), Seq(), Seq('sigvv), Seq('dumbfunc), None, None)
    b.newNewThread('newthread, 'newthread_r, 'newstack_r, None, 'newthread_nsc, None)
    b.newNscPassValues('newthread_nsc, Seq(), Seq())
    
    b.newTrap('trap, Seq(), Seq(), None, None)

    b.newCommInst('threadexit, Seq(), CommInsts("@uvm.thread_exit"), Seq(), Seq(), Seq(), Seq(), None, None)
    
    b.newFuncSig('sigvv, Seq(), Seq())
    b.newFunc('dumbfunc, 'sigvv)
    b.newFuncVer('dumbfv, 'dumbfunc, Seq('dumbentry))
    b.newBB('dumbentry, Seq(), Seq(), None, Seq('dumbthreadexit))
    b.newCommInst('dumbthreadexit, Seq(), CommInsts("@uvm.thread_exit"), Seq(), Seq(), Seq(), Seq(), None, None)
    
    b.load()
    
    val func = ctx.handleFromFunc('func)
    val a0 = ctx.handleFromConst('cv0)
    val a1 = ctx.handleFromConst('cv0)

    testFunc(ctx, func, Seq(a0, a1), None) { (ctx, th, st, wpid) =>
      TrapHandlerResult.Rebind(st, HowToResume.PassValues(Seq()))
    }
    
    ctx.closeContext()
  }
  
  it should "create instructions with ExcClause" in {
    val ctx = microVM.newContext()

    val b = ctx.newIRBuilder()
    val ns = new ConvenientNs(b, "createexcclause")
    implicit def _lookup = ns.symLookup _
    
    b.newTypeInt('i32, 32)
    
    b.newFuncSig('main_sig, Seq('i32, 'i32), Seq())
    
    b.newFunc('main, 'main_sig)
    
    b.newFuncVer('main_v1, 'main, Seq('entry, 'bb1, 'bb2))
    
    b.newBB('entry, Seq('x, 'y), Seq('i32, 'i32), None, Seq('div))
    b.newBinOp('div, 'z, BinOptr.SDIV, 'i32, 'x, 'y, Some('excClause))
    b.newExcClause('excClause, 'norDest, 'excDest)
    b.newDestClause('norDest, 'bb1, Seq('x, 'y, 'z))
    b.newDestClause('excDest, 'bb2, Seq())
    
    b.newBB('bb1, Seq('x1, 'y1, 'z1), Seq('i32, 'i32, 'i32), None, Seq('threadexit1))
    b.newCommInst('threadexit1, Seq(), CommInsts("@uvm.thread_exit"), Seq(), Seq(), Seq(), Seq(), None, None)
    
    b.newBB('bb2, Seq(), Seq(), None, Seq('threadexit2))
    b.newCommInst('threadexit2, Seq(), CommInsts("@uvm.thread_exit"), Seq(), Seq(), Seq(), Seq(), None, None)
    
    b.load()

    ctx.close()
  }

  it should "create functions exposed functions" in {
    val ctx = microVM.newContext()

    val b = ctx.newIRBuilder()
    
    val ns = new ConvenientNs(b, "funcsandexpfuncs")
    implicit def _lookup = ns.symLookup _
    
    b.newTypeInt('i64, 64)
    b.newFuncSig('sig1, Seq(), Seq())
    b.newConstInt('i64_0, 'i64, 0L)
    
    b.newExpFunc('expfunc1, 'func1, Flag("#DEFAULT"), 'i64_0)
    b.newFunc('func1, 'sig1)

    b.newFunc('func2, 'sig1)
    b.newExpFunc('expfunc2, 'func2, Flag("#DEFAULT"), 'i64_0)
    
    b.load()
    
    ctx.closeContext()
  }
}