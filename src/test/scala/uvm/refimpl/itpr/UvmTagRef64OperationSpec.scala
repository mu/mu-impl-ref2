package uvm.refimpl.itpr

import java.lang.Double.{ doubleToRawLongBits, isNaN, longBitsToDouble }

import org.scalatest._

import uvm.UvmTestBase
import uvm.refimpl.itpr._

class UvmTagRef64OperationSpec extends UvmTestBase {
  behavior of "TagRef64 operations"

  it should "treat a NaN with suffix 1 as an integer." in {
    OpHelper.tr64IsInt(0x7ff0000000000001L) shouldBe true
    OpHelper.tr64IsInt(0xfff0000000000001L) shouldBe true
    OpHelper.tr64IsInt(0xffffffffffffffffL) shouldBe true
  }

  it should "treat a NaN with suffix 10 as a reference." in {
    OpHelper.tr64IsRef(0x7ff0000000000002L) shouldBe true
    OpHelper.tr64IsRef(0xfff0000000000002L) shouldBe true
    OpHelper.tr64IsRef(0xfffffffffffffffeL) shouldBe true
  }

  it should "treat other bit patterns as double" in {
    OpHelper.tr64IsFP(0x0L) shouldBe true
    OpHelper.tr64IsFP(0x123456789abcdef0L) shouldBe true
    OpHelper.tr64IsFP(0x7ff123456789abccL) shouldBe true
    OpHelper.tr64IsFP(0xfffffffffffffffcL) shouldBe true
    OpHelper.tr64IsFP(doubleToRawLongBits(3.1415927)) shouldBe true
  }

  it should "encode integers" in {
    OpHelper.intToTr64(0x0000000000000L) shouldBe 0x7ff0000000000001L
    OpHelper.intToTr64(0xfffffffffffffL) shouldBe 0xffffffffffffffffL
    OpHelper.intToTr64(0x5555555555555L) shouldBe 0x7ffaaaaaaaaaaaabL
    OpHelper.intToTr64(0xaaaaaaaaaaaaaL) shouldBe 0xfff5555555555555L
  }

  it should "encode double" in {
    OpHelper.fpToTr64(3.14) shouldBe java.lang.Double.doubleToRawLongBits(3.14)
    OpHelper.fpToTr64(-3.14) shouldBe java.lang.Double.doubleToRawLongBits(-3.14)
    OpHelper.fpToTr64(java.lang.Double.POSITIVE_INFINITY) shouldBe 0x7ff0000000000000L
    OpHelper.fpToTr64(longBitsToDouble(0x7ff123456789abcdL)) shouldBe 0x7ff0000000000008L
    isNaN(longBitsToDouble(OpHelper.fpToTr64(longBitsToDouble(0x7ff123456789abcdL)))) shouldBe true
  }

  it should "encode ref and tag" in {
    OpHelper.refToTr64(0x000000000000L, 0x00L) shouldBe 0x7ff0000000000002L
    OpHelper.refToTr64(0x7ffffffffff8L, 0x00L) shouldBe 0x7ff07ffffffffffaL
    OpHelper.refToTr64(0xfffffffffff8L, 0x00L) shouldBe 0x7ff0fffffffffffaL
    OpHelper.refToTr64(0x000000000000L, 0x3fL) shouldBe 0xffff000000000006L
  }

  it should "decode integer" in {
    OpHelper.tr64ToInt(0x7ff0000000000001L) shouldBe 0
    OpHelper.tr64ToInt(0xfff0000000000001L) shouldBe 0x8000000000000L
    OpHelper.tr64ToInt(0xfff5555555555555L) shouldBe 0xaaaaaaaaaaaaaL
    OpHelper.tr64ToInt(0x7ffaaaaaaaaaaaabL) shouldBe 0x5555555555555L
  }

  it should "decode double" in {
    OpHelper.tr64ToFP(0x0000000000000000L) shouldBe +0.0
    OpHelper.tr64ToFP(0x8000000000000000L) shouldBe -0.0
    OpHelper.tr64ToFP(0x3ff0000000000000L) shouldBe 1.0
    isNaN(OpHelper.tr64ToFP(0x7ff0000000000008L)) shouldBe true
  }
  
  it should "decodde ref and tag" in {
    OpHelper.tr64ToRef(0x7ff0555555555552L) shouldBe 0x555555555550L
    OpHelper.tr64ToRef(0x7ff0aaaaaaaaaaaaL) shouldBe 0xaaaaaaaaaaa8L
    OpHelper.tr64ToRef(0x7ff0fffffffffffaL) shouldBe 0xfffffffffff8L
    OpHelper.tr64ToTag(0x7ff0555555555552L) shouldBe 0
    OpHelper.tr64ToTag(0xffff000000000006L) shouldBe 0x3f
    OpHelper.tr64ToTag(0xfff0000000000002L) shouldBe 0x20
    OpHelper.tr64ToTag(0x7fff000000000002L) shouldBe 0x1e
    OpHelper.tr64ToTag(0x7ff0000000000006L) shouldBe 0x01
  }
}
