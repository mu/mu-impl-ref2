package uvm.refimpl.itpr

import org.scalatest._

import ch.qos.logback.classic.Level._
import uvm._
import uvm.refimpl._
import uvm.refimpl.UvmBundleTesterBase
import uvm.refimpl.itpr._
import uvm.ssavariables._
import uvm.types._

class UvmInterpreterProprietaryTests extends UvmBundleTesterBase {
  setLogLevels(
    ROOT_LOGGER_NAME -> INFO,
    "uvm.refimpl.itpr" -> DEBUG)

  preloadBundles("tests/uvm-refimpl-test/primitives.uir",
    "tests/uvm-refimpl-test/proprietary-tests.uir")

  "The micro VM" should "print statistics when requested" in {
    val ctx = microVM.newContext()

    val func = ctx.handleFromFunc("@stats_test")
    
    val arg1 = ctx.handleFromInt(1, 64)
    val arg2 = ctx.handleFromInt(2, 64)

    testFunc(ctx, func, Seq(arg1, arg2)) { (ctx, th, st, wp) =>
      returnFromTrap(st)
    }

    ctx.closeContext()
  }
}