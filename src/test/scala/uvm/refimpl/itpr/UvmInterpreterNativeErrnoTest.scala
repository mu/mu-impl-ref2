package uvm.refimpl.itpr

import org.scalatest._
import java.io.FileReader
import uvm._
import uvm.types._
import uvm.ssavariables._
import uvm.refimpl._
import uvm.refimpl.itpr._
import MemoryOrder._
import AtomicRMWOptr._
import uvm.refimpl.Word
import ch.qos.logback.classic.Level._
import uvm.refimpl.UvmBundleTesterBase
import com.kenai.jffi.Library
import jnr.posix.POSIXFactory
import uvm.refimpl.nat.NativeLibraryTestHelper

class UvmInterpreterNativeErrnoTest extends UvmBundleTesterBase {
  setLogLevels(
    ROOT_LOGGER_NAME -> INFO,
    "uvm.refimpl.itpr" -> DEBUG,
    "uvm.refimpl.nat" -> DEBUG
    )

  NativeLibraryTestHelper.loadTestLibraryToMicroVM(microVM, "simplecfuncs")
  preloadBundles("tests/uvm-refimpl-test/primitives.uir", "tests/uvm-refimpl-test/native-errno-test.uir")

  "The CCALL instruction" should "save errno to thread's errno box" in {
    val ctx = microVM.newContext()

    val func = ctx.handleFromFunc("@errno_test")
    val a0 = ctx.handleFromInt(42, 32)

    testFunc(ctx, func, Seq(a0)) { (ctx, th, st, wp) =>
      val Seq(rv: MuIntValue) = ctx.dumpKeepalives(st, 0)

      ctx.handleToSInt(rv) shouldEqual 42

      returnFromTrap(st)
    }

    ctx.closeContext()
  }
}