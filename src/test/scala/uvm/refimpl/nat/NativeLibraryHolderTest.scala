package uvm.refimpl.nat

import uvm._
import uvm.refimpl._
import uvm.UvmTestBase
import uvm.ir.textinput.ExtraMatchers
import uvm.refimpl.UvmBundleTesterBase
import uvm.ssavariables.MemoryOrder
import uvm.comminsts.CommInsts
import uvm.ssavariables.ConvOptr
import uvm.ssavariables.Flag
import jnr.posix.POSIX
import jnr.posix.POSIXFactory
import uvm.refimpl.itpr.MemoryOperations
import java.nio.charset.StandardCharsets
import uvm.refimpl.HowToResume
import java.io.StringWriter
import java.io.OutputStreamWriter
import uvm.ir.irbuilder.IRBuilder
import scala.collection.mutable.HashMap
import uvm.utils.WithUtils.tryWithResource
import uvm.refimpl.TrapHandlerResult.Rebind
import uvm.refimpl.HowToResume.PassValues

class NativeLibraryHolderTest extends UvmBundleTesterBase {
  behavior of "NativeLibraryHolder"

  class ConvenientNs(b: IRBuilder, prefix: String) {
    val map = new HashMap[Symbol, MuID]()

    def muNameOf(sym: Symbol): MuName = "@" + prefix + "." + sym.name

    def symLookup(sym: Symbol): MuID = {
      map.getOrElseUpdate(sym, {
        b.genSym(muNameOf(sym))
      })
    }
  }

  microVM.nativeLibraryHolder.loadLibrary("tests/c-snippets/simplecfuncs.so")

  it should "link EXTERN consts with C symbols" in {
    tryWithResource(microVM.newContext()) { ctx =>
      val b = ctx.newIRBuilder

      val ns = new ConvenientNs(b, "hello-world-test")
      implicit def _lookup = ns.symLookup _

      b.newTypeInt('i32, 32)

      b.newFuncSig('hwsig, Seq('i32), Seq('i32))
      b.newTypeUFuncPtr('hwfp, 'hwsig)
      b.newConstExtern('hw, 'hwfp, "hello_world")

      b.newConstInt('fortytwo, 'i32, 42)

      b.newFuncSig('mainsig, Seq(), Seq())
      b.newFunc('main, 'mainsig)

      b.newFuncVer('mainv1, 'main, Seq('entry))
      b.newBB('entry, Seq(), Seq(), None, Seq('ccall, 'trap, 'threadexit))
      b.newCCall('ccall, Seq('rv), Flag("#DEFAULT"), 'hwfp, 'hwsig, 'hw, Seq('fortytwo), None, None)
      b.newTrap('trap, Seq(), Seq(), None, Some('trapka))
      b.newKeepaliveClause('trapka, Seq('rv))
      b.newCommInst('threadexit, Seq(), CommInsts("@uvm.thread_exit"), Seq(), Seq(), Seq(), Seq(), None, None)

      b.load()

      System.err.print("Bundle dump:\n")
      microVM.debugPrintGlobalBundle(new OutputStreamWriter(System.err))

      val func = ctx.handleFromFunc('main)

      val TrapID = ns.symLookup('trap)

      testFunc(ctx, func, Seq()) { (ctx, th, st, wp) =>
        val cursor = ctx.newCursor(st)
        val iid = ctx.curInst(cursor)
        iid match {
          case TrapID => {
            val Seq(rv: MuIntValue) = ctx.dumpKeepalives(cursor)
            ctx.closeCursor(cursor)
            ctx.handleToSInt(rv) shouldBe 43
            Rebind(st, PassValues(Seq()))
          }
          case _ => fail("Unexpected trap %d".format(iid))
        }
      }
    }
  }
}