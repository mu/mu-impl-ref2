
package object uvm {
  type MuID = Identified.MuID
  type MuName = Identified.MuName
}