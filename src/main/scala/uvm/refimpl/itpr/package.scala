package uvm.refimpl

import uvm.Function
import uvm.ir.irbuilder.IRBuilder

package object itpr {

  type BoxFunc = BoxOpaque[Function]
  type BoxThread = BoxOpaque[InterpreterThread]
  type BoxStack = BoxOpaque[InterpreterStack]
  type BoxFrameCursor = BoxOpaque[FrameCursor]
  type BoxIRBuilder = BoxOpaque[IRBuilder]

  def BoxFunc = BoxOpaque.apply[Function] _
  def BoxThread = BoxOpaque.apply[InterpreterThread] _
  def BoxStack = BoxOpaque.apply[InterpreterStack] _
  def BoxFrameCursor = BoxOpaque.apply[FrameCursor] _
  def BoxIRBuilder = BoxOpaque.apply[IRBuilder] _

  implicit class MagicalBox(val box: ValueBox) extends AnyVal {
    def asIntRaw: BigInt = box.asInstanceOf[BoxInt].value
    def asUInt(l: Int): BigInt = OpHelper.prepareUnsigned(box.asIntRaw, l)
    def asSInt(l: Int): BigInt = OpHelper.prepareSigned(box.asIntRaw, l)
    def asInt32: BigInt = asUInt(32)
    def asInt64: BigInt = asUInt(64)
    def asUInt32: BigInt = asUInt(32)
    def asUInt64: BigInt = asUInt(64)
    def asSInt32: BigInt = asSInt(32)
    def asSInt64: BigInt = asSInt(64)
    def asInt1: BigInt = OpHelper.prepareUnsigned(box.asIntRaw, 1)
    def asBoolean: Boolean = OpHelper.prepareUnsigned(box.asIntRaw, 1) == 1
    def asFloat: Float = box.asInstanceOf[BoxFloat].value
    def asDouble: Double = box.asInstanceOf[BoxDouble].value
    def asPtr: Word = box.asInstanceOf[BoxPointer].addr
    def asSeq: Seq[ValueBox] = box.asInstanceOf[BoxSeq].values
    def asRef: Word = box.asInstanceOf[BoxRef].objRef
    def asIRef: (Word, Word) = box.asInstanceOf[BoxIRef].oo
    def asIRefLoc: Word = box.asInstanceOf[BoxIRef].addr
    def asTR64Raw: Long = box.asInstanceOf[BoxTagRef64].raw
    def asFunc: Option[Function] = box.asInstanceOf[BoxFunc].obj
    def asThread: Option[InterpreterThread] = box.asInstanceOf[BoxThread].obj
    def asStack: Option[InterpreterStack] = box.asInstanceOf[BoxStack].obj
    def asFrameCursor: Option[FrameCursor] = box.asInstanceOf[BoxFrameCursor].obj
    def asIRBuilder: Option[IRBuilder] = box.asInstanceOf[BoxIRBuilder].obj

    def asIntRaw_=(v: BigInt): Unit = box.asInstanceOf[BoxInt].value = v
    def asInt1_=(v: BigInt): Unit = box.setInt(v, 1)
    def asInt32_=(v: BigInt): Unit = box.setInt(v, 32)
    def asInt64_=(v: BigInt): Unit = box.setInt(v, 64)
    def asUInt32_=(v: BigInt): Unit = box.setInt(v, 32)
    def asUInt64_=(v: BigInt): Unit = box.setInt(v, 64)
    def asSInt32_=(v: BigInt): Unit = box.setInt(v, 32)
    def asSInt64_=(v: BigInt): Unit = box.setInt(v, 64)
    def asBoolean_=(v: Boolean): Unit = box.setInt(if (v) 1 else 0, 1)
    def asFloat_=(v: Float): Unit = box.asInstanceOf[BoxFloat].value = v
    def asDouble_=(v: Double): Unit = box.asInstanceOf[BoxDouble].value = v
    def asPtr_=(v: Word): Unit = box.asInstanceOf[BoxPointer].addr = v
    def asSeq_=(vs: Seq[ValueBox]): Unit = { for ((dst, src) <- box.asSeq zip vs) { dst copyFrom src } }
    def asRef_=(v: Word): Unit = box.asInstanceOf[BoxRef].objRef = v
    def asIRef_=(oo: (Word, Word)): Unit = box.asInstanceOf[BoxIRef].oo = oo
    def asTR64Raw_=(v: Long): Unit = box.asInstanceOf[BoxTagRef64].raw = v
    def asFunc_=(v: Option[Function]): Unit = box.asInstanceOf[BoxFunc].obj = v
    def asThread_=(v: Option[InterpreterThread]): Unit = box.asInstanceOf[BoxThread].obj = v
    def asStack_=(v: Option[InterpreterStack]): Unit = box.asInstanceOf[BoxStack].obj = v
    def asFrameCursor_=(v: Option[FrameCursor]): Unit = box.asInstanceOf[BoxFrameCursor].obj = v
    def asIRBuilder_=(v: Option[IRBuilder]): Unit = box.asInstanceOf[BoxIRBuilder].obj = v

    def getSInt(len: Int): BigInt = OpHelper.prepareSigned(box.asIntRaw, len)
    def getUInt(len: Int): BigInt = OpHelper.prepareUnsigned(box.asIntRaw, len)
    def setInt(v: BigInt, len: Int): Unit = box.asIntRaw = OpHelper.unprepare(v, len)
  }
}