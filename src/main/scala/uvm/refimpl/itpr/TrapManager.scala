package uvm.refimpl.itpr

import scala.collection.mutable.HashSet

import uvm.MuID
import uvm.refimpl._

class TrapManager(implicit microVM: MicroVM) {
  var trapHandler: TrapHandler = DefaultTrapHandler

  private val enabledWatchPoints = new HashSet[MuID]()

  def isWatchPointEnabled(wpID: MuID): Boolean = enabledWatchPoints.contains(wpID)

  def enableWatchPoint(wpID: MuID): Unit = enabledWatchPoints.add(wpID)

  def disableWatchPoint(wpID: MuID): Unit = enabledWatchPoints.remove(wpID)

  object DefaultTrapHandler extends TrapHandler {
    def handleTrap(ctx: MuCtx, thread: MuThreadRefValue, stack: MuStackRefValue, watchPointID: MuID): TrapHandlerResult = {
      val thrID = thread.vb.obj.get.id
      val staID = stack.vb.obj.get.id
      val cursor = ctx.newCursor(stack)
      val curFuncID = ctx.curFunc(cursor)
      val curFuncVerID = ctx.curFuncVer(cursor)
      val curInstID = ctx.curInst(cursor)
      ctx.closeCursor(cursor)
      throw new UvmRuntimeException("Unhandled trap. thread %d, stack: %d, func: %d, funcver: %d, inst %d, watch point ID %d".format(
        thrID, staID, curFuncID, curFuncVerID, curInstID, watchPointID))
    }
  }

}
