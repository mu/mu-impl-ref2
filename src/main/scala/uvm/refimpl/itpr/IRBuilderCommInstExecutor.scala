package uvm.refimpl.itpr

import uvm._
import uvm.comminsts._
import uvm.refimpl._
import uvm.refimpl.mem._
import uvm.refimpl.nat.CDefs
import uvm.refimpl.nat.NativeMemoryAccessHelper
import uvm.ssavariables._

object IRBuilderCommInstExecutor {
  implicit def toBinOptr(i: Int): BinOptr.Value = CDefs.toBinOptr(i)
  implicit def toCmpOptr(i: Int): CmpOptr.Value = CDefs.toCmpOptr(i)
  implicit def toConvOptr(i: Int): ConvOptr.Value = CDefs.toConvOptr(i)
  implicit def toMemoryOrder(i: Int): MemoryOrder.Value = CDefs.toMemoryOrder(i)
  implicit def toAtomicRMWOptr(i: Int): AtomicRMWOptr.Value = CDefs.toAtomicRMWOptr(i)
  implicit def toFlag(i: Int): Flag = i match {
    case CDefs.MU_CC_DEFAULT => Flag("#DEFAULT")
  }
  implicit def toFlags(is: IndexedSeq[Int]): IndexedSeq[Flag] = is.map(toFlag)
  implicit def toCommInst(i: Int): CommInst = CommInsts(i)

  def unsignedLongSeqToBigInt(nums: Seq[Long]): BigInt = {
    var bigNum = BigInt(0)
    var i = 0
    for (num <- nums) {
      bigNum = bigNum | ((BigInt(num) & 0xffffffffffffffffL) << 64 * i)
      i += 1
    }
    bigNum
  }
  implicit class IDOptionalUtil(val id: MuID) extends AnyVal {
    def asOptionalID: Option[MuID] = if (id == 0) None else Some(id)
  }
}

/**
 * A part of the InterpreterThread that interprets common instructions
 */
trait IRBuilderCommInstExecutor extends InterpreterActions with ObjectPinner {
  import IRBuilderCommInstExecutor._

  implicit protected def mutator: Mutator
  implicit protected def memorySupport: MemorySupport

  protected def loadInt64Array(ir: (Word, Word), sz: Word): IndexedSeq[Long] = {
    val (obj, off) = ir
    val loc = obj + off
    MemoryOperations.loadInt64Array(loc, sz)
  }

  protected def loadInt32Array(ir: (Word, Word), sz: Word): IndexedSeq[Int] = {
    val (obj, off) = ir
    val loc = obj + off
    MemoryOperations.loadInt32Array(loc, sz)
  }

  protected def loadFlagArray(ir: (Word, Word), sz: Word): IndexedSeq[Flag] = {
    val (obj, off) = ir
    val loc = obj + off
    MemoryOperations.loadInt32Array(loc, sz).map(toFlag)
  }

  protected def loadCString(ir: (Word, Word)): String = {
    val (obj, off) = ir
    val loc = obj + off
    NativeMemoryAccessHelper.readCString(loc)
  }

  protected def loadMaybeCString(ir: (Word, Word)): Option[String] = {
    val (obj, off) = ir
    val loc = obj + off
    NativeMemoryAccessHelper.readCStringOptional(loc)
  }

  private type TB = TrantientBundle

  def interpretCurrentIRBuilderCommonInstruction(): Unit = {
    assert(curInst.isInstanceOf[InstCommInst])
    val InstCommInst(ci, _, _, _, argList, _, _) = curInst

    assert(ci.name.get.startsWith("@uvm.irbuilder"))

    ci.name.get match {
      case "@uvm.irbuilder.new_ir_builder" => {
        val irBuilder = microVM.newIRBuilder()
        results(0).asIRBuilder = Some(irBuilder)

        continueNormally()
      }

      /// The auto-generated implementations work just fine.
      //case "@uvm.irbuilder.load" => {
      //  val _param0 = argList(0).asIRBuilder.get
      //  _param0.load()
      //  continueNormally()
      //}
      //case "@uvm.irbuilder.abort" => {
      //  val _param0 = argList(0).asIRBuilder.get
      //  _param0.abort()
      //  continueNormally()
      //}

      // Auto-generated implementations go here
      /// GEN:BEGIN:IRBUILDER_IMPL
      case "@uvm.irbuilder.load" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val _rv = b.load()
        continueNormally()
      }
      case "@uvm.irbuilder.abort" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val _rv = b.abort()
        continueNormally()
      }
      case "@uvm.irbuilder.gen_sym" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val name = argList(1).asIRef
        val _maybestr_name = loadMaybeCString(name)
        val _rv = b.genSym(_maybestr_name)
        results(0).asInt32 = _rv
        continueNormally()
      }
      case "@uvm.irbuilder.new_type_int" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val len = argList(2).asInt32.toInt
        val _rv = b.newTypeInt(id, len)
        continueNormally()
      }
      case "@uvm.irbuilder.new_type_float" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val _rv = b.newTypeFloat(id)
        continueNormally()
      }
      case "@uvm.irbuilder.new_type_double" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val _rv = b.newTypeDouble(id)
        continueNormally()
      }
      case "@uvm.irbuilder.new_type_uptr" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val ty = argList(2).asInt32.toInt
        val _rv = b.newTypeUPtr(id, ty)
        continueNormally()
      }
      case "@uvm.irbuilder.new_type_ufuncptr" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val sig = argList(2).asInt32.toInt
        val _rv = b.newTypeUFuncPtr(id, sig)
        continueNormally()
      }
      case "@uvm.irbuilder.new_type_struct" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val fieldtys = argList(2).asIRef
        val nfieldtys = argList(3).asInt64.toLong
        val _ary_fieldtys = loadInt32Array(fieldtys, nfieldtys)
        val _rv = b.newTypeStruct(id, _ary_fieldtys)
        continueNormally()
      }
      case "@uvm.irbuilder.new_type_hybrid" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val fixedtys = argList(2).asIRef
        val nfixedtys = argList(3).asInt64.toLong
        val varty = argList(4).asInt32.toInt
        val _ary_fixedtys = loadInt32Array(fixedtys, nfixedtys)
        val _rv = b.newTypeHybrid(id, _ary_fixedtys, varty)
        continueNormally()
      }
      case "@uvm.irbuilder.new_type_array" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val elemty = argList(2).asInt32.toInt
        val len = argList(3).asInt64.toLong
        val _rv = b.newTypeArray(id, elemty, len)
        continueNormally()
      }
      case "@uvm.irbuilder.new_type_vector" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val elemty = argList(2).asInt32.toInt
        val len = argList(3).asInt64.toLong
        val _rv = b.newTypeVector(id, elemty, len)
        continueNormally()
      }
      case "@uvm.irbuilder.new_type_void" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val _rv = b.newTypeVoid(id)
        continueNormally()
      }
      case "@uvm.irbuilder.new_type_ref" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val ty = argList(2).asInt32.toInt
        val _rv = b.newTypeRef(id, ty)
        continueNormally()
      }
      case "@uvm.irbuilder.new_type_iref" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val ty = argList(2).asInt32.toInt
        val _rv = b.newTypeIRef(id, ty)
        continueNormally()
      }
      case "@uvm.irbuilder.new_type_weakref" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val ty = argList(2).asInt32.toInt
        val _rv = b.newTypeWeakRef(id, ty)
        continueNormally()
      }
      case "@uvm.irbuilder.new_type_funcref" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val sig = argList(2).asInt32.toInt
        val _rv = b.newTypeFuncRef(id, sig)
        continueNormally()
      }
      case "@uvm.irbuilder.new_type_tagref64" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val _rv = b.newTypeTagRef64(id)
        continueNormally()
      }
      case "@uvm.irbuilder.new_type_threadref" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val _rv = b.newTypeThreadRef(id)
        continueNormally()
      }
      case "@uvm.irbuilder.new_type_stackref" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val _rv = b.newTypeStackRef(id)
        continueNormally()
      }
      case "@uvm.irbuilder.new_type_framecursorref" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val _rv = b.newTypeFrameCursorRef(id)
        continueNormally()
      }
      case "@uvm.irbuilder.new_type_irbuilderref" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val _rv = b.newTypeIRBuilderRef(id)
        continueNormally()
      }
      case "@uvm.irbuilder.new_funcsig" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val paramtys = argList(2).asIRef
        val nparamtys = argList(3).asInt64.toLong
        val rettys = argList(4).asIRef
        val nrettys = argList(5).asInt64.toLong
        val _ary_paramtys = loadInt32Array(paramtys, nparamtys)
        val _ary_rettys = loadInt32Array(rettys, nrettys)
        val _rv = b.newFuncSig(id, _ary_paramtys, _ary_rettys)
        continueNormally()
      }
      case "@uvm.irbuilder.new_const_int" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val ty = argList(2).asInt32.toInt
        val value = argList(3).asInt64.toLong
        val _rv = b.newConstInt(id, ty, value)
        continueNormally()
      }
      case "@uvm.irbuilder.new_const_float" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val ty = argList(2).asInt32.toInt
        val value = argList(3).asFloat
        val _rv = b.newConstFloat(id, ty, value)
        continueNormally()
      }
      case "@uvm.irbuilder.new_const_double" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val ty = argList(2).asInt32.toInt
        val value = argList(3).asDouble
        val _rv = b.newConstDouble(id, ty, value)
        continueNormally()
      }
      case "@uvm.irbuilder.new_const_null" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val ty = argList(2).asInt32.toInt
        val _rv = b.newConstNull(id, ty)
        continueNormally()
      }
      case "@uvm.irbuilder.new_const_seq" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val ty = argList(2).asInt32.toInt
        val elems = argList(3).asIRef
        val nelems = argList(4).asInt64.toLong
        val _ary_elems = loadInt32Array(elems, nelems)
        val _rv = b.newConstSeq(id, ty, _ary_elems)
        continueNormally()
      }
      case "@uvm.irbuilder.new_const_extern" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val ty = argList(2).asInt32.toInt
        val symbol = argList(3).asIRef
        val _str_symbol = loadCString(symbol)
        val _rv = b.newConstExtern(id, ty, _str_symbol)
        continueNormally()
      }
      case "@uvm.irbuilder.new_global_cell" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val ty = argList(2).asInt32.toInt
        val _rv = b.newGlobalCell(id, ty)
        continueNormally()
      }
      case "@uvm.irbuilder.new_func" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val sig = argList(2).asInt32.toInt
        val _rv = b.newFunc(id, sig)
        continueNormally()
      }
      case "@uvm.irbuilder.new_exp_func" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val func = argList(2).asInt32.toInt
        val callconv = argList(3).asInt32.toInt
        val cookie = argList(4).asInt32.toInt
        val _rv = b.newExpFunc(id, func, callconv, cookie)
        continueNormally()
      }
      case "@uvm.irbuilder.new_func_ver" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val func = argList(2).asInt32.toInt
        val bbs = argList(3).asIRef
        val nbbs = argList(4).asInt64.toLong
        val _ary_bbs = loadInt32Array(bbs, nbbs)
        val _rv = b.newFuncVer(id, func, _ary_bbs)
        continueNormally()
      }
      case "@uvm.irbuilder.new_bb" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val nor_param_ids = argList(2).asIRef
        val nor_param_types = argList(3).asIRef
        val n_nor_params = argList(4).asInt64.toLong
        val exc_param_id = argList(5).asInt32.toInt.asOptionalID
        val insts = argList(6).asIRef
        val ninsts = argList(7).asInt64.toLong
        val _ary_nor_param_ids = loadInt32Array(nor_param_ids, n_nor_params)
        val _ary_nor_param_types = loadInt32Array(nor_param_types, n_nor_params)
        val _ary_insts = loadInt32Array(insts, ninsts)
        val _rv = b.newBB(id, _ary_nor_param_ids, _ary_nor_param_types, exc_param_id, _ary_insts)
        continueNormally()
      }
      case "@uvm.irbuilder.new_dest_clause" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val dest = argList(2).asInt32.toInt
        val vars = argList(3).asIRef
        val nvars = argList(4).asInt64.toLong
        val _ary_vars = loadInt32Array(vars, nvars)
        val _rv = b.newDestClause(id, dest, _ary_vars)
        continueNormally()
      }
      case "@uvm.irbuilder.new_exc_clause" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val nor = argList(2).asInt32.toInt
        val exc = argList(3).asInt32.toInt
        val _rv = b.newExcClause(id, nor, exc)
        continueNormally()
      }
      case "@uvm.irbuilder.new_keepalive_clause" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val vars = argList(2).asIRef
        val nvars = argList(3).asInt64.toLong
        val _ary_vars = loadInt32Array(vars, nvars)
        val _rv = b.newKeepaliveClause(id, _ary_vars)
        continueNormally()
      }
      case "@uvm.irbuilder.new_csc_ret_with" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val rettys = argList(2).asIRef
        val nrettys = argList(3).asInt64.toLong
        val _ary_rettys = loadInt32Array(rettys, nrettys)
        val _rv = b.newCscRetWith(id, _ary_rettys)
        continueNormally()
      }
      case "@uvm.irbuilder.new_csc_kill_old" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val _rv = b.newCscKillOld(id)
        continueNormally()
      }
      case "@uvm.irbuilder.new_nsc_pass_values" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val tys = argList(2).asIRef
        val vars = argList(3).asIRef
        val ntysvars = argList(4).asInt64.toLong
        val _ary_tys = loadInt32Array(tys, ntysvars)
        val _ary_vars = loadInt32Array(vars, ntysvars)
        val _rv = b.newNscPassValues(id, _ary_tys, _ary_vars)
        continueNormally()
      }
      case "@uvm.irbuilder.new_nsc_throw_exc" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val exc = argList(2).asInt32.toInt
        val _rv = b.newNscThrowExc(id, exc)
        continueNormally()
      }
      case "@uvm.irbuilder.new_binop" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val result_id = argList(2).asInt32.toInt
        val optr = argList(3).asInt32.toInt
        val ty = argList(4).asInt32.toInt
        val opnd1 = argList(5).asInt32.toInt
        val opnd2 = argList(6).asInt32.toInt
        val exc_clause = argList(7).asInt32.toInt.asOptionalID
        val _rv = b.newBinOp(id, result_id, optr, ty, opnd1, opnd2, exc_clause)
        continueNormally()
      }
      case "@uvm.irbuilder.new_binop_with_status" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val result_id = argList(2).asInt32.toInt
        val status_result_ids = argList(3).asIRef
        val n_status_result_ids = argList(4).asInt64.toLong
        val optr = argList(5).asInt32.toInt
        val status_flags = argList(6).asInt32.toInt
        val ty = argList(7).asInt32.toInt
        val opnd1 = argList(8).asInt32.toInt
        val opnd2 = argList(9).asInt32.toInt
        val exc_clause = argList(10).asInt32.toInt.asOptionalID
        val _ary_status_result_ids = loadInt32Array(status_result_ids, n_status_result_ids)
        val _rv = b.newBinOpWithStatus(id, result_id, _ary_status_result_ids, optr, status_flags, ty, opnd1, opnd2, exc_clause)
        continueNormally()
      }
      case "@uvm.irbuilder.new_cmp" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val result_id = argList(2).asInt32.toInt
        val optr = argList(3).asInt32.toInt
        val ty = argList(4).asInt32.toInt
        val opnd1 = argList(5).asInt32.toInt
        val opnd2 = argList(6).asInt32.toInt
        val _rv = b.newCmp(id, result_id, optr, ty, opnd1, opnd2)
        continueNormally()
      }
      case "@uvm.irbuilder.new_conv" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val result_id = argList(2).asInt32.toInt
        val optr = argList(3).asInt32.toInt
        val from_ty = argList(4).asInt32.toInt
        val to_ty = argList(5).asInt32.toInt
        val opnd = argList(6).asInt32.toInt
        val _rv = b.newConv(id, result_id, optr, from_ty, to_ty, opnd)
        continueNormally()
      }
      case "@uvm.irbuilder.new_select" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val result_id = argList(2).asInt32.toInt
        val cond_ty = argList(3).asInt32.toInt
        val opnd_ty = argList(4).asInt32.toInt
        val cond = argList(5).asInt32.toInt
        val if_true = argList(6).asInt32.toInt
        val if_false = argList(7).asInt32.toInt
        val _rv = b.newSelect(id, result_id, cond_ty, opnd_ty, cond, if_true, if_false)
        continueNormally()
      }
      case "@uvm.irbuilder.new_branch" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val dest = argList(2).asInt32.toInt
        val _rv = b.newBranch(id, dest)
        continueNormally()
      }
      case "@uvm.irbuilder.new_branch2" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val cond = argList(2).asInt32.toInt
        val if_true = argList(3).asInt32.toInt
        val if_false = argList(4).asInt32.toInt
        val _rv = b.newBranch2(id, cond, if_true, if_false)
        continueNormally()
      }
      case "@uvm.irbuilder.new_switch" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val opnd_ty = argList(2).asInt32.toInt
        val opnd = argList(3).asInt32.toInt
        val default_dest = argList(4).asInt32.toInt
        val cases = argList(5).asIRef
        val dests = argList(6).asIRef
        val ncasesdests = argList(7).asInt64.toLong
        val _ary_cases = loadInt32Array(cases, ncasesdests)
        val _ary_dests = loadInt32Array(dests, ncasesdests)
        val _rv = b.newSwitch(id, opnd_ty, opnd, default_dest, _ary_cases, _ary_dests)
        continueNormally()
      }
      case "@uvm.irbuilder.new_call" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val result_ids = argList(2).asIRef
        val n_result_ids = argList(3).asInt64.toLong
        val sig = argList(4).asInt32.toInt
        val callee = argList(5).asInt32.toInt
        val args = argList(6).asIRef
        val nargs = argList(7).asInt64.toLong
        val exc_clause = argList(8).asInt32.toInt.asOptionalID
        val keepalive_clause = argList(9).asInt32.toInt.asOptionalID
        val _ary_result_ids = loadInt32Array(result_ids, n_result_ids)
        val _ary_args = loadInt32Array(args, nargs)
        val _rv = b.newCall(id, _ary_result_ids, sig, callee, _ary_args, exc_clause, keepalive_clause)
        continueNormally()
      }
      case "@uvm.irbuilder.new_tailcall" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val sig = argList(2).asInt32.toInt
        val callee = argList(3).asInt32.toInt
        val args = argList(4).asIRef
        val nargs = argList(5).asInt64.toLong
        val _ary_args = loadInt32Array(args, nargs)
        val _rv = b.newTailCall(id, sig, callee, _ary_args)
        continueNormally()
      }
      case "@uvm.irbuilder.new_ret" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val rvs = argList(2).asIRef
        val nrvs = argList(3).asInt64.toLong
        val _ary_rvs = loadInt32Array(rvs, nrvs)
        val _rv = b.newRet(id, _ary_rvs)
        continueNormally()
      }
      case "@uvm.irbuilder.new_throw" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val exc = argList(2).asInt32.toInt
        val _rv = b.newThrow(id, exc)
        continueNormally()
      }
      case "@uvm.irbuilder.new_extractvalue" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val result_id = argList(2).asInt32.toInt
        val strty = argList(3).asInt32.toInt
        val index = argList(4).asInt32.toInt
        val opnd = argList(5).asInt32.toInt
        val _rv = b.newExtractValue(id, result_id, strty, index, opnd)
        continueNormally()
      }
      case "@uvm.irbuilder.new_insertvalue" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val result_id = argList(2).asInt32.toInt
        val strty = argList(3).asInt32.toInt
        val index = argList(4).asInt32.toInt
        val opnd = argList(5).asInt32.toInt
        val newval = argList(6).asInt32.toInt
        val _rv = b.newInsertValue(id, result_id, strty, index, opnd, newval)
        continueNormally()
      }
      case "@uvm.irbuilder.new_extractelement" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val result_id = argList(2).asInt32.toInt
        val seqty = argList(3).asInt32.toInt
        val indty = argList(4).asInt32.toInt
        val opnd = argList(5).asInt32.toInt
        val index = argList(6).asInt32.toInt
        val _rv = b.newExtractElement(id, result_id, seqty, indty, opnd, index)
        continueNormally()
      }
      case "@uvm.irbuilder.new_insertelement" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val result_id = argList(2).asInt32.toInt
        val seqty = argList(3).asInt32.toInt
        val indty = argList(4).asInt32.toInt
        val opnd = argList(5).asInt32.toInt
        val index = argList(6).asInt32.toInt
        val newval = argList(7).asInt32.toInt
        val _rv = b.newInsertElement(id, result_id, seqty, indty, opnd, index, newval)
        continueNormally()
      }
      case "@uvm.irbuilder.new_shufflevector" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val result_id = argList(2).asInt32.toInt
        val vecty = argList(3).asInt32.toInt
        val maskty = argList(4).asInt32.toInt
        val vec1 = argList(5).asInt32.toInt
        val vec2 = argList(6).asInt32.toInt
        val mask = argList(7).asInt32.toInt
        val _rv = b.newShuffleVector(id, result_id, vecty, maskty, vec1, vec2, mask)
        continueNormally()
      }
      case "@uvm.irbuilder.new_new" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val result_id = argList(2).asInt32.toInt
        val allocty = argList(3).asInt32.toInt
        val exc_clause = argList(4).asInt32.toInt.asOptionalID
        val _rv = b.newNew(id, result_id, allocty, exc_clause)
        continueNormally()
      }
      case "@uvm.irbuilder.new_newhybrid" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val result_id = argList(2).asInt32.toInt
        val allocty = argList(3).asInt32.toInt
        val lenty = argList(4).asInt32.toInt
        val length = argList(5).asInt32.toInt
        val exc_clause = argList(6).asInt32.toInt.asOptionalID
        val _rv = b.newNewHybrid(id, result_id, allocty, lenty, length, exc_clause)
        continueNormally()
      }
      case "@uvm.irbuilder.new_alloca" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val result_id = argList(2).asInt32.toInt
        val allocty = argList(3).asInt32.toInt
        val exc_clause = argList(4).asInt32.toInt.asOptionalID
        val _rv = b.newAlloca(id, result_id, allocty, exc_clause)
        continueNormally()
      }
      case "@uvm.irbuilder.new_allocahybrid" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val result_id = argList(2).asInt32.toInt
        val allocty = argList(3).asInt32.toInt
        val lenty = argList(4).asInt32.toInt
        val length = argList(5).asInt32.toInt
        val exc_clause = argList(6).asInt32.toInt.asOptionalID
        val _rv = b.newAllocaHybrid(id, result_id, allocty, lenty, length, exc_clause)
        continueNormally()
      }
      case "@uvm.irbuilder.new_getiref" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val result_id = argList(2).asInt32.toInt
        val refty = argList(3).asInt32.toInt
        val opnd = argList(4).asInt32.toInt
        val _rv = b.newGetIRef(id, result_id, refty, opnd)
        continueNormally()
      }
      case "@uvm.irbuilder.new_getfieldiref" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val result_id = argList(2).asInt32.toInt
        val is_ptr = argList(3).asInt32.toInt
        val refty = argList(4).asInt32.toInt
        val index = argList(5).asInt32.toInt
        val opnd = argList(6).asInt32.toInt
        val _bool_is_ptr = is_ptr != 0
        val _rv = b.newGetFieldIRef(id, result_id, _bool_is_ptr, refty, index, opnd)
        continueNormally()
      }
      case "@uvm.irbuilder.new_getelemiref" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val result_id = argList(2).asInt32.toInt
        val is_ptr = argList(3).asInt32.toInt
        val refty = argList(4).asInt32.toInt
        val indty = argList(5).asInt32.toInt
        val opnd = argList(6).asInt32.toInt
        val index = argList(7).asInt32.toInt
        val _bool_is_ptr = is_ptr != 0
        val _rv = b.newGetElemIRef(id, result_id, _bool_is_ptr, refty, indty, opnd, index)
        continueNormally()
      }
      case "@uvm.irbuilder.new_shiftiref" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val result_id = argList(2).asInt32.toInt
        val is_ptr = argList(3).asInt32.toInt
        val refty = argList(4).asInt32.toInt
        val offty = argList(5).asInt32.toInt
        val opnd = argList(6).asInt32.toInt
        val offset = argList(7).asInt32.toInt
        val _bool_is_ptr = is_ptr != 0
        val _rv = b.newShiftIRef(id, result_id, _bool_is_ptr, refty, offty, opnd, offset)
        continueNormally()
      }
      case "@uvm.irbuilder.new_getvarpartiref" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val result_id = argList(2).asInt32.toInt
        val is_ptr = argList(3).asInt32.toInt
        val refty = argList(4).asInt32.toInt
        val opnd = argList(5).asInt32.toInt
        val _bool_is_ptr = is_ptr != 0
        val _rv = b.newGetVarPartIRef(id, result_id, _bool_is_ptr, refty, opnd)
        continueNormally()
      }
      case "@uvm.irbuilder.new_load" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val result_id = argList(2).asInt32.toInt
        val is_ptr = argList(3).asInt32.toInt
        val ord = argList(4).asInt32.toInt
        val refty = argList(5).asInt32.toInt
        val loc = argList(6).asInt32.toInt
        val exc_clause = argList(7).asInt32.toInt.asOptionalID
        val _bool_is_ptr = is_ptr != 0
        val _rv = b.newLoad(id, result_id, _bool_is_ptr, ord, refty, loc, exc_clause)
        continueNormally()
      }
      case "@uvm.irbuilder.new_store" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val is_ptr = argList(2).asInt32.toInt
        val ord = argList(3).asInt32.toInt
        val refty = argList(4).asInt32.toInt
        val loc = argList(5).asInt32.toInt
        val newval = argList(6).asInt32.toInt
        val exc_clause = argList(7).asInt32.toInt.asOptionalID
        val _bool_is_ptr = is_ptr != 0
        val _rv = b.newStore(id, _bool_is_ptr, ord, refty, loc, newval, exc_clause)
        continueNormally()
      }
      case "@uvm.irbuilder.new_cmpxchg" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val value_result_id = argList(2).asInt32.toInt
        val succ_result_id = argList(3).asInt32.toInt
        val is_ptr = argList(4).asInt32.toInt
        val is_weak = argList(5).asInt32.toInt
        val ord_succ = argList(6).asInt32.toInt
        val ord_fail = argList(7).asInt32.toInt
        val refty = argList(8).asInt32.toInt
        val loc = argList(9).asInt32.toInt
        val expected = argList(10).asInt32.toInt
        val desired = argList(11).asInt32.toInt
        val exc_clause = argList(12).asInt32.toInt.asOptionalID
        val _bool_is_ptr = is_ptr != 0
        val _bool_is_weak = is_weak != 0
        val _rv = b.newCmpXchg(id, value_result_id, succ_result_id, _bool_is_ptr, _bool_is_weak, ord_succ, ord_fail, refty, loc, expected, desired, exc_clause)
        continueNormally()
      }
      case "@uvm.irbuilder.new_atomicrmw" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val result_id = argList(2).asInt32.toInt
        val is_ptr = argList(3).asInt32.toInt
        val ord = argList(4).asInt32.toInt
        val optr = argList(5).asInt32.toInt
        val ref_ty = argList(6).asInt32.toInt
        val loc = argList(7).asInt32.toInt
        val opnd = argList(8).asInt32.toInt
        val exc_clause = argList(9).asInt32.toInt.asOptionalID
        val _bool_is_ptr = is_ptr != 0
        val _rv = b.newAtomicRMW(id, result_id, _bool_is_ptr, ord, optr, ref_ty, loc, opnd, exc_clause)
        continueNormally()
      }
      case "@uvm.irbuilder.new_fence" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val ord = argList(2).asInt32.toInt
        val _rv = b.newFence(id, ord)
        continueNormally()
      }
      case "@uvm.irbuilder.new_trap" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val result_ids = argList(2).asIRef
        val rettys = argList(3).asIRef
        val nretvals = argList(4).asInt64.toLong
        val exc_clause = argList(5).asInt32.toInt.asOptionalID
        val keepalive_clause = argList(6).asInt32.toInt.asOptionalID
        val _ary_result_ids = loadInt32Array(result_ids, nretvals)
        val _ary_rettys = loadInt32Array(rettys, nretvals)
        val _rv = b.newTrap(id, _ary_result_ids, _ary_rettys, exc_clause, keepalive_clause)
        continueNormally()
      }
      case "@uvm.irbuilder.new_watchpoint" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val wpid = argList(2).asInt32.toInt
        val result_ids = argList(3).asIRef
        val rettys = argList(4).asIRef
        val nretvals = argList(5).asInt64.toLong
        val dis = argList(6).asInt32.toInt
        val ena = argList(7).asInt32.toInt
        val exc = argList(8).asInt32.toInt.asOptionalID
        val keepalive_clause = argList(9).asInt32.toInt.asOptionalID
        val _ary_result_ids = loadInt32Array(result_ids, nretvals)
        val _ary_rettys = loadInt32Array(rettys, nretvals)
        val _rv = b.newWatchPoint(id, wpid, _ary_result_ids, _ary_rettys, dis, ena, exc, keepalive_clause)
        continueNormally()
      }
      case "@uvm.irbuilder.new_wpbranch" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val wpid = argList(2).asInt32.toInt
        val dis = argList(3).asInt32.toInt
        val ena = argList(4).asInt32.toInt
        val _rv = b.newWPBranch(id, wpid, dis, ena)
        continueNormally()
      }
      case "@uvm.irbuilder.new_ccall" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val result_ids = argList(2).asIRef
        val n_result_ids = argList(3).asInt64.toLong
        val callconv = argList(4).asInt32.toInt
        val callee_ty = argList(5).asInt32.toInt
        val sig = argList(6).asInt32.toInt
        val callee = argList(7).asInt32.toInt
        val args = argList(8).asIRef
        val nargs = argList(9).asInt64.toLong
        val exc_clause = argList(10).asInt32.toInt.asOptionalID
        val keepalive_clause = argList(11).asInt32.toInt.asOptionalID
        val _ary_result_ids = loadInt32Array(result_ids, n_result_ids)
        val _ary_args = loadInt32Array(args, nargs)
        val _rv = b.newCCall(id, _ary_result_ids, callconv, callee_ty, sig, callee, _ary_args, exc_clause, keepalive_clause)
        continueNormally()
      }
      case "@uvm.irbuilder.new_newthread" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val result_id = argList(2).asInt32.toInt
        val stack = argList(3).asInt32.toInt
        val threadlocal = argList(4).asInt32.toInt.asOptionalID
        val new_stack_clause = argList(5).asInt32.toInt
        val exc_clause = argList(6).asInt32.toInt.asOptionalID
        val _rv = b.newNewThread(id, result_id, stack, threadlocal, new_stack_clause, exc_clause)
        continueNormally()
      }
      case "@uvm.irbuilder.new_swapstack" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val result_ids = argList(2).asIRef
        val n_result_ids = argList(3).asInt64.toLong
        val swappee = argList(4).asInt32.toInt
        val cur_stack_clause = argList(5).asInt32.toInt
        val new_stack_clause = argList(6).asInt32.toInt
        val exc_clause = argList(7).asInt32.toInt.asOptionalID
        val keepalive_clause = argList(8).asInt32.toInt.asOptionalID
        val _ary_result_ids = loadInt32Array(result_ids, n_result_ids)
        val _rv = b.newSwapStack(id, _ary_result_ids, swappee, cur_stack_clause, new_stack_clause, exc_clause, keepalive_clause)
        continueNormally()
      }
      case "@uvm.irbuilder.new_comminst" => {
        val b = argList(0).asIRBuilder.getOrElse(throw new UvmNullGenRefException("CommInst arg %b must not be null"))
        val id = argList(1).asInt32.toInt
        val result_ids = argList(2).asIRef
        val n_result_ids = argList(3).asInt64.toLong
        val opcode = argList(4).asInt32.toInt
        val flags = argList(5).asIRef
        val nflags = argList(6).asInt64.toLong
        val tys = argList(7).asIRef
        val ntys = argList(8).asInt64.toLong
        val sigs = argList(9).asIRef
        val nsigs = argList(10).asInt64.toLong
        val args = argList(11).asIRef
        val nargs = argList(12).asInt64.toLong
        val exc_clause = argList(13).asInt32.toInt.asOptionalID
        val keepalive_clause = argList(14).asInt32.toInt.asOptionalID
        val _ary_result_ids = loadInt32Array(result_ids, n_result_ids)
        val _ary_flags = loadInt32Array(flags, nflags)
        val _ary_tys = loadInt32Array(tys, ntys)
        val _ary_sigs = loadInt32Array(sigs, nsigs)
        val _ary_args = loadInt32Array(args, nargs)
        val _rv = b.newCommInst(id, _ary_result_ids, opcode, _ary_flags, _ary_tys, _ary_sigs, _ary_args, exc_clause, keepalive_clause)
        continueNormally()
      }
      /// GEN:END:IRBUILDER_IMPL

      case ciName => {
        throw new UvmRefImplException("Unimplemented IR builder common instruction %s".format(ciName))
      }

    }

  }
}
