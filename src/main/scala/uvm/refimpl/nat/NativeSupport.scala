package uvm.refimpl.nat

import com.kenai.jffi.ClosureManager
import com.kenai.jffi.LastError
import com.kenai.jffi.MemoryIO

import jnr.ffi.Pointer
import jnr.ffi.provider.jffi.NativeRuntime

/**
 * Holder of JNR-specific resources.
 */
object NativeSupport {
  // Force using NativeRuntime (although that's default).
  val jnrRuntime = NativeRuntime.getInstance
  val jnrMemoryManager = jnrRuntime.getMemoryManager
  val theMemory = jnrMemoryManager.newPointer(0L)
  
  // This is from JFFI, not JNR-FFI.
  val jffiClosureManager = ClosureManager.getInstance()
  val jffiLastError = LastError.getInstance()
  
  // Explicit memory management
  
  val IO = MemoryIO.getInstance()
  
  def allocateManual(size: Long): ManualMemory = allocateManual(size, true)
  
  def allocateManual(size: Long, clear: Boolean): ManualMemory = {
    val addr = IO.allocateMemory(size, clear)
    new ManualMemory(addr, size)
  }
}

class ManualMemory(val address: Long, val size: Long) extends AutoCloseable {
  def asJnrPointer(): Pointer = Pointer.wrap(NativeSupport.jnrRuntime, address, size)
  
  override def close(): Unit = {
    NativeSupport.IO.freeMemory(address)
  }
}