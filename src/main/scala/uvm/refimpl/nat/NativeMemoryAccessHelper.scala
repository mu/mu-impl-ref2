package uvm.refimpl.nat

import java.nio.charset.StandardCharsets

object NativeMemoryAccessHelper {
  import NativeSupport._
  val MAX_NAME_SIZE = 65536

  def readCString(addr: Long): String = {
    val str = theMemory.getString(addr, MAX_NAME_SIZE, StandardCharsets.US_ASCII)
    str
  }
  
  def readCStringArray(base: Long, len: Long): IndexedSeq[String] = readLongArray(base, len).map(readCString)
  
  def readCStringOptional(addr: Long): Option[String] = {
    if (addr == 0L) None else Some(readCString(addr))
  }

  def readCharArray(base: Long, len: Long): String = {
    require(len <= Int.MaxValue, "Currently Mu refimpl2 only supports string length with 32-bit range")
    val str = theMemory.getString(base, len.toInt, StandardCharsets.US_ASCII)
    str
  }

  def unsignedLongSeqToBigInt(nums: Seq[Long]): BigInt = {
    var bigNum = BigInt(0)
    var i = 0
    for (num <- nums) {
      bigNum = bigNum | ((BigInt(num) & 0xffffffffffffffffL) << 64 * i)
      i += 1
    }
    bigNum
  }

  def readIntArray(base: Long, len: Long): IndexedSeq[Int] = {
    if (base == 0L) {
      IndexedSeq[Int]()
    } else {
      for (i <- 0L until len) yield {
        val addr = base + i * 4L
        val v = theMemory.getInt(addr)
        v
      }
    }
  }

  def readLongArray(base: Long, len: Long): IndexedSeq[Long] = {
    if (base == 0L) {
      IndexedSeq[Long]()
    } else {
      for (i <- 0L until len) yield {
        val addr = base + i * 8L
        val v = theMemory.getLong(addr)
        v
      }
    }
  }
  
}