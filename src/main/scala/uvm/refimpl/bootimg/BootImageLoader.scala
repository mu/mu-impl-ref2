package uvm.refimpl.bootimg

import java.io.BufferedReader
import java.io.Closeable
import java.io.FileNotFoundException
import java.io.InputStream
import java.io.InputStreamReader
import java.io.Reader
import java.nio.charset.StandardCharsets
import java.nio.file.Paths
import java.util.zip.ZipEntry
import java.util.zip.ZipFile

import scala.collection.Map
import scala.collection.mutable.HashMap

import org.slf4j.LoggerFactory

import com.typesafe.scalalogging.Logger

import uvm.{ MuID, MuName }
import uvm.refimpl.{ WORD_SIZE_BYTES, Word }
import uvm.refimpl.MicroVM
import uvm.refimpl.cmdline.NativeArgv
import uvm.refimpl.itpr.BoxInt
import uvm.refimpl.itpr.BoxPointer
import uvm.refimpl.itpr.HowToResume
import uvm.refimpl.itpr.OpHelper
import uvm.refimpl.mem.HeaderUtils
import uvm.refimpl.mem.TypeSizes
import uvm.refimpl.nat.NativeSupport
import uvm.types.TypeHybrid
import uvm.utils.IOHelpers
import uvm.utils.IOHelpers.forEachLine
import uvm.utils.MappedIDFactory
import uvm.utils.WithUtils.tryWithResource

object BootImageLoader {
  val logger = Logger(LoggerFactory.getLogger(getClass.getName))
  val HEAP_INIT_MUTATOR_NAME = "boot-image-loader-heap-init"
  val PRIMORDIAL_INIT_MUTATOR_NAME = "boot-image-loader-primordial"
  val EXTRALIBS_FILE = "extralibs"
}

class BootImageLoader(file: String, maybeAllArgs: Option[Seq[String]])(implicit microVM: MicroVM) extends AutoCloseable {
  import BootImageFile._
  import BootImageLoader._

  private val globalBundle = microVM.globalBundle
  private val memoryManager = microVM.memoryManager
  private val heap = memoryManager.heap
  private val globalMemory = memoryManager.globalMemory
  private val constantPool = microVM.constantPool
  private implicit val memorySupport = memoryManager.memorySupport

  val bootImagePath = Paths.get(file)
  val zip = try {
    new ZipFile(bootImagePath.toFile())
  } catch {
    case e: FileNotFoundException => throw new BootImageLoaderException(
      "Boot image not found: " + file, e)
  }

  val objNumToAddr = new HashMap[Long, Word]()
  def globalNumToAddr(num: Long): Word = {
    val g = globalBundle.globalCellNs(num.toInt)
    globalMemory.addrForGlobalCell(g)
  }
  
  val extraSyms = new HashMap[String, ManualSymRecord]()

  private def zipFile(entry: ZipEntry): InputStream = zip.getInputStream(entry)
  private def zipFile(name: String): InputStream = zipFile(zip.getEntry(name))
  private def zipFileText(name: String): Reader = new InputStreamReader(zipFile(name), StandardCharsets.UTF_8)
  private def zipFileTextBuf(name: String): BufferedReader = new BufferedReader(zipFileText(name))

  override def close(): Unit = {
    zip.close()
  }

  def load(): Unit = {
    logger.info("Loading boot image: {}".format(file))

    val metaInfo = loadMetaInfo()

    loadExtraLibs(metaInfo)

    loadUIRBundle()

    initializeMemory()

    maybeCreatePrimordialThread(metaInfo)
  }

  private def loadMetaInfo(): Map[String, String] = {
    val lines = tryWithResource(zipFileTextBuf(METAINFO_FILE)) { br =>
      IOHelpers.readLines(br)
    }
    val pairs = lines.map { line =>
      val Array(k, v) = line.split("=", -1)
      (k, v)
    }
    pairs.toMap
  }

  private def loadExtraLibs(metaInfo: Map[String, String]): Unit = {
    for (line <- metaInfo.get("extralibs")) {
      val libs = line.split(":")

      for (lib <- libs) {
        logger.info("Loading extra library: " + lib)
        microVM.nativeLibraryHolder.loadLibrary(lib)
      }
    }
  }

  private def loadUIRBundle(): Unit = {
    val map = loadIDNameMap()
    val mappedIDFactory = new MappedIDFactory(map)

    tryWithResource(zipFileText(UIRBUNDLE_FILE)) { r =>
      val bundle = microVM.irReader.read(r, globalBundle, mappedIDFactory)
      microVM.addBundle(bundle)
    }

    val maxID = map.values.max
    microVM.idFactory.setNextID(maxID + 1)
  }

  private def loadIDNameMap(): Map[MuName, MuID] = {
    val map = new HashMap[MuName, MuID]()
    val idNameMapTxt = tryWithResource(zipFileTextBuf(IDNAMEMAP_FILE)) { br =>
      var finished = false
      while (!finished) {
        Option(br.readLine()) match {
          case None => finished = true
          case Some(line) => {
            val kv = line.split(",")
            val id = kv(0).toInt
            val name = kv(1)
            map(name) = id
          }
        }
      }
    }
    map
  }

  private def initializeMemory(): Unit = {
    // Disable GC and prevent object movement
    heap.gcDisabled = true

    allocateAndCopy()
    loadManualSyms()
    relocate()

    // Re-enable GC when we are done.
    heap.gcDisabled = false
  }

  private def allocateAndCopy(): Unit = {
    // Initialise global cell and heap objects.
    forEachUnit("global") { (ais, ar) =>
      val UnitAllocRecord(num, fileOffset, ty, varLen, _) = ar
      assert(!ty.isInstanceOf[TypeHybrid], "BUG: hybrid found in global space. num: %d, fileOffset: %d, type: %s".format(
        num, fileOffset, ty))

      val addr = globalNumToAddr(num)
      val size = TypeSizes.sizeOf(ty)

      ais.readToMemory(addr, size)
    }

    tryWithResource(heap.makeMutator(HEAP_INIT_MUTATOR_NAME)) { mutator =>
      forEachUnit("heap") { (ais, ar) =>
        val UnitAllocRecord(num, fileOffset, ty, varLen, _) = ar

        val (size, addr) = ty match {
          case t: TypeHybrid => {
            val sz = TypeSizes.hybridSizeOf(t, varLen)
            val ad = mutator.newHybrid(t, varLen)
            (sz, ad)
          }
          case t => {
            val sz = TypeSizes.sizeOf(t)
            val ad = mutator.newScalar(t)
            (sz, ad)
          }
        }

        objNumToAddr(num) = addr

        ais.readToMemory(addr, size)
      }
    }
  }

  private def forEachUnit(groupName: String)(f: (AlignedInputStream, UnitAllocRecord) => Unit): Unit = {
    tryWithResource(new AlignedInputStream(zipFile(groupName + FILEEXT_DATA))) { dataIS =>
      tryWithResource(zipFileTextBuf(groupName + FILEEXT_ALLOC)) { allocIS =>
        forEachLine(allocIS) { line =>
          val elems = line.split(",")
          val num = elems(0).toLong
          val fileOffset = elems(1).toLong
          val tyID = elems(2).toInt
          val varLen = elems(3).toLong
          val tyName = elems(4)
          val ty = globalBundle.typeNs(tyID)
          val ar = UnitAllocRecord(num, fileOffset, ty, varLen, 0L)
          dataIS.seekTo(fileOffset)
          f(dataIS, ar)
        }
      }
    }
  }
  
  private def loadManualSyms(): Unit = {
    forEachManualSymRec { msr =>
      extraSyms(msr.sym) = msr
    }
  }
  
  private def forEachManualSymRec(f: ManualSymRecord => Unit): Unit = {
    tryWithResource(zipFileTextBuf(MANUALSYMS_FILE)) { manualSymsIS =>
      forEachLine(manualSymsIS) { line =>
        val elems = line.split(",")
        val sym = elems(0)
        val targetNum = elems(1).toLong
        val targetOffset = elems(2).toLong
        val msr = ManualSymRecord(sym, targetNum, targetOffset)
        f(msr)
      }
    }
  }

  private def relocate(): Unit = {
    forEachReloc("global") { rr =>
      val FieldRelocRecord(num, fieldOffset, fieldKind, targetKind, targetNum, targetOffset, targetString) = rr
      val base = globalNumToAddr(num)
      relocField(base, fieldOffset, fieldKind, targetKind, targetNum, targetOffset, targetString)
    }

    forEachReloc("heap") { rr =>
      val FieldRelocRecord(num, fieldOffset, fieldKind, targetKind, targetNum, targetOffset, targetString) = rr
      val base = objNumToAddr(num)
      relocField(base, fieldOffset, fieldKind, targetKind, targetNum, targetOffset, targetString)
    }
  }

  private def forEachReloc(groupName: String)(f: FieldRelocRecord => Unit): Unit = {
    tryWithResource(zipFileTextBuf(groupName + FILEEXT_RELOC)) { relocIS =>
      forEachLine(relocIS) { line =>
        val elems = line.split(",", -1) // include the trailing empty string
        val num = elems(0).toLong
        val fieldOffset = elems(1).toLong
        val fieldKind = elems(2)
        val targetKind = elems(3)
        val targetNum = elems(4).toLong
        val targetOffset = elems(5).toLong
        val targetString = elems(6)
        val rr = FieldRelocRecord(num, fieldOffset, fieldKind, targetKind, targetNum, targetOffset, targetString)
        f(rr)
      }
    }
  }

  private def relocField(srcBase: Word, srcOffset: Word, fieldKind: String,
    targetKind: String, targetNum: Long, targetOffset: Word, targetSymbol: String): Unit = {
    val srcFieldAddr = srcBase + srcOffset
    fieldKind match {
      case AS_UPTR => {
        assert(targetKind != TO_FUNC, "BUG: uptr field referring to func")
        assert(targetKind != TO_HEAP, "BUG: uptr field referring to obj")
        val targetAddr = targetKind match {
          case TO_GLOBAL => getTargetMemBase(targetKind, targetNum) + targetOffset
          case TO_SYM    => getSymbolAddr(targetSymbol)
        }
        memorySupport.storeLong(srcFieldAddr, targetAddr)
      }
      case AS_REF => {
        assert(targetOffset == 0L)
        assert(targetKind != TO_FUNC, "BUG: ref field referring to function")
        assert(targetKind != TO_SYM, "BUG: ref field referring to symbol")
        val targetBase = getTargetMemBase(targetKind, targetNum)
        memorySupport.storeLong(srcFieldAddr, targetBase)
      }
      case AS_IREF => {
        assert(targetKind != TO_FUNC, "BUG: iref field referring to function")
        assert(targetKind != TO_SYM, "BUG: iref field referring to symbol")
        val targetBase = getTargetMemBase(targetKind, targetNum)
        val (irefBase, irefOffset) = targetKind match {
          case TO_GLOBAL => (0L, targetBase + targetOffset)
          case TO_HEAP   => (targetBase, targetOffset)
        }
        memorySupport.storeLong(srcFieldAddr, irefBase)
        memorySupport.storeLong(srcFieldAddr + WORD_SIZE_BYTES, irefOffset)
      }
      case AS_TAGREF => {
        assert(targetOffset == 0L)
        assert(targetKind != TO_FUNC, "BUG: tagref64 field referring to function")
        assert(targetKind != TO_SYM, "BUG: tagref64 field referring to symbol")
        val targetBase = getTargetMemBase(targetKind, targetNum)
        val oldTR64 = memorySupport.loadLong(srcFieldAddr)
        val oldTag = OpHelper.tr64ToTag(oldTR64)
        val newTR64 = OpHelper.refToTr64(targetBase, oldTag)
        memorySupport.storeLong(srcFieldAddr, newTR64)
      }
      case AS_FUNCREF => {
        assert(targetOffset == 0L)
        assert(targetKind == TO_FUNC, "BUG: funcref field referring to memory")
        assert(targetNum != 0L, "BUG: funcref target is 0")
        memorySupport.storeLong(srcFieldAddr, targetNum)
      }
    }
  }

  private def getTargetMemBase(targetKind: String, targetNum: Long): Word = {
    targetKind match {
      case TO_GLOBAL => globalNumToAddr(targetNum)
      case TO_HEAP   => objNumToAddr(targetNum)
    }
  }

  private def getSymbolAddr(targetSymbol: String): Word = {
    extraSyms.get(targetSymbol) match {
      // If there is a manual symbol (specified in makeBootImage), use it;
      case Some(msr) => globalNumToAddr(msr.targetNum) + msr.targetOffset
      // Otherwise, resolve the symbol natively
      case None      => microVM.nativeLibraryHolder.getSymbolAddress(targetSymbol)
    }
  }

  private def maybeCreatePrimordialThread(metaInfo: Map[String, String]): Unit = {
    metaInfo.get(KEY_INITFUNC).map(_.toInt) match {
      case Some(initFuncID) => {
        val func = microVM.globalBundle.funcNs(initFuncID)

        val allArgs = maybeAllArgs.getOrElse {
          throw new BootImageLoaderException("Boot image has a primordial thread, but the MicroVM instance is not created " +
            "with command line arguments. Please use the tools/runmu.sh command line utility to run boot images that has " +
            "primordial threads. In realistic scenarios, the primordial thread in the boot image is supposed to be the " +
            "entry point of a program, thus primordial threads should only exist in STAND-ALONE EXECUTABLE boot iamges.")
        }

        val threadLocalAddr = metaInfo.get(KEY_THREADLOCAL).map(_.toLong).map { num =>
          objNumToAddr(num)
        }.getOrElse(0L)

        tryWithResource(heap.makeMutator(PRIMORDIAL_INIT_MUTATOR_NAME)) { mutator =>
          logger.info("Creating primordial thread using function %d...".format(initFuncID))
          val func = microVM.globalBundle.funcNs(initFuncID)
          logger.info("Stack-bottom function name: %s".format(func.name.getOrElse("(no name)")))

          logger.info("Thread-local obj address: %d 0x%x".format(threadLocalAddr, threadLocalAddr))
          logger.info {
            if (threadLocalAddr == 0L) {
              "The thread-local obj ref is NULL"
            } else {
              val tag = HeaderUtils.getTag(threadLocalAddr)
              val ty = HeaderUtils.getType(microVM, tag)
              "Thread-local obj type: %s %s".format(ty.repr, ty)
            }
          }

          val stack = microVM.threadStackManager.newStack(func, mutator)
          logger.info("All arguments: %s".format(allArgs))
          val nativeArgv = new NativeArgv(allArgs)
          microVM.maybeNativeArgv = Some(nativeArgv)
          val argcBox = BoxInt(nativeArgv.argc)
          val argvBox = BoxPointer(nativeArgv.argv)
          val htr = HowToResume.PassValues(Seq(argcBox, argvBox))
          val thread = microVM.threadStackManager.newThread(stack, threadLocalAddr, htr)
        }
      }
      case None => {
        logger.info("Boot image does not have a primordial thread (not an error).")
      }
    }
  }
}

class AlignedInputStream(is: InputStream) extends Closeable {
  var position: Long = 0L

  private val BUFFER_SZ = 16384
  private val buffer = new Array[Byte](BUFFER_SZ)

  def seekTo(newPosition: Long): Unit = {
    assert(newPosition >= position)

    if (newPosition > position) {
      is.skip(newPosition - position)
      position = newPosition
    }
  }

  def readToMemory(addr: Long, size: Long): Unit = {
    var nextAddr = addr
    var remain = size

    while (remain > BUFFER_SZ) {
      transfer(nextAddr, BUFFER_SZ)
      nextAddr += BUFFER_SZ
      remain -= BUFFER_SZ
    }

    if (remain > 0) {
      transfer(nextAddr, remain.toInt)
    }

    position += size
  }

  private def transfer(addr: Long, size: Int): Unit = {
    var actual = is.read(buffer, 0, size)
    while (actual < size) {
      actual += is.read(buffer, actual, size - actual)
    }
    NativeSupport.theMemory.put(addr, buffer, 0, size)
  }

  def close(): Unit = {
    is.close()
  }
}