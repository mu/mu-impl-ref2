package uvm.refimpl.bootimg

import uvm.refimpl.UvmRuntimeException

class BootImageBuilderException(message: String = null, cause: Throwable = null) extends UvmRuntimeException(message, cause)
class BootImageLoaderException(message: String = null, cause: Throwable = null) extends UvmRuntimeException(message, cause)
