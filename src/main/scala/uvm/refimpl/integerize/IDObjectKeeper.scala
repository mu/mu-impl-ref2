package uvm.refimpl.integerize

import scala.collection.mutable.HashMap

import uvm.refimpl.MuInternalID
import uvm.refimpl.UvmRuntimeException
import uvm.refimpl.UvmUnknownIDException
import uvm.utils.IDFactory

/**
 * An object with this trait can be uniquely identified within a MicroVM using an Int ID.
 * <p>
 * This is different from Identified because the ID in this context is used as opaque references to these objects. Currently, these
 * objects are threads, stacks and frame cursors. These are referred by opaque references which, in the memory are represented as numerical
 * IDs.
 */
trait HasID {
  def id: MuInternalID
}

/**
 * Keep HasID objects and allow looking them up by their IDs.
 *
 * @param kind A name that indicates what kind of object it holds. For debugging.
 */
class IDObjectKeeper[T <: HasID](val kind: String) {
  val registry = new HashMap[Int, T]()
  val idFactory = IDFactory.newOneBasedIDFactory()

  /** Get an object by its ID. */
  def apply(id: MuInternalID) = try {
    registry.apply(id)
  } catch {
    case e: NoSuchElementException => throw new UvmUnknownIDException(
        "Attempted to lookup %s object by ID %d, but it is not found".format(kind, id))
  }

  /** Get an object by its ID, handle non-existing cases. */
  def get(id: MuInternalID) = registry.get(id)

  /** Add an object to the registry. */
  def put(obj: T): Unit = {
    if (registry.put(obj.id, obj).isDefined) {
      throw new UvmRuntimeException("%s of ID %s already exists.".format(kind, obj.id))
    }
  }

  /** Iterate through all objects. */
  def values: Iterable[T] = registry.values

  /** Remove an object from the registry. */
  def remove(obj: T): Unit = {
    val old = registry.remove(obj.id)
    if (old.isDefined && !(old.get eq obj)) {
      throw new UvmRuntimeException("The %s removed is not the same object as the argument. ID: %d".format(kind, obj.id))
    }
  }

  /** Create an ID for a new object of this type. */
  def newID() = idFactory.getID()
}
