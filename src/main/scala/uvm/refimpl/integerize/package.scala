package uvm.refimpl

import uvm.Function
import uvm.refimpl.itpr.InterpreterThread
import uvm.refimpl.itpr.FrameCursor
import uvm.refimpl.itpr.InterpreterStack
import uvm.ir.irbuilder.IRBuilder

package object integerize {
  implicit object FunctionIntegerizer extends CanBeIntegerized[Function] {
    def _integerize(obj: Function)(implicit microVM: MicroVM): Word = obj.id.toLong
    def _deIntegerize(num: Word)(implicit microVM: MicroVM): Function = microVM.globalBundle.funcNs(num.toInt)
  }

  implicit object ThreadIntegerizer extends CanBeIntegerized[InterpreterThread] {
    def _integerize(obj: InterpreterThread)(implicit microVM: MicroVM): Word = obj.id.toLong
    def _deIntegerize(num: Word)(implicit microVM: MicroVM): InterpreterThread = microVM.threadStackManager.threadRegistry(num.toInt)
  }

  implicit object StackIntegerizer extends CanBeIntegerized[InterpreterStack] {
    def _integerize(obj: InterpreterStack)(implicit microVM: MicroVM): Word = obj.id.toLong
    def _deIntegerize(num: Word)(implicit microVM: MicroVM): InterpreterStack = microVM.threadStackManager.stackRegistry(num.toInt)
  }

  implicit object FrameCursorIntegerizer extends CanBeIntegerized[FrameCursor] {
    def _integerize(obj: FrameCursor)(implicit microVM: MicroVM): Word = obj.id.toLong
    def _deIntegerize(num: Word)(implicit microVM: MicroVM): FrameCursor = microVM.threadStackManager.frameCursorRegistry(num.toInt)
  }

  implicit object IRBuilderIntegerizer extends CanBeIntegerized[IRBuilder] {
    def _integerize(obj: IRBuilder)(implicit microVM: MicroVM): Word = obj.id.toLong
    def _deIntegerize(num: Word)(implicit microVM: MicroVM): IRBuilder = microVM.irBuilderRegistry(num.toInt)
  }
}