package uvm.refimpl.integerize

import scala.annotation.implicitNotFound

import uvm.refimpl._

@implicitNotFound("Mu entity type ${T} cannot be represented as an integer thus cannot be loaded/stored from the memory.")
trait CanBeIntegerized[T] {
  protected def _integerize(obj: T)(implicit microVM: MicroVM): Word
  protected def _deIntegerize(num: Word)(implicit microVM: MicroVM): T

  def integerize(obj: Option[T])(implicit microVM: MicroVM): Word =
    obj.map(_integerize).getOrElse(0L)
  def deIntegerize(num: Word)(implicit microVM: MicroVM): Option[T] = {
    if (num == 0L) {
      None
    } else {
      Some(_deIntegerize(num))
    }
  }
}