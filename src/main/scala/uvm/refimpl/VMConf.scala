/**
 * This class allows the MicroVM to be configured from a text string. It increases the flexibility of language bindings.
 * Other programming languages can set heap/global sizes and logging via this interface. It is less static than a fixed
 * MicroVM constructor signature, but more options can be added/removed without changing the C binding API.
 */
package uvm.refimpl

import scala.collection.mutable.ArrayBuffer

import uvm.refimpl.mem.TypeSizes._
import org.slf4j.LoggerFactory
import ch.qos.logback.classic.joran.JoranConfigurator
import ch.qos.logback.classic.LoggerContext

class VMOption[+T](
    val name: String,
    val desc: String,
    val parser: String => T,
    val default: T) {
  def maybeParse(maybeStr: Option[String]): T = {
    maybeStr.map(parser).getOrElse(default)
  }
  def apply(kvMap: Map[String, String]): T = {
    maybeParse(kvMap.get(name))
  }
}

object VMConfParser {
  def sizeWithSuffix(str: String): Long = {
    val multiplier = str.last match {
      case 'K' => Some(1024L)
      case 'M' => Some(1024L * 1024L)
      case 'G' => Some(1024L * 1024L * 1024L)
      case 'T' => Some(1024L * 1024L * 1024L * 1024L)
      case _   => None
    }

    multiplier match {
      case Some(m) => str.init.toLong * m
      case None    => str.toLong
    }
  }

  def parseBoolean(str: String): Boolean = {
    str.toLowerCase().toBoolean
  }

  def parseColonSeparatedList(str: String): Seq[String] = {
    str.split(":")
  }

  def parseMaybeString(str: String): Option[String] = {
    Some(str)
  }

  val options = new ArrayBuffer[VMOption[Any]]

  def opt[T](
    name: String,
    desc: String,
    parser: String => T,
    default: T): VMOption[T] = {
    val option = new VMOption[T](name, desc, parser, default)
    options += option
    option
  }

  val sosSize = opt[Long](
    name = "sosSize",
    desc = """The size of the small object space in bytes. May have suffix K, M, G or T. 1K = 1024B""",
    parser = sizeWithSuffix,
    default = VMConf.DEFAULT_CONF.sosSize)

  val losSize = opt[Long](
    name = "losSize",
    desc = """The size of the large object space in bytes. May have suffix K, M, G or T. 1K = 1024B""",
    parser = sizeWithSuffix,
    default = VMConf.DEFAULT_CONF.losSize)

  val globalSize = opt[Long](
    name = "globalSize",
    desc = """The size of the large object space in bytes. May have suffix K, M, G or T. 1K = 1024B""",
    parser = sizeWithSuffix,
    default = VMConf.DEFAULT_CONF.globalSize)

  val stackSize = opt[Long](
    name = "stackSize",
    desc = """The size of each stack in bytes. May have suffix K, M, G or T. 1K = 1024B""",
    parser = sizeWithSuffix,
    default = VMConf.DEFAULT_CONF.stackSize)

  val dumpBundle = opt[Boolean](
    name = "dumpBundle",
    desc = """Dump the bundle when a bundle is loaded.""",
    parser = parseBoolean,
    default = VMConf.DEFAULT_CONF.dumpBundle)

  val staticCheck = opt[Boolean](
    name = "staticCheck",
    desc = """Run static checker after each bundle is loaded.""",
    parser = parseBoolean,
    default = VMConf.DEFAULT_CONF.staticCheck)

  val sourceInfo = opt[Boolean](
    name = "sourceInfo",
    desc = """Provide line/column info in Mu IR when errors occur. May significantly slow down parsing!!!""",
    parser = parseBoolean,
    default = VMConf.DEFAULT_CONF.sourceInfo)

  val automagicReloc = opt[Boolean](
    name = "automagicReloc",
    desc = """'Automagic' relocation. Affects boot image building, but not loading.""",
    parser = parseBoolean,
    default = VMConf.DEFAULT_CONF.automagicReloc)

  val extraLibs = opt[Seq[String]](
    name = "extraLibs",
    desc = """Extra libraries to load when starting the micro VM. This is a colon-separated list of libraries.
Each is passed to the dlsym function.""",
    parser = parseColonSeparatedList,
    default = VMConf.DEFAULT_CONF.extraLibs)

  val bootImg = opt[Option[String]](
    name = "bootImg",
    desc = """The path to the boot image.""",
    parser = parseMaybeString,
    default = VMConf.DEFAULT_CONF.bootImg)

  val logbackConfig = opt[String](
    name = "logbackConfig",
    desc = """The logback XML configuration file""",
    parser = identity,
    default = "")

  val vmLog = opt[String](
    name = "vmLog",
    desc = """The log level of the micro VM. Can be ALL, TRACE, DEBUG, INFO, WARN, ERROR, OFF""",
    parser = identity,
    default = "WARN")

  val gcLog = opt[String](
    name = "gcLog",
    desc = """The log level of the garbage collector. Can be ALL, TRACE, DEBUG, INFO, WARN, ERROR, OFF""",
    parser = identity,
    default = "ERROR")

  val uPtrHack = opt[Boolean](
    name = "uPtrHack",
    desc = """Allow memory locations of general reference types to be accessed by uptr""",
    parser = parseBoolean,
    default = false)
}

object VMConf {
  val DEFAULT_CONF = new VMConf()

  val ReComment = """^\s*#.*$""".r
  val ReBlank = """^\s*$""".r
  val ReConf = """^\s*(\w+)=(\S+)\s*$""".r

  def apply(confStr: String): VMConf = {
    val kvs = confStr.lines.map {
      case ReComment()        => None
      case ReBlank()          => None
      case ReConf(key, value) => Some((key, value))
    }.flatten

    val map = kvs.toMap

    apply(map)
  }

  def apply(kvMap: Map[String, String]): VMConf = {
    configureLog(kvMap)

    new VMConf(
      sosSize = VMConfParser.sosSize(kvMap),
      losSize = VMConfParser.losSize(kvMap),
      globalSize = VMConfParser.globalSize(kvMap),
      stackSize = VMConfParser.stackSize(kvMap),
      dumpBundle = VMConfParser.dumpBundle(kvMap),
      staticCheck = VMConfParser.staticCheck(kvMap),
      sourceInfo = VMConfParser.sourceInfo(kvMap),
      automagicReloc = VMConfParser.automagicReloc(kvMap),
      extraLibs = VMConfParser.extraLibs(kvMap),
      bootImg = VMConfParser.bootImg(kvMap),
      uPtrHack = VMConfParser.uPtrHack(kvMap))
  }

  def configureLog(kvMap: Map[String, String]): Unit = {
    kvMap.get("logbackConfig") foreach { filename =>
      val context = LoggerFactory.getILoggerFactory().asInstanceOf[LoggerContext]
      context.reset()

      val configurator = new JoranConfigurator();
      configurator.setContext(context);
      configurator.doConfigure(filename);
    }

    kvMap.get("vmLog") foreach { value => setLog("uvm", value) }
    kvMap.get("gcLog") foreach { value => setLog("uvm.refimpl.mem", value) }
  }

  def setLog(name: String, levelStr: String): Unit = {
    import ch.qos.logback.classic.{ Level, Logger => LLogger }
    import ch.qos.logback.classic.Level._
    import org.slf4j.LoggerFactory

    val level = Level.toLevel(levelStr)

    LoggerFactory.getLogger(name).asInstanceOf[LLogger].setLevel(level)
  }
}

class VMConf(
  val sosSize: Word = MicroVM.DEFAULT_SOS_SIZE,
  val losSize: Word = MicroVM.DEFAULT_LOS_SIZE,
  val globalSize: Word = MicroVM.DEFAULT_GLOBAL_SIZE,
  val stackSize: Word = MicroVM.DEFAULT_STACK_SIZE,
  val dumpBundle: Boolean = false,
  val staticCheck: Boolean = true,
  val sourceInfo: Boolean = false,
  val automagicReloc: Boolean = false,
  val extraLibs: Seq[String] = Seq(),
  val bootImg: Option[String] = None,
  val uPtrHack: Boolean = false)

