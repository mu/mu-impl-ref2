package uvm.refimpl.cmdline

import uvm.refimpl.WORD_SIZE_BYTES
import uvm.refimpl.nat.NativeSupport

class NativeArgv(args: Seq[String]) extends AutoCloseable {
  val argc = args.size

  private val argvSB = new StringBuilder()
  private val argvOffs = new Array[Long](argc)
  
  for ((arg, i) <- (args).zipWithIndex) {
    argvOffs(i) = argvSB.length
    argvSB ++= arg += '\u0000'
  }

  val cArgvMem = NativeSupport.allocateManual(argvOffs.length * WORD_SIZE_BYTES.toInt)
  val cArgvBufMem = NativeSupport.allocateManual(argvSB.length)

  val cArgv = cArgvMem.asJnrPointer()
  val cArgvBuf = cArgvBufMem.asJnrPointer()

  for (i <- 0 until argvOffs.length) {
    cArgv.putAddress(i * WORD_SIZE_BYTES, cArgvBuf.address() + argvOffs(i))
  }
  for (i <- 0 until argvSB.length) {
    cArgvBuf.putByte(i, argvSB.charAt(i).toByte)    // Assume single-byte encoding
  }
  
  val argv = cArgv.address()
  
  override def close(): Unit = {
    cArgvBufMem.close()
    cArgvMem.close()
  }
}