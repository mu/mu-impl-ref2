package uvm.refimpl

import scala.collection.mutable.HashMap
import uvm.ssavariables.Instruction

object IndentableStringBuilder {
  def apply(indentSize: Int) = new IndentableStringBuilder(indentSize)
  def apply() = new IndentableStringBuilder(2)
}

class IndentableStringBuilder private (indentSize: Int) {
  val sb = new StringBuilder()
  
  var curIndent = 0
  
  def indent(): IndentableStringBuilder = {
    curIndent += indentSize
    this
  }
  
  def dedent(): IndentableStringBuilder = {
    curIndent -= indentSize
    this
  }
  
  def addLine(line: String): IndentableStringBuilder = {
    for (_ <- 0 until curIndent) {
      sb += ' '
    }
    sb ++= line += '\n'
    this
  }
  
  def blockIndent(f: => Unit): Unit = {
    indent()
    f
    dedent()
  }
  
  override def toString() = sb.toString()
}

class VMStats {
  var printCount: Long = 0L

  var exceptionsThrown: Long = 0L
  var peiCalled: Long = 0L
  var allocaExecuted: Long = 0L
  var bytesAllocatedHeap: Long = 0L

  val heapAllocSizeMap: HashMap[Long, Long] = new HashMap()
  val instExecCount: HashMap[String, Long] = new HashMap()

  def onInstExec(i: String): Unit = {
    val oldCount = instExecCount.getOrElse(i, 0L)
    val newCount = oldCount + 1
    instExecCount.update(i, newCount)
  }

  def onHeapAlloc(sz: Long): Unit = {
    bytesAllocatedHeap += sz

    val oldCount = heapAllocSizeMap.getOrElse(sz, 0L)
    val newCount = oldCount + 1
    heapAllocSizeMap.update(sz, newCount)
  }

  def getStatsString(userNum: Long): String = {
    printCount += 1

    val sb = IndentableStringBuilder()
    
    sb addLine s"======== VM Statistics Begin | printCount=${printCount} userNum=${userNum}"
    
    sb blockIndent {
      sb addLine "Exceptions thrown: %d".format(exceptionsThrown)
      sb addLine "PEI called: %d".format(peiCalled)
      sb addLine "ALLOCA and ALLOCAHYBRID executed: %d".format(allocaExecuted)

      sb addLine "Instructions executed:"
      sb blockIndent {
        for ((i, c) <- instExecCount.toSeq.sortBy(_._2).reverse) {
          sb addLine "%s: %d".format(i, c)
        }
      }

      sb addLine "Bytes allocated on the heap (only small obj space): %d".format(bytesAllocatedHeap)
      sb addLine "Heap object size : allocation count:"
      sb blockIndent {
        for ((i, c) <- heapAllocSizeMap.toSeq.sortBy(_._1)) {
          sb addLine "%d: %d".format(i, c)
        }
      }
    }
    sb addLine s"======== VM Statistics End | printCount=${printCount} userNum=${userNum}"

    sb.toString()
  }

  def printStats(userNum: Long): Unit = {
    print(getStatsString(userNum))
  }

  def clearStats(): Unit = {
    exceptionsThrown = 0L
    peiCalled = 0L
    allocaExecuted = 0L
    bytesAllocatedHeap = 0L

    heapAllocSizeMap.clear()
    instExecCount.clear()
  }

}