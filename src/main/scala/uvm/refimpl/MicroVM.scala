package uvm.refimpl

import java.io.OutputStreamWriter
import java.io.Writer

import scala.collection.mutable.HashSet

import uvm._
import uvm.ir.irbuilder.IRBuilder
import uvm.ir.irbuilder.IRBuilderListener
import uvm.ir.textinput.UIRTextReader
import uvm.ir.textoutput.BundleSerializer
import uvm.ir.textoutput.DebugBundleSerializer
import uvm.refimpl.bootimg.BootImageBuilder
import uvm.refimpl.bootimg.BootImageLoader
import uvm.refimpl.hail.HailScriptLoader
import uvm.refimpl.integerize.IDObjectKeeper
import uvm.refimpl.itpr._
import uvm.refimpl.mem._
import uvm.refimpl.nat.NativeCallHelper
import uvm.refimpl.nat.NativeLibraryHolder
import uvm.staticanalysis.StaticAnalyzer
import uvm.utils.IDFactory
import uvm.utils.WithUtils.tryWithResource
import uvm.refimpl.bootimg.PrimordialInfo
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory
import uvm.refimpl.cmdline.NativeArgv

object MicroVM {
  val logger = Logger(LoggerFactory.getLogger(getClass.getName))

  val DEFAULT_SOS_SIZE: Word = 2L * 1024L * 1024L; // 2MiB
  val DEFAULT_LOS_SIZE: Word = 2L * 1024L * 1024L; // 2MiB
  val DEFAULT_GLOBAL_SIZE: Word = 1L * 1024L * 1024L; // 1MiB
  val DEFAULT_STACK_SIZE: Word = 63L * 1024L; // 60KiB per stack

  def apply(): MicroVM = {
    val vmConf = new VMConf()
    MicroVM(vmConf)
  }

  def apply(confStr: String): MicroVM = {
    val vmConf = VMConf(confStr)
    MicroVM(vmConf)
  }

  def apply(confStr: String, allArgs: Seq[String]): MicroVM = {
    val vmConf = VMConf(confStr)
    MicroVM(vmConf, allArgs)
  }
  
  def apply(vmConf: VMConf): MicroVM = {
    new MicroVM(vmConf, None)
  }
  
  def apply(vmConf: VMConf, allArgs: Seq[String]): MicroVM = {
    new MicroVM(vmConf, Some(allArgs))
  }
}

class MicroVM private (val vmConf: VMConf, val allArgs: Option[Seq[String]]) extends IRBuilderListener with AutoCloseable {
  // implicitly injected resources
  private implicit val microVM = this
  
  val stats = new VMStats()

  val staticAnalyzer = new StaticAnalyzer()

  val globalBundle = new GlobalBundle()
  val constantPool = new ConstantPool()

  /// Initialise the global bundle and constant pool with (private) pre-loaded stuff
  {
    // VOID, BYTE, BYTE_ARRAY: The micro VM allocates stacks on the heap in the large object space.
    // It is represented as a big chunk of byte array.
    // So the GC must know about this type because the GC looks up the globalBundle for types.

    // BYTES, BYTES_R, REFS, REFS_R: Types for the @uvm.meta.* common instructions.

    import staticAnalyzer.predefinedEntities._
    for (ty <- Seq(VOID, BYTE, BYTE_ARRAY, BYTES, BYTES_R, REFS, REFS_R)) {
      globalBundle.typeNs.add(ty)
    }

    // Some internal constants needed by the HAIL loader

    for (c <- Seq(NULL_REF_VOID, NULL_IREF_VOID, NULL_FUNCREF_VV, NULL_THREADREF, NULL_STACKREF)) {
      globalBundle.constantNs.add(c)
      constantPool.addGlobalVar(c)
    }

    // Some types required by the RunMu class

    for (ty <- Seq(C_INT, C_CHAR, C_CHARP, C_CHARPP)) {
      globalBundle.typeNs.add(ty)
    }
    
    // Perform type inference on all preloaded s
    staticAnalyzer.typeInferer.inferBundle(globalBundle)
  }
  
  /**
   * A hack that allows general reference types to be accessed by pointers.
   * <p>
   * By default (true), memory locations of general reference types can only be accessed via iref.
   * This enforcement can be disabled as a workaround for the current mu-client-pypy project.
   */
  val enforceRefTypeNoPointerAccess = !vmConf.uPtrHack
  
  if (!enforceRefTypeNoPointerAccess) {
    MicroVM.logger.warn("Allowing memory locations of general reference types to be accessed by uptr.")
  }
  
  val memoryManager = new MemoryManager(vmConf)

  private implicit val memorySupport = memoryManager.memorySupport

  val nativeLibraryHolder = NativeLibraryHolder(vmConf.extraLibs: _*)

  implicit val nativeCallHelper = new NativeCallHelper()
  
  var maybeNativeArgv: Option[NativeArgv] = None

  val threadStackManager = new ThreadStackManager()

  val trapManager = new TrapManager()
  val contexts = new HashSet[MuCtx]()

  val idFactory = IDFactory.newClientIDFactory()
  val irBuilderRegistry = new IDObjectKeeper[IRBuilder]("IR builder")
  val irReader = new UIRTextReader(idFactory, recordSourceInfo = vmConf.sourceInfo)
  val hailScriptLoader = new HailScriptLoader(recordSourceInfo = vmConf.sourceInfo)

  val bootImageBuilder = new BootImageBuilder()
  
  /// Bundle building stuff
  
  def newIRBuilder(): IRBuilder = {
    val irBuilder = new IRBuilder(irBuilderRegistry.newID(), globalBundle, idFactory, Some(this))
    irBuilderRegistry.put(irBuilder)
    irBuilder
  }
  
  override def onBundleLoad(irBuilder: IRBuilder, bundle: TrantientBundle): Unit = {
    addBundle(bundle)
    irBuilderRegistry.remove(irBuilder)
  }
  
  override def onBundleAbort(irBuilder: IRBuilder): Unit = {
    irBuilderRegistry.remove(irBuilder)
  }

  /**
   * Add things from a bundle to the Micro VM.
   */
  def addBundle(bundle: TrantientBundle) {
    // Make sure all SSA variables have undergone type inference
    staticAnalyzer.typeInferer.inferBundle(bundle)

    if (vmConf.dumpBundle) {
      val dbs = new DebugBundleSerializer(bundle)
      dbs.writeUIR(new OutputStreamWriter(System.err))
    }

    if (vmConf.staticCheck) {
      staticAnalyzer.bundleChecker.checkBundle(bundle, Some(globalBundle))
    }

    globalBundle.merge(bundle);

    for (gc <- bundle.globalCellNs.all) {
      memoryManager.globalMemory.addGlobalCell(gc)
    }
    for (ef <- bundle.expFuncNs.all) {
      nativeCallHelper.exposeFuncStatic(ef)
    }
    // Must allocate the memory and expose the functions before making constants.
    for (g <- bundle.globalVarNs.all) {
      constantPool.addGlobalVar(g)
    }
  }

  private val contextIDFactory = IDFactory.newOneBasedIDFactory()

  /**
   * Create a new MuCtx. Part of the API.
   */
  def newContext(): MuCtx = newContext("user")

  /**
   * Create a new MuCtx. Extended to add a name for debugging.
   */
  def newContext(name: String): MuCtx = {
    val id = contextIDFactory.getID()
    val mutator = microVM.memoryManager.heap.makeMutator("MuCtx-%d-%s".format(id, name)) // This may trigger GC
    val ca = new MuCtx(id, mutator)
    contexts.add(ca)
    ca
  }
  /**
   * Given a name, get the ID of an identified entity.
   */
  def idOf(name: MuName): MuID = globalBundle.allNs(name).id

  /**
   * Given an ID, get the name of an identified entity.
   */
  def nameOf(id: MuID): MuName = globalBundle.allNs(id).name.get

  /**
   * Set the trap handler.
   */
  def setTrapHandler(trapHandler: TrapHandler): Unit = {
    trapManager.trapHandler = trapHandler
  }

  /**
   * Make boot image.
   */
  def makeBootImage(whiteList: Seq[MuID], outputFile: String): Unit = {
    val whiteListObjs = whiteList.map(globalBundle.topLevelNs.apply)

    bootImageBuilder.makeBootImage(whiteListObjs, PrimordialInfo.NO_PRIMORDIAL, Seq(), Seq(), outputFile)
  }

  /**
   * Print the global bundle to the writer w.
   */
  def debugPrintGlobalBundle(w: Writer): Unit = {
    val bs = new BundleSerializer(globalBundle, globalBundle.allTopLevels().toSet)
    bs.writeUIR(w)
  }

  /**
   * Print the global bundle to stderr.
   */
  def debugPrintGlobalBundle(): Unit = {
    debugPrintGlobalBundle(new OutputStreamWriter(System.err))
  }

  /**
   * Load from a boot image.
   */
  def loadBootImage(file: String): Unit = {
    tryWithResource(new BootImageLoader(file, allArgs)) { bil =>
      bil.load()
    }
  }

  /**
   * Execute. This is the external pusher of the execution.
   */
  def execute(): Unit = {
    threadStackManager.execute()
    
    MicroVM.logger.info("Execution statistics:")
    MicroVM.logger.info(stats.getStatsString(0))
  }

  // Automatically load the boot image if provided
  {
    vmConf.bootImg foreach { fileName =>
      loadBootImage(fileName)
    }
  }
  
  override def close(): Unit = {
    maybeNativeArgv.foreach(_.close())
    threadStackManager.close()
    nativeLibraryHolder.close()
    memoryManager.close()
  }
}