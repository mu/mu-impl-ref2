package uvm.types

import uvm._

abstract class Type extends TopLevel {
  override final def toString: String = Type.prettyPrint(this)
}

abstract trait AbstractEQComparableType extends Type
abstract trait AbstractULTComparableType extends Type

abstract class AbstractFPType extends Type

abstract class AbstractGenRefType extends Type

abstract class AbstractRefType extends AbstractGenRefType {
  def ty: Type
}

abstract class AbstractObjRefType extends AbstractRefType

abstract class AbstractOpaqueRefType extends AbstractGenRefType with AbstractEQComparableType

abstract class AbstractCompositeType extends Type

abstract class AbstractStructType extends AbstractCompositeType {
  def fieldTys: Seq[Type]
}

abstract class AbstractSeqType extends AbstractCompositeType {
  def elemTy: Type
  def len: Long
}

abstract class AbstractPointerType extends Type with AbstractEQComparableType with AbstractULTComparableType

case class TypeInt(var length: Int) extends Type with AbstractEQComparableType with AbstractULTComparableType
case class TypeFloat() extends AbstractFPType
case class TypeDouble() extends AbstractFPType
case class TypeUPtr(var ty: Type) extends AbstractPointerType
case class TypeUFuncPtr(var sig: FuncSig) extends AbstractPointerType
case class TypeStruct(var fieldTys: Seq[Type]) extends AbstractStructType
case class TypeHybrid(var fieldTys: Seq[Type], var varTy: Type) extends AbstractStructType
case class TypeArray(var elemTy: Type, var len: Long) extends AbstractSeqType
case class TypeVector(var elemTy: Type, var len: Long) extends AbstractSeqType
case class TypeVoid() extends Type
case class TypeRef(var ty: Type) extends AbstractObjRefType with AbstractEQComparableType
case class TypeIRef(var ty: Type) extends AbstractRefType with AbstractEQComparableType with AbstractULTComparableType
case class TypeWeakRef(var ty: Type) extends AbstractObjRefType
case class TypeTagRef64() extends Type with AbstractEQComparableType
case class TypeFuncRef(var sig: FuncSig) extends AbstractOpaqueRefType
case class TypeThreadRef() extends AbstractOpaqueRefType
case class TypeStackRef() extends AbstractOpaqueRefType
case class TypeFrameCursorRef() extends AbstractOpaqueRefType
case class TypeIRBuilderRef() extends AbstractOpaqueRefType

object Type {
  def prettyPrint(ty: Type): String = ty match {
    case TypeInt(length)               => "int<%d>".format(length)
    case TypeFloat()                   => "float"
    case TypeDouble()                  => "double"
    case TypeUPtr(ty)                  => "uptr<%s>".format(ty.repr)
    case TypeUFuncPtr(sig)             => "ufuncptr<%s>".format(sig.repr)
    case TypeStruct(fieldTys)          => "struct<%s>".format(fieldTys.map(_.repr).mkString(" "))
    case TypeHybrid(fieldTys, varPart) => "hybrid<%s %s>".format(fieldTys.map(_.repr).mkString(" "), varPart.repr)
    case TypeArray(elemTy, len)        => "array<%s %d>".format(elemTy.repr, len)
    case TypeVector(elemTy, len)       => "vector<%s %d>".format(elemTy.repr, len)
    case TypeVoid()                    => "void"
    case TypeRef(ty)                   => "ref<%s>".format(ty.repr)
    case TypeIRef(ty)                  => "iref<%s>".format(ty.repr)
    case TypeWeakRef(ty)               => "weakref<%s>".format(ty.repr)
    case TypeTagRef64()                => "tagref64"
    case TypeFuncRef(sig)              => "funcref<%s>".format(sig.repr)
    case TypeThreadRef()               => "threadref"
    case TypeStackRef()                => "stackref"
    case TypeFrameCursorRef()          => "framecursorref"
    case TypeIRBuilderRef()            => "irbuilderref"
    case _                             => "unknown type " + ty.getClass.getName
  }
}
