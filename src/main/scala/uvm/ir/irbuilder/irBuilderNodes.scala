package uvm.ir.irbuilder

import uvm.MuID

object IRBuilderNode {
  type MuTypeNode = MuID
  type MuFuncSigNode = MuID
  type MuVarNode = MuID
  type MuGlobalVarNode = MuID
  type MuLocalVarNode = MuID
  type MuConstNode = MuID
  type MuConstIntNode = MuID
  type MuFuncNode = MuID
  type MuFuncVerNode = MuID
  type MuBBNode = MuID
  type MuInstNode = MuID
  type MuDestClause = MuID
  type MuExcClause = MuID
  type MuKeepaliveClause = MuID
  type MuCurStackClause = MuID
  type MuNewStackClause = MuID
  type MuWPID = MuID
  type MuFlag = Int
  //type MuBinOptr = MuFlag
  //type MuCmpOptr = MuFlag
  //type MuConvOptr = MuFlag
  //type MuMemOrd = MuFlag
  //type MuAtomicRMWOptr  = MuFlag
  //type MuCallConv = MuFlag
  //type MuCommInst = MuFlag
}

import IRBuilderNode._
import uvm.ssavariables._
import uvm.comminsts.CommInst

abstract class IRBuilderNode {
  def id: MuID
}

// Categorise by phase
abstract class TypeSigLike extends IRBuilderNode
abstract class OtherTopLevelNode extends IRBuilderNode

// Categorize by namespace (in uvm.Bundle)
abstract class IRTypeNode extends TypeSigLike
abstract class IRSigNode extends TypeSigLike
abstract class IRConstNode extends OtherTopLevelNode

// Other nodes with refimpl-level superclasses
abstract class IRInstNode extends IRBuilderNode
abstract class IRCurStackClauseNode extends IRBuilderNode
abstract class IRNewStackClauseNode extends IRBuilderNode

// EXT:BEGIN:IRBUILDER_NODES

// format: OFF
case class NodeTypeInt           (id: MuID, len: Int)                                     extends IRTypeNode
case class NodeTypeFloat         (id: MuID)                                               extends IRTypeNode
case class NodeTypeDouble        (id: MuID)                                               extends IRTypeNode
case class NodeTypeUPtr          (id: MuID, ty: MuTypeNode)                               extends IRTypeNode
case class NodeTypeUFuncPtr      (id: MuID, sig: MuFuncSigNode)                           extends IRTypeNode

case class NodeTypeStruct        (id: MuID, fieldTys: Seq[MuTypeNode])                    extends IRTypeNode
case class NodeTypeHybrid        (id: MuID, fixedTys: Seq[MuTypeNode], varTy: MuTypeNode) extends IRTypeNode
case class NodeTypeArray         (id: MuID, elemTy: MuTypeNode, len: Long)                extends IRTypeNode
case class NodeTypeVector        (id: MuID, elemTy: MuTypeNode, len: Long)                extends IRTypeNode
case class NodeTypeVoid          (id: MuID)                                               extends IRTypeNode

case class NodeTypeRef           (id: MuID, ty: MuTypeNode)                               extends IRTypeNode
case class NodeTypeIRef          (id: MuID, ty: MuTypeNode)                               extends IRTypeNode
case class NodeTypeWeakRef       (id: MuID, ty: MuTypeNode)                               extends IRTypeNode
case class NodeTypeFuncRef       (id: MuID, sig: MuFuncSigNode)                           extends IRTypeNode

case class NodeTypeTagRef64      (id: MuID)                                               extends IRTypeNode
case class NodeTypeThreadRef     (id: MuID)                                               extends IRTypeNode
case class NodeTypeStackRef      (id: MuID)                                               extends IRTypeNode
case class NodeTypeFrameCursorRef(id: MuID)                                               extends IRTypeNode
case class NodeTypeIRBuilderRef  (id: MuID)                                               extends IRTypeNode
// format: ON

case class NodeFuncSig(id: MuID, paramTys: Seq[MuTypeNode], retTys: Seq[MuTypeNode]) extends IRSigNode
  
// format: OFF
case class NodeConstInt   (id: MuID, ty: MuTypeNode, value: BigInt)               extends IRConstNode
case class NodeConstFloat (id: MuID, ty: MuTypeNode, value: Float)                extends IRConstNode
case class NodeConstDouble(id: MuID, ty: MuTypeNode, value: Double)               extends IRConstNode
case class NodeConstNull  (id: MuID, ty: MuTypeNode)                              extends IRConstNode
case class NodeConstSeq   (id: MuID, ty: MuTypeNode, elems: Seq[MuGlobalVarNode]) extends IRConstNode
case class NodeConstExtern(id: MuID, ty: MuTypeNode, symbol: String)              extends IRConstNode
// format: ON

case class NodeGlobalCell(id: MuID, ty: MuTypeNode) extends OtherTopLevelNode

case class NodeFunc(id: MuID, sig: MuFuncSigNode) extends OtherTopLevelNode

case class NodeExpFunc(id: MuID, func: MuFuncNode, callconv: Flag, cookie: MuConstIntNode) extends OtherTopLevelNode

case class NodeFuncVer(id: MuID, func: MuFuncNode, bbs: Seq[MuBBNode]) extends IRBuilderNode

case class NodeBB(id: MuID, norParamIDs: Seq[MuID], norParamTys: Seq[MuTypeNode], excParamID: Option[MuID], insts: Seq[MuInstNode]) extends IRBuilderNode
case class NodeDestClause(id: MuID, dest: MuBBNode, vars: Seq[MuVarNode]) extends IRBuilderNode

case class NodeExcClause(id: MuID, nor: MuDestClause, exc: MuDestClause) extends IRBuilderNode

case class NodeKeepaliveClause(id: MuID, vars: Seq[MuLocalVarNode]) extends IRBuilderNode

case class NodeCscRetWith(id: MuID, rettys: Seq[MuVarNode]) extends IRCurStackClauseNode
case class NodeCscKillOld(id: MuID) extends IRCurStackClauseNode

case class NodeNscPassValues(id: MuID, tys: Seq[MuTypeNode], vars: Seq[MuVarNode]) extends IRNewStackClauseNode
case class NodeNscThrowExc(id: MuID, exc: MuVarNode) extends IRNewStackClauseNode
    
case class NodeBinOp(id: MuID, resultID: MuID, statusResultIDs: Seq[MuID], optr: BinOptr.Value, flags: BinOpStatus, ty: MuTypeNode, opnd1: MuVarNode, opnd2: MuVarNode, excClause: Option[MuExcClause]) extends IRInstNode
case class NodeCmp(id: MuID, resultID: MuID, optr: CmpOptr.Value, ty: MuTypeNode, opnd1: MuVarNode, opnd2: MuVarNode) extends IRInstNode
case class NodeConv(id: MuID, resultID: MuID, optr: ConvOptr.Value, fromTy: MuTypeNode, toTy: MuTypeNode, opnd: MuVarNode) extends IRInstNode
case class NodeSelect(id: MuID, resultID: MuID, condTy: MuTypeNode, opndTy: MuTypeNode, cond: MuVarNode, ifTrue: MuVarNode, ifFalse: MuVarNode) extends IRInstNode

case class NodeBranch(id: MuID, dest: MuDestClause) extends IRInstNode
case class NodeBranch2(id: MuID, cond: MuVarNode, ifTrue: MuDestClause, ifFalse: MuDestClause) extends IRInstNode
case class NodeSwitch(id: MuID, opndTy: MuTypeNode, opnd: MuVarNode, defaultDest: MuDestClause, cases: Seq[MuConstNode], dests: Seq[MuDestClause]) extends IRInstNode
case class NodeCall(id: MuID, resultIDs: Seq[MuID], sig: MuFuncSigNode, callee: MuVarNode, args: Seq[MuVarNode], excClause: Option[MuExcClause], keepaliveClause: Option[MuKeepaliveClause]) extends IRInstNode
case class NodeTailCall(id: MuID, sig: MuFuncSigNode, callee: MuVarNode, args: Seq[MuVarNode]) extends IRInstNode
case class NodeRet(id: MuID, rvs: Seq[MuVarNode]) extends IRInstNode
case class NodeThrow(id: MuID, exc: MuVarNode) extends IRInstNode
case class NodeExtractValue(id: MuID, resultID: MuID, strty: MuTypeNode, index: Int, opnd: MuVarNode) extends IRInstNode
case class NodeInsertValue(id: MuID, resultID: MuID, strty: MuTypeNode, index: Int, opnd: MuVarNode, newval: MuVarNode) extends IRInstNode
case class NodeExtractElement(id: MuID, resultID: MuID, seqty: MuTypeNode, indty: MuTypeNode, opnd: MuVarNode, index: MuVarNode) extends IRInstNode
case class NodeInsertElement(id: MuID, resultID: MuID, seqty: MuTypeNode, indty: MuTypeNode, opnd: MuVarNode, index: MuVarNode, newval: MuVarNode) extends IRInstNode
case class NodeShuffleVector(id: MuID, resultID: MuID, vecty: MuTypeNode, maskty: MuTypeNode, vec1: MuVarNode, vec2: MuVarNode, mask: MuVarNode) extends IRInstNode
case class NodeNew(id: MuID, resultID: MuID, allocty: MuTypeNode, excClause: Option[MuExcClause]) extends IRInstNode
case class NodeNewHybrid(id: MuID, resultID: MuID, allocty: MuTypeNode, lenty: MuTypeNode, length: MuVarNode, excClause: Option[MuExcClause]) extends IRInstNode
case class NodeAlloca(id: MuID, resultID: MuID, allocty: MuTypeNode, excClause: Option[MuExcClause]) extends IRInstNode
case class NodeAllocaHybrid(id: MuID, resultID: MuID, allocty: MuTypeNode, lenty: MuTypeNode, length: MuVarNode, excClause: Option[MuExcClause]) extends IRInstNode
case class NodeGetIRef(id: MuID, resultID: MuID, refty: MuTypeNode, opnd: MuVarNode) extends IRInstNode
case class NodeGetFieldIRef(id: MuID, resultID: MuID, isPtr: Boolean, refty: MuTypeNode, index: Int, opnd: MuVarNode) extends IRInstNode
case class NodeGetElemIRef(id: MuID, resultID: MuID, isPtr: Boolean, refty: MuTypeNode, indty: MuTypeNode, opnd: MuVarNode, index: MuVarNode) extends IRInstNode
case class NodeShiftIRef(id: MuID, resultID: MuID, isPtr: Boolean, refty: MuTypeNode, offty: MuTypeNode, opnd: MuVarNode, offset: MuVarNode) extends IRInstNode
case class NodeGetVarPartIRef(id: MuID, resultID: MuID, isPtr: Boolean, refty: MuTypeNode, opnd: MuVarNode) extends IRInstNode
case class NodeLoad(id: MuID, resultID: MuID, isPtr: Boolean, ord: MemoryOrder.Value, refty: MuTypeNode, loc: MuVarNode, excClause: Option[MuExcClause]) extends IRInstNode
case class NodeStore(id: MuID, isPtr: Boolean, ord: MemoryOrder.Value, refty: MuTypeNode, loc: MuVarNode, newval: MuVarNode, excClause: Option[MuExcClause]) extends IRInstNode
case class NodeCmpXchg(id: MuID, valueResultID: MuID, succResultID: MuID, isPtr: Boolean, isWeak: Boolean, ordSucc: MemoryOrder.Value, ordFail: MemoryOrder.Value, refty: MuTypeNode, loc: MuVarNode, expected: MuVarNode, desired: MuVarNode, excClause: Option[MuExcClause]) extends IRInstNode
case class NodeAtomicRMW(id: MuID, resultID: MuID, isPtr: Boolean, ord: MemoryOrder.Value, optr: AtomicRMWOptr.Value, refTy: MuTypeNode, loc: MuVarNode, opnd: MuVarNode, excClause: Option[MuExcClause]) extends IRInstNode
case class NodeFence(id: MuID, ord: MemoryOrder.Value) extends IRInstNode
case class NodeTrap(id: MuID, resultIDs: Seq[MuID], rettys: Seq[MuTypeNode], excClause: Option[MuExcClause], keepaliveClause: Option[MuKeepaliveClause]) extends IRInstNode
case class NodeWatchPoint(id: MuID, wpid: MuWPID, resultIDs: Seq[MuID], rettys: Seq[MuTypeNode], dis: MuDestClause, ena: MuDestClause, exc: Option[MuDestClause], keepaliveClause: Option[MuKeepaliveClause]) extends IRInstNode
case class NodeWPBranch(id: MuID, wpid: MuWPID, dis: MuDestClause, ena: MuDestClause) extends IRInstNode
case class NodeCCall(id: MuID, resultIDs: Seq[MuID], callconv: Flag, calleeTy: MuTypeNode, sig: MuFuncSigNode, callee: MuVarNode, args: Seq[MuVarNode], excClause: Option[MuExcClause], keepaliveClause: Option[MuKeepaliveClause]) extends IRInstNode
case class NodeNewThread(id: MuID, resultID: MuID, stack: MuVarNode, threadlocal: Option[MuVarNode], newStackClause: MuNewStackClause, excClause: Option[MuExcClause]) extends IRInstNode
case class NodeSwapStack(id: MuID, resultIDs: Seq[MuID], swappee: MuVarNode, curStackClause: MuCurStackClause, newStackClause: MuNewStackClause, excClause: Option[MuExcClause], keepaliveClause: Option[MuKeepaliveClause]) extends IRInstNode
case class NodeCommInst(id: MuID, resultIDs: Seq[MuID], opcode: CommInst, flags: Seq[Flag], tys: Seq[MuTypeNode], sigs: Seq[MuFuncSigNode], args: Seq[MuVarNode], excClause: Option[MuExcClause], keepaliveClause: Option[MuKeepaliveClause]) extends IRInstNode

// EXT:END:IRBUILDER_NODES