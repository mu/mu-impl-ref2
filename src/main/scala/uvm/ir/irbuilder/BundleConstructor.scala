package uvm.ir.irbuilder

import scala.collection.mutable.ArrayBuffer

import uvm._
import uvm.ir.irbuilder.IRBuilderNode._
import uvm.ssavariables._
import uvm.types._
import uvm.utils.Later
import uvm.utils.Later._

/** This class converts the soup of ID-referenced nodes into the refimpl's internal AST. */
class BundleConstructor(idNameMap: Map[MuID, MuName], nodeList: Seq[IRBuilderNode], globalBundle: GlobalBundle) {

  val idNodeMap = nodeList.map(n => (n.id, n)).toMap

  def findNode[T <: IRBuilderNode](id: MuID): T = {
    idNodeMap.getOrElse(id, {
      val maybeName = idNameMap.get(id)
      throw new NodeIDNotFoundException(id, maybeName)
    }).asInstanceOf[T]
  }

  val bundle = new TrantientBundle()

  def setIDAndMaybeName[T <: IdentifiedSettable](obj: T, id: MuID): T = {
    obj.id = id
    obj.name = idNameMap.get(id)
    obj
  }

  def cascadeLookup[T <: Identified](id: MuID, ns1: Namespace[T], ns2: Namespace[T]): T = {
    ns1.get(id).getOrElse {
      ns2.get(id).getOrElse {
        val maybeName = idNameMap.get(id)
        throw new UnknownIDException(id, maybeName)
      }
    }
  }

  implicit def resTy(id: MuID): Type = cascadeLookup(id, bundle.typeNs, globalBundle.typeNs)
  implicit def resTys(ids: Seq[MuID]): Seq[Type] = ids.map(resTy)

  implicit def resSig(id: MuID): FuncSig = cascadeLookup(id, bundle.funcSigNs, globalBundle.funcSigNs)

  // used by InstSwitch
  def resConst(id: MuID): Constant = cascadeLookup(id, bundle.constantNs, globalBundle.constantNs)

  // These auto-resolution may conflict with the resVar method when building instructions. Explicitly enable them in methods.
  def resGlobalVar(id: MuID): GlobalVariable = cascadeLookup(id, bundle.globalVarNs, globalBundle.globalVarNs)

  def resFunc(id: MuID): Function = cascadeLookup(id, bundle.funcNs, globalBundle.funcNs)

  def resConstInt(id: MuID): ConstInt = resGlobalVar(id) match {
    case ci: ConstInt => ci
    case other        => throw new AssertionError("Constant int expected, but %d found. ID: %d".format(other.getClass.getName, id))
  }

  def needType[E <: Type](id: MuID, expectedType: Class[E], n: String): E = {
    val t = resTy(id)
    if (!(expectedType.isAssignableFrom(t.getClass))) {
      throw new IRBuilderException("Type id:%d name:%s does not match expected type. Expected %s, actually %s.".format(
        id, idNameMap.getOrElse(id, "(no name)"), n, t))
    }
    t.asInstanceOf[E]
  }

  def needInt(id: MuID) = needType(id, classOf[TypeInt], "int")
  def needStruct(id: MuID) = needType(id, classOf[TypeStruct], "struct")
  def needAbsStruct(id: MuID) = needType(id, classOf[AbstractStructType], "struct or hybrid")
  def needArray(id: MuID) = needType(id, classOf[TypeArray], "array")
  def needVector(id: MuID) = needType(id, classOf[TypeVector], "vector")
  def needHybrid(id: MuID) = needType(id, classOf[TypeHybrid], "hybrid")
  def needSeq(id: MuID) = needType(id, classOf[AbstractSeqType], "array or vector")

  def makeBundle(): TrantientBundle = {
    // Enable auto resolution in this global scope
    @inline implicit def _resGlobalVar = resGlobalVar _
    @inline implicit def _resFunc = resFunc _

    // Classify nodes into categories for phasing.
    val typeNodes = new ArrayBuffer[IRTypeNode]
    val sigNodes = new ArrayBuffer[IRSigNode]
    val constNodes = new ArrayBuffer[IRConstNode]
    val expFuncNodes = new ArrayBuffer[NodeExpFunc]
    val otherTopLevelNodes = new ArrayBuffer[OtherTopLevelNode]
    val funcVers = new ArrayBuffer[NodeFuncVer]()
    val otherNodes = new ArrayBuffer[IRBuilderNode]

    nodeList foreach {
      case n: IRTypeNode        => typeNodes += n
      case n: IRSigNode         => sigNodes += n
      case n: IRConstNode       => constNodes += n
      case n: NodeExpFunc       => expFuncNodes += n
      case n: OtherTopLevelNode => otherTopLevelNodes += n
      case n: NodeFuncVer       => funcVers += n
      case n                    => otherNodes += n
    }

    // Phase 1: create types and sigs, and resolve their refs

    val phase1 = new Later()

    typeNodes foreach { irNode =>
      val muNode: Type = irNode match {
        case NodeTypeInt(id, len)      => new TypeInt(len)
        case NodeTypeFloat(id)         => new TypeFloat()
        case NodeTypeDouble(id)        => new TypeDouble()
        case NodeTypeUPtr(id, ty)      => new TypeUPtr(null).later(phase1) { t => t.ty = ty }
        case NodeTypeUFuncPtr(id, sig) => new TypeUFuncPtr(null).later(phase1) { t => t.sig = sig }

        case NodeTypeStruct(id, fieldTys) => new TypeStruct(null).later(phase1) { t =>
          t.fieldTys = fieldTys.map(resTy)
        }
        case NodeTypeHybrid(id, fixedTys, varTy) => new TypeHybrid(null, null).later(phase1) { t =>
          t.fieldTys = fixedTys.map(resTy)
          t.varTy = varTy
        }
        case NodeTypeArray(id, elemTy, len) => new TypeArray(null, len).later(phase1) { t =>
          t.elemTy = elemTy
        }
        case NodeTypeVector(id, elemTy, len) => new TypeVector(null, len).later(phase1) { t =>
          t.elemTy = elemTy
        }
        case NodeTypeVoid(id)           => new TypeVoid()

        case NodeTypeRef(id, ty)        => new TypeRef(null).later(phase1) { t => t.ty = ty }
        case NodeTypeIRef(id, ty)       => new TypeIRef(null).later(phase1) { t => t.ty = ty }
        case NodeTypeWeakRef(id, ty)    => new TypeWeakRef(null).later(phase1) { t => t.ty = ty }
        case NodeTypeFuncRef(id, sig)   => new TypeFuncRef(null).later(phase1) { t => t.sig = sig }

        case NodeTypeTagRef64(id)       => new TypeTagRef64()
        case NodeTypeThreadRef(id)      => new TypeThreadRef()
        case NodeTypeStackRef(id)       => new TypeStackRef()
        case NodeTypeFrameCursorRef(id) => new TypeFrameCursorRef()
        case NodeTypeIRBuilderRef(id)   => new TypeIRBuilderRef()
      }
      setIDAndMaybeName(muNode, irNode.id)
      bundle.typeNs.add(muNode)
    }

    sigNodes foreach { irNode =>
      val NodeFuncSig(id, paramTys, retTys) = irNode
      val muNode: FuncSig = new FuncSig(null, null).later(phase1) { sig =>
        sig.paramTys = paramTys.map(resTy)
        sig.retTys = retTys.map(resTy)
      }
      setIDAndMaybeName(muNode, irNode.id)
      bundle.funcSigNs.add(muNode)
    }

    phase1.doAll()

    // Now, all types and sigs are created and their references are resolved.

    // Phase 2: create and resolve consts, global cells, funcs, expfuncs and resolve their refs.

    val phase2 = new Later()

    constNodes foreach { irNode =>
      val muNode: Constant = irNode match {
        case NodeConstInt(id, ty, value)     => new ConstInt(ty, value)
        case NodeConstFloat(id, ty, value)   => new ConstFloat(ty, value)
        case NodeConstDouble(id, ty, value)  => new ConstDouble(ty, value)
        case NodeConstNull(id, ty)           => new ConstNull(ty)
        case NodeConstExtern(id, ty, symbol) => new ConstExtern(ty, symbol)
        case NodeConstSeq(id, ty, elems) => new ConstSeq(ty, null).later(phase2) { c =>
          c.elems = elems.map(resGlobalVar)
        }
      }
      setIDAndMaybeName(muNode, irNode.id)
      bundle.constantNs.add(muNode)
    }

    otherTopLevelNodes foreach {
      case NodeGlobalCell(id: MuID, ty: MuTypeNode) => {
        val muNode = new GlobalCell(ty)
        setIDAndMaybeName(muNode, id)
        bundle.globalCellNs.add(muNode)
      }
      case NodeFunc(id: MuID, sig: MuFuncSigNode) => {
        val muNode = new Function()
        muNode.sig = sig
        setIDAndMaybeName(muNode, id)
        bundle.funcNs.add(muNode)
      }
    }
    
    // Build ExposedFunc instances after building Function instances.
    expFuncNodes foreach {
      case NodeExpFunc(id: MuID, func: MuFuncNode, callconv: Flag, cookie: MuConstIntNode) => {
        val muNode = new ExposedFunc(func, callconv, resConstInt(cookie))
        setIDAndMaybeName(muNode, id)
        bundle.expFuncNs.add(muNode)
      }
    }

    phase2.doAll()

    // Now all top-level definitions are created and their inter-references are resolved

    // Phase 3: Define all defined functions (.funcdef)
    val phase3 = new Later()

    funcVers foreach { irNode =>
      val muNode = new FuncVer(irNode.func)
      setIDAndMaybeName(muNode, irNode.id)
      bundle.funcVerNs.add(muNode)

      phase3 { () =>
        defineFunction(irNode, muNode)
      }
    }

    phase3.doAll()

    bundle
  }

  def defineFunction(irNode: NodeFuncVer, muFuncVer: FuncVer): Unit = {
    muFuncVer.bbs = new ArrayBuffer()
    muFuncVer.bbNs = bundle.allNs.makeSubSpace("basic block")
    val irBBmuBBs = irNode.bbs.map { bbID =>
      val irBB = findNode[NodeBB](bbID)
      val muBB = new BasicBlock(muFuncVer)
      setIDAndMaybeName(muBB, bbID)
      muFuncVer.bbs += muBB
      muFuncVer.bbNs.add(muBB)
      (irBB, muBB)
    }

    for ((irBB, muBB) <- irBBmuBBs) {
      muBB.norParams = new ArrayBuffer()
      muBB.excParam = None
      muBB.insts = new ArrayBuffer()
      muBB.localVarNs = bundle.allNs.makeSubSpace("local variable")
      muBB.localInstNs = bundle.allNs.makeSubSpace("instruction")
      for ((norParamID, norParamTyID) <- irBB.norParamIDs zip irBB.norParamTys) {
        val muType = resTy(norParamTyID)
        val muNorParam = NorParam(muType)
        setIDAndMaybeName(muNorParam, norParamID)
        muBB.norParams += muNorParam
        muBB.localVarNs.add(muNorParam)
      }

      for (excParamID <- irBB.excParamID) {
        val muExcParam = ExcParam()
        setIDAndMaybeName(muExcParam, excParamID)
        muBB.excParam = Some(muExcParam)
        muBB.localVarNs.add(muExcParam)
      }

      for (instID <- irBB.insts) {
        val irInst = findNode[IRInstNode](instID)
        val muInst = defineInst(muFuncVer, muBB, irInst)
        setIDAndMaybeName(muInst, instID)
        muBB.insts += muInst
        muBB.localInstNs.add(muInst)
      }
    }
  }

  def defineInst(muFuncVer: FuncVer, muBB: BasicBlock, irInst: IRInstNode): Instruction = try {
    import BundleConstructorImplicitMagics._

    implicit val self = this
    implicit val bbNs = muFuncVer.bbNs
    implicit val localVarNs = muBB.localVarNs

    implicit def resVar(id: MuVarNode): SSAVariable = {
      localVarNs.get(id).getOrElse {
        cascadeLookup(id, bundle.globalVarNs, globalBundle.globalVarNs)
      }
    }

    implicit def resVars(ids: Seq[MuVarNode]): Seq[SSAVariable] = {
      ids.map(resVar)
    }

    // used by keepalives
    def resLocalVar(id: MuVarNode): LocalVariable = {
      localVarNs.get(id).getOrElse {
        val vars = localVarNs.all.map("  " + _.repr).mkString("\n")
        throw new IRBuilderException("Local variable id:%d name:%s not found.\nFYI: Current local variables are:\n%s".format(
          id, idNameMap.getOrElse(id, "(no name)"), vars))
      }
    }

    @inline
    implicit def resBB(id: MuBBNode): BasicBlock = {
      bbNs(id)
    }

    @inline
    implicit def resDestClause(id: MuDestClause): DestClause = {
      val NodeDestClause(_, dest, vars) = findNode[NodeDestClause](id)
      DestClause(dest, vars.map(resVar))
    }

    @inline
    implicit def resExcClause(maybeID: Option[MuExcClause]): Option[ExcClause] = maybeID map { id =>
      val irExcClause = findNode[NodeExcClause](id)
      ExcClause(irExcClause.nor, irExcClause.exc)
    }

    @inline
    implicit def resKeepalives(maybeID: Option[MuKeepaliveClause]): Seq[LocalVariable] = maybeID map { id =>
      val irKeepaliveClause = findNode[NodeKeepaliveClause](id)
      irKeepaliveClause.vars.map(resLocalVar)
    } getOrElse (Seq())

    @inline
    implicit def resCurStackClause(id: MuCurStackClause): CurStackAction = {
      val irCurStackClause = findNode[IRCurStackClauseNode](id)
      irCurStackClause match {
        case NodeCscRetWith(_, tys) => RetWith(tys)
        case NodeCscKillOld(_)      => KillOld()
      }
    }

    @inline
    implicit def resNewStackClause(id: MuNewStackClause): NewStackAction = {
      val irNewStackClause = findNode[IRNewStackClauseNode](id)
      irNewStackClause match {
        case NodeNscPassValues(_, tys, vars) => PassValues(tys, vars)
        case NodeNscThrowExc(_, exc)         => ThrowExc(exc)
      }
    }

    val muInst: Instruction = irInst match {
      case NodeBinOp(id, resultID, statusResultIDs, optr, flags, ty, opnd1, opnd2, excClause) =>
        InstBinOp(optr, flags, ty, opnd1, opnd2, null) --> (Seq(resultID)++statusResultIDs) EXC excClause
      case NodeCmp(id, resultID, optr, ty, opnd1, opnd2) =>
        InstCmp(optr, ty, opnd1, opnd2) --> resultID
      case NodeConv(id, resultID, optr, fromTy, toTy, opnd) =>
        InstConv(optr, fromTy, toTy, opnd) --> resultID
      case NodeSelect(id, resultID, condTy, opndTy, cond, ifTrue, ifFalse) =>
        InstSelect(condTy, opndTy, cond, ifTrue, ifFalse) --> resultID
      case NodeBranch(id, dest) =>
        InstBranch(dest)
      case NodeBranch2(id, cond, ifTrue, ifFalse) =>
        InstBranch2(cond, ifTrue, ifFalse)
      case NodeSwitch(id, opndTy, opnd, defaultDest, cases, dests) => {
        val caseDests = for ((c, d) <- cases zip dests) yield {
          (resConst(c), resDestClause(d))
        }
        InstSwitch(opndTy, opnd, defaultDest, ArrayBuffer(caseDests: _*))
      }
      case NodeCall(id, resultIDs, sig, callee, args, excClause, keepaliveClause) =>
        InstCall(sig, callee, args, null, keepaliveClause) --> resultIDs EXC excClause
      case NodeTailCall(id, sig, callee, args) =>
        InstTailCall(sig, callee, args)
      case NodeRet(id, rvs) =>
        InstRet(muFuncVer, rvs)
      case NodeThrow(id, exc) =>
        InstThrow(exc)
      case NodeExtractValue(id, resultID, strty, index, opnd) =>
        InstExtractValue(needStruct(strty), index, opnd) --> resultID
      case NodeInsertValue(id, resultID, strty, index, opnd, newval) =>
        InstInsertValue(needStruct(strty), index, opnd, newval) --> resultID
      case NodeExtractElement(id, resultID, seqty, indty, opnd, index) =>
        InstExtractElement(needSeq(seqty), needInt(indty), opnd, index) --> resultID
      case NodeInsertElement(id, resultID, seqty, indty, opnd, index, newval) =>
        InstInsertElement(needSeq(seqty), needInt(indty), opnd, index, newval) --> resultID
      case NodeShuffleVector(id, resultID, vecty, maskty, vec1, vec2, mask) =>
        InstShuffleVector(needVector(vecty), needVector(maskty), vec1, vec2, mask) --> resultID
      case NodeNew(id, resultID, allocty, excClause) =>
        InstNew(allocty, null) --> resultID EXC excClause
      case NodeNewHybrid(id, resultID, allocty, lenty, length, excClause) =>
        InstNewHybrid(needHybrid(allocty), needInt(lenty), length, null) --> resultID EXC excClause
      case NodeAlloca(id, resultID, allocty, excClause) =>
        InstAlloca(allocty, null) --> resultID EXC excClause
      case NodeAllocaHybrid(id, resultID, allocty, lenty, length, excClause) =>
        InstAllocaHybrid(needHybrid(allocty), needInt(lenty), length, null) --> resultID EXC excClause
      case NodeGetIRef(id, resultID, refty, opnd) =>
        InstGetIRef(refty, opnd) --> resultID
      case NodeGetFieldIRef(id, resultID, isPtr, refty, index, opnd) =>
        InstGetFieldIRef(isPtr, needAbsStruct(refty), index, opnd) --> resultID
      case NodeGetElemIRef(id, resultID, isPtr, refty, indty, opnd, index) =>
        InstGetElemIRef(isPtr, needSeq(refty), needInt(indty), opnd, index) --> resultID
      case NodeShiftIRef(id, resultID, isPtr, refty, offty, opnd, offset) =>
        InstShiftIRef(isPtr, refty, needInt(offty), opnd, offset) --> resultID
      case NodeGetVarPartIRef(id, resultID, isPtr, refty, opnd) =>
        InstGetVarPartIRef(isPtr, needHybrid(refty), opnd) --> resultID
      case NodeLoad(id, resultID, isPtr, ord, refty, loc, excClause) =>
        InstLoad(isPtr, ord, refty, loc, null) --> resultID EXC excClause
      case NodeStore(id, isPtr, ord, refty, loc, newval, excClause) =>
        InstStore(isPtr, ord, refty, loc, newval, null) EXC excClause
      case NodeCmpXchg(id, valueResultID, succResultID, isPtr, isWeak, ordSucc, ordFail, refty, loc, expected, desired, excClause) =>
        InstCmpXchg(isPtr, isWeak, ordSucc, ordFail, refty, loc, expected, desired, null) --> (valueResultID, succResultID) EXC excClause
      case NodeAtomicRMW(id, resultID, isPtr, ord, optr, refTy, loc, opnd, excClause) =>
        InstAtomicRMW(isPtr, ord, optr, refTy, loc, opnd, null) --> resultID EXC excClause
      case NodeFence(id, ord) =>
        InstFence(ord)
      case NodeTrap(id, resultIDs, rettys, excClause, keepaliveClause) =>
        InstTrap(rettys, null, keepaliveClause) --> resultIDs EXC excClause
      case NodeWatchPoint(id, wpid, resultIDs, rettys, dis, ena, exc, keepaliveClause) =>
        InstWatchPoint(wpid, rettys, dis, ena, exc.map(resDestClause), keepaliveClause) --> resultIDs
      case NodeWPBranch(id, wpid, dis, ena) =>
        InstWPBranch(wpid, dis, ena)
      case NodeCCall(id, resultIDs, callconv, calleeTy, sig, callee, args, excClause, keepaliveClause) =>
        InstCCall(callconv, calleeTy, sig, callee, args, null, keepaliveClause) --> resultIDs EXC excClause
      case NodeNewThread(id, resultID, stack, threadlocal, newStackClause, excClause) =>
        InstNewThread(stack, threadlocal.map(resVar), newStackClause, null) --> resultID EXC excClause
      case NodeSwapStack(id, resultIDs, swappee, curStackClause, newStackClause, excClause, keepaliveClause) =>
        InstSwapStack(swappee, curStackClause, newStackClause, null, keepaliveClause) --> resultIDs EXC excClause
      case NodeCommInst(id, resultIDs, opcode, flags, tys, sigs, args, excClause, keepaliveClause) =>
        InstCommInst(opcode, flags, tys, sigs.map(resSig), args, null, keepaliveClause) --> resultIDs EXC excClause
    }
    muInst.bb = muBB
    muInst
  } catch {
    case e: Exception => {
      throw new IRBuilderException("Exception thrown when building instruction. Func: %s, ver: %s, bb: %s, inst: %d:%s".format(
        muFuncVer.func.repr, muFuncVer.repr, muBB.repr, irInst.id, idNameMap.getOrElse(irInst.id, "(no name)")), e)
    }
  }
}

private object BundleConstructorImplicitMagics {
  implicit class AddResultsSeq[T <: Instruction](val inst: T) extends AnyVal {
    @inline
    def -->(resultIDs: Seq[MuID])(implicit self: BundleConstructor, localVarNs: Namespace[LocalVariable]): T = {
      inst --> (resultIDs: _*)
    }
  }
  implicit class AddResultsVarArg[T <: Instruction](val inst: T) extends AnyVal {
    @inline
    def -->(resultIDs: MuID*)(implicit self: BundleConstructor, localVarNs: Namespace[LocalVariable]): T = {
      val instResults = for ((resultID, i) <- resultIDs.zipWithIndex) yield {
        val instRes = InstResult(inst, i)
        self.setIDAndMaybeName(instRes, resultID)
        localVarNs.add(instRes)
        instRes
      }
      inst.results = instResults.toIndexedSeq
      inst
    }
  }
  implicit class AddExcClause[T <: HasExcClause](val inst: T) extends AnyVal {
    @inline
    def EXC(excClause: Option[MuID])(implicit resExcClause: Option[MuID] => Option[ExcClause]): T = {
      inst.excClause = resExcClause(excClause)
      inst
    }
  }
}
