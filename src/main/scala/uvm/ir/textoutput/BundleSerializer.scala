package uvm.ir.textoutput

import java.io.Writer

import scala.collection.Set

import uvm._
import uvm.ssavariables._
import uvm.types._

object EntityUtils {
  def getName(obj: Identified): String = {
    obj.name.getOrElse("@uvm.unnamed" + obj.id.toString())
  }

  def getNames(objs: Seq[Identified]): String = {
    objs.map(getName).mkString(" ")
  }

  def isMetaEntity(id: MuID): Boolean = {
    id < 65536
  }

  def isMetaEntity(obj: Identified): Boolean = {
    isMetaEntity(obj.id)
  }
}

abstract class AbstractBundleSerializer(bundle: Bundle) {
  import AbstractBundleSerializer._
  import EntityUtils._

  def writeUIR(sb: StringBuilder): Unit

  def writeUIR(output: Writer): Unit = {
    val sb = new StringBuilder()
    writeUIR(sb)
    output.append(sb)
    output.flush()
  }

  def writeType(ty: Type, output: StringBuilder): Unit = {
    val ctor = ty match {
      case TypeInt(l)                    => s"int<${l}>"
      case TypeFloat()                   => "float"
      case TypeDouble()                  => "double"
      case TypeUPtr(ty)                  => s"uptr<${ty.n}>"
      case TypeUFuncPtr(sig)             => s"ufuncptr<${sig.n}>"
      case TypeStruct(fieldTys)          => s"struct<${fieldTys.ns}>"
      case TypeHybrid(fieldTys, varPart) => s"hybrid<${fieldTys.ns} ${varPart.n}>"
      case TypeArray(elemTy, len)        => s"array<${elemTy.n} ${len}>"
      case TypeVector(elemTy, len)       => s"vector<${elemTy.n} ${len}>"
      case TypeVoid()                    => "void"
      case TypeRef(ty)                   => s"ref<${ty.n}>"
      case TypeIRef(ty)                  => s"iref<${ty.n}>"
      case TypeWeakRef(ty)               => s"weakref<${ty.n}>"
      case TypeTagRef64()                => "tagref64"
      case TypeFuncRef(sig)              => s"funcref<${sig.n}>"
      case TypeThreadRef()               => "threadref"
      case TypeStackRef()                => "stackref"
      case TypeFrameCursorRef()          => "framecursorref"
      case TypeIRBuilderRef()            => "irbuilderref"
      case _                             => throw new BundleSerializerException("unknown type " + ty.getClass.getName)
    }
    output.append(s".typedef ${ty.n} = ${ctor}\n")
  }

  def writeSig(sig: FuncSig, output: StringBuilder): Unit = {
    output.append(s".funcsig ${sig.n} = (${sig.paramTys.ns}) -> (${sig.retTys.ns})\n")
  }

  def writeConst(const: Constant, output: StringBuilder): Unit = {
    val ty = const.constTy
    val ctor = const match {
      case ConstInt(_, num)       => num.toString
      case ConstFloat(_, num)     => "bitsf(0x%x)".format(java.lang.Float.floatToRawIntBits(num))
      case ConstDouble(_, num)    => "bitsd(0x%x)".format(java.lang.Double.doubleToRawLongBits(num))
      case ConstNull(_)           => "NULL"
      case ConstSeq(_, elems)     => "{%s}".format(getNames(elems))
      case ConstExtern(_, symbol) => "EXTERN \"%s\"".format(symbol)
      case _                      => throw new BundleSerializerException("unknown constant " + const.getClass.getName)
    }
    output.append(s".const ${const.n} <${ty.n}> = ${ctor}\n")
  }

  def writeGlobal(global: GlobalCell, output: StringBuilder): Unit = {
    output.append(s".global ${global.n} <${global.cellTy.n}>\n")
  }

  def writeExpFunc(expFunc: ExposedFunc, output: StringBuilder): Unit = {
    output.append(s".expose ${expFunc.n} = ${expFunc.func.n} ${expFunc.callConv.name} ${expFunc.cookie.n}\n")
  }

  def writeFunc(func: Function, output: StringBuilder): Unit = {
    output.append(s".funcdecl ${func.n} <${func.sig.n}>\n")
  }

  def writeFuncVer(ver: FuncVer, output: StringBuilder): Unit = {
    val func = ver.func
    output.append(s".funcdef ${func.n} VERSION ${ver.n} <${func.sig.n}> {\n")
    for (bb <- ver.bbs) {
      writeBB(bb, output)
    }
    output.append("}\n")
  }

  def norParamToString(p: NorParam): String = {
    s"<${p.ty.n}> ${p.n} "
  }

  def excParamToString(p: ExcParam): String = {
    s" [${p.n}]"
  }

  def writeBB(bb: BasicBlock, output: StringBuilder): Unit = {
    output.append {
      val norParamList = bb.norParams.map(norParamToString).mkString(" ")
      val maybeExcParam = bb.excParam.map(excParamToString).getOrElse("")
      s"  ${bb.n}(${norParamList})${maybeExcParam}:\n"
    }

    for (inst <- bb.insts) {
      writeInst(inst, output)
    }
  }

  def writeInst(inst: Instruction, output: StringBuilder): Unit = {
    implicit val bs = this

    val body = inst match {
      case InstBinOp(op, flags, opndTy, op1, op2, excClause)       => s"${op.s} ${flags.flags} <${opndTy.n}> ${op1.n} ${op2.n} ${excClause.e}"
      case InstCmp(op, opndTy, op1, op2)                           => s"${op.s} <${opndTy.n}> ${op1.n} ${op2.n}"
      case InstConv(op, fromTy, toTy, opnd)                        => s"${op.s} <${fromTy.n} ${toTy.n}> ${opnd.n}"
      case InstSelect(condTy, opndTy, cond, ifTrue, ifFalse)       => s"SELECT <${condTy.n} ${opndTy.n}> ${cond.n} ${ifTrue.n} ${ifFalse.n}"
      case InstBranch(dest)                                        => s"BRANCH ${dest.d}"
      case InstBranch2(cond, ifTrue, ifFalse)                      => s"BRANCH2 ${cond.n} ${ifTrue.d} ${ifFalse.d}"
      case i @ InstSwitch(opndTy, opnd, defDest, cases)            => switchToString(i)
      case InstCall(sig, callee, argList, excClause, keepalives)   => s"CALL <${sig.n}> ${callee.n} (${argList.ns}) ${excClause.e} ${keepalives.kas}"
      case InstTailCall(sig, callee, argList)                      => s"TAILCALL <${sig.n}> ${callee.n} (${argList.ns})"
      case InstRet(funcVer, retVals)                               => s"RET (${retVals.ns})"
      case InstThrow(excVal)                                       => s"THROW ${excVal.n}"
      case InstExtractValue(strTy, index, opnd)                    => s"EXTRACTVALUE <${strTy.n} ${index}> ${opnd.n}"
      case InstInsertValue(strTy, index, opnd, newVal)             => s"INSERTVALUE <${strTy.n} ${index}> ${opnd.n} ${newVal.n}"
      case InstExtractElement(seqTy, indTy, opnd, index)           => s"EXTRACTELEMENT <${seqTy.n} ${indTy.n}> ${opnd.n} ${index.n}"
      case InstInsertElement(seqTy, indTy, opnd, index, newVal)    => s"INSERTELEMENT <${seqTy.n} ${indTy.n}> ${opnd.n} ${index.n} ${newVal.n}"
      case InstShuffleVector(vecTy, maskTy, vec1, vec2, mask)      => s"SHUFFLEVECTOR <${vecTy.n} ${maskTy.n}> ${vec1.n} ${vec2.n} ${mask.n}"
      case InstNew(allocTy, excClause)                             => s"NEW <${allocTy.n}> ${excClause.e}"
      case InstNewHybrid(allocTy, lenTy, length, excClause)        => s"NEWHYBRID <${allocTy.n} ${lenTy.n}> ${length.n} ${excClause.e}"
      case InstAlloca(allocTy, excClause)                          => s"ALLOCA <${allocTy.n}> ${excClause.e}"
      case InstAllocaHybrid(allocTy, lenTy, length, excClause)     => s"ALLOCAHYBRID <${allocTy.n} ${lenTy.n}> ${length.n} ${excClause.e}"
      case InstGetIRef(referentTy, opnd)                           => s"GETIREF <${referentTy.n}> ${opnd.n}"
      case InstGetFieldIRef(ptr, referentTy, index, opnd)          => s"GETFIELDIREF ${ptr.p} <${referentTy.n} ${index}> ${opnd.n}"
      case InstGetElemIRef(ptr, referentTy, indTy, opnd, index)    => s"GETELEMIREF ${ptr.p} <${referentTy.n} ${indTy.n}> ${opnd.n} ${index.n}"
      case InstShiftIRef(ptr, referentTy, offTy, opnd, offset)     => s"SHIFTIREF ${ptr.p} <${referentTy.n} ${offTy.n}> ${opnd.n} ${offset.n}"
      case InstGetVarPartIRef(ptr, referentTy, opnd)               => s"GETVARPARTIREF ${ptr.p} <${referentTy.n}> ${opnd.n}"
      case InstLoad(ptr, ord, referentTy, loc, excClause)          => s"LOAD ${ptr.p} ${ord.s} <${referentTy.n}> ${loc.n} ${excClause.e}"
      case InstStore(ptr, ord, referentTy, loc, newVal, excClause) => s"STORE ${ptr.p} ${ord.s} <${referentTy.n}> ${loc.n} ${newVal.n} ${excClause.e}"
      case InstCmpXchg(ptr, weak, ordSucc, ordFail, referentTy, loc, expected, desired, excClause) // This line is long ...
      => s"CMPXCHG ${ptr.p} ${weak.w} ${ordSucc.s} ${ordFail.s} <${referentTy.n}> ${loc.n} ${expected.n} ${desired.n} ${excClause.e}"
      case InstAtomicRMW(ptr, ord, op, referentTy, loc, opnd, excClause) => s"ATOMICRMW ${ptr.p} ${ord.s} ${op.s} <${referentTy.n}> ${loc.n} ${opnd.n} ${excClause.e}"
      case InstFence(ord) => s"FENCE ${ord.s}"
      case InstTrap(retTys, excClause, keepalives) => s"TRAP <${retTys.ns}> ${excClause.e} ${keepalives.kas}"
      case InstWatchPoint(wpID, retTys, dis, ena, exc, keepalives) => s"WATCHPOINT ${wpID} <${retTys.ns}> ${dis.d} ${ena.d} ${maybeWPExc(exc)} ${keepalives.kas}"
      case InstWPBranch(wpID, dis, ena) => s"WPBRANCH ${wpID} ${dis.d} ${ena.d}"
      case InstCCall(callConv, funcTy, sig, callee, argList, excClause, keepalives) =>
        s"CCALL ${callConv.name} <${funcTy.n} ${sig.n}> ${callee.n} (${argList.ns}) ${excClause.e} ${keepalives.kas}"
      case InstNewThread(stack, threadLocal, newStackAction, excClause) =>
        s"NEWTHREAD ${stack.n} ${maybeThreadLocal(threadLocal)} ${newStackActionToString(newStackAction)} ${excClause.e}"
      case InstSwapStack(swappee, curStackAction, newStackAction, excClause, keepalives) =>
        s"SWAPSTACK ${swappee.n} ${curStackActionToString(curStackAction)} ${newStackActionToString(newStackAction)} ${excClause.e} ${keepalives.kas}"
      case InstCommInst(ci, flagList, typeList, funcSigList, argList, excClause, keepalives) =>
        s"""COMMINST ${ci.name.get} [${flagList.map(_.name).mkString(" ")}] <${typeList.ns}> <[${funcSigList.ns}]> (${argList.ns}) ${excClause.e} ${keepalives.kas}"""
      case _ => throw new BundleSerializerException("Unknown instruction: " + inst.getClass.getName)
    }
    output.append(s"    (${inst.results.ns}) = [${inst.n}] ${body}\n")
  }
}

object AbstractBundleSerializer {
  import EntityUtils._

  def destToString(dest: DestClause): String = {
    s"${dest.bb.n}(${dest.args.ns})"
  }

  def excClauseToString(maybeExc: Option[ExcClause]): String = maybeExc match {
    case None                      => ""
    case Some(ExcClause(nor, exc)) => s"EXC(${nor.d} ${exc.d})"
  }

  def keepalivesToString(kas: Seq[LocalVariable]): String = {
    s"KEEPALIVE(${kas.ns})"
  }

  def ptrToString(isPtr: Boolean): String = {
    if (isPtr) "PTR" else ""
  }

  def maybeWPExc(me: Option[DestClause]): String = me match {
    case None    => ""
    case Some(e) => s"WPEXC(${destToString(e)})"
  }

  def switchToString(sw: InstSwitch): String = {
    val InstSwitch(opndTy, opnd, defDest, cases) = sw
    val casesStr = (cases.map {
      case (v, d) => s"${v.n} ${d.d}"
    }).mkString("\n")
    s"SWITCH <${opndTy.n}> ${opnd.n} ${defDest.d} { ${casesStr} }"
  }

  def maybeThreadLocal(tl: Option[SSAVariable]): String = tl match {
    case None    => ""
    case Some(v) => s"THREADLOCAL(${getName(v)})"
  }

  def newStackActionToString(nsa: NewStackAction): String = nsa match {
    case PassValues(tys, vs) => s"PASS_VALUES <${getNames(tys)}> (${getNames(vs)})"
    case ThrowExc(e)         => s"THROW_EXC ${getName(e)}"
  }

  def curStackActionToString(csa: CurStackAction): String = csa match {
    case RetWith(tys) => s"RET_WITH <${getNames(tys)}>"
    case KillOld()    => s"KILL_OLD"
  }

  implicit class MagicalStringify(val thing: Any) extends AnyVal {
    def s: String = thing.toString
  }
  implicit class MagicalNameLookup(val thing: Identified) extends AnyVal {
    def n: String = getName(thing)
  }
  implicit class MagicalNamesLookup(val things: Seq[Identified]) extends AnyVal {
    def ns: String = getNames(things)
  }
  implicit class MagicalExcClause(val maybeExc: Option[ExcClause]) extends AnyVal {
    def e: String = excClauseToString(maybeExc)
  }
  implicit class MagicalDestClause(val dest: DestClause) extends AnyVal {
    def d: String = destToString(dest)
  }
  implicit class MagicalKeepalives(val lvs: Seq[LocalVariable]) extends AnyVal {
    def kas: String = keepalivesToString(lvs)
  }
  implicit class MagicalBoolean(val value: Boolean) extends AnyVal {
    def p: String = if (value) "PTR" else ""
    def w: String = if (value) "WEAK" else ""
  }
  implicit class MagicalBinOpStatus(val thing: BinOpStatus) extends AnyVal {
    def flags: String = if (thing==0) "" else {
      val nzcv = Seq((BOS_N, "N"), (BOS_Z, "Z"), (BOS_C, "C"), (BOS_V, "V")).flatMap { case (bit, txt) =>
        if ((thing & bit) == 0) None else Some("#"+txt)
      }
      "[" + nzcv.mkString(" ") + "]"
    }
  }
}

/**
 * This class serialises TrantientBundle, suitable for debugging (the dumpBundle VMConf option uses this).
 */
class DebugBundleSerializer(val bundle: TrantientBundle) extends AbstractBundleSerializer(bundle) {

  def writeUIR(output: StringBuilder): Unit = {
    for (obj <- bundle.typeNs.all) { writeType(obj, output) }
    for (obj <- bundle.funcSigNs.all) { writeSig(obj, output) }
    for (obj <- bundle.constantNs.all) { writeConst(obj, output) }
    for (obj <- bundle.globalCellNs.all) { writeGlobal(obj, output) }
    for (obj <- bundle.expFuncNs.all) { writeExpFunc(obj, output) }
    
    val funcSet = bundle.funcNs.all.toSet
    val verFuncSet = bundle.funcVerNs.all.map(_.func).toSet
    val declSet = funcSet diff verFuncSet

    for (obj <- declSet) { writeFunc(obj, output) }
    for (obj <- bundle.funcVerNs.all) { writeFuncVer(obj, output) }
  }
  
}

/**
 * This class serialises GlobalBundle using a whiteList of top-levels, suitable for building boot image.
 */
class BundleSerializer(val bundle: GlobalBundle, val whiteList: Set[TopLevel]) extends AbstractBundleSerializer(bundle) {
  import EntityUtils._

  def filter(obj: TopLevel): Boolean = {
    !isMetaEntity(obj) && whiteList.contains(obj)
  }

  def writeIDNameMap(output: Writer): Unit = {
    val sb = new StringBuilder()
    writeIDNameMap(sb)
    output.append(sb)
    output.flush()
  }

  def writeIDNameMap(output: StringBuilder): Unit = {
    for (obj <- whiteList if !isMetaEntity(obj)) {
      writeIDNamePair(obj, output)
      obj match {
        case func: Function => {
          bundle.funcToVers(func).headOption match {
            case Some(fv) => {
              writeIDNamePair(fv, output)
              fv.bbs foreach { bb =>
                writeIDNamePair(bb, output)
                bb.localVarNs.all foreach { lv => writeIDNamePair(lv, output) }
                bb.insts foreach { inst => writeIDNamePair(inst, output) }
              }
            }
            case None => // ignore
          }
        }
        case _ => // ignore
      }
    }
  }

  private def writeIDNamePair(obj: Identified, output: StringBuilder): Unit = {
    val id = obj.id
    val name = getName(obj)
    output.append("%d,%s\n".format(id, name))
  }

  def writeUIR(output: StringBuilder): Unit = {
    for (obj <- bundle.typeNs.all if filter(obj)) { writeType(obj, output) }
    for (obj <- bundle.funcSigNs.all if filter(obj)) { writeSig(obj, output) }
    for (obj <- bundle.constantNs.all if filter(obj)) { writeConst(obj, output) }
    for (obj <- bundle.globalCellNs.all if filter(obj)) { writeGlobal(obj, output) }
    for (obj <- bundle.expFuncNs.all if filter(obj)) { writeExpFunc(obj, output) }
    for (obj <- bundle.funcNs.all if filter(obj)) { writeFuncWithMaybeVersion(obj, output) }
  }

  def writeFuncWithMaybeVersion(func: Function, output: StringBuilder): Unit = {
    bundle.funcToVers(func).headOption match {
      case None => writeFunc(func, output)
      case Some(ver) => writeFuncVer(ver, output)
    }
  }
}
