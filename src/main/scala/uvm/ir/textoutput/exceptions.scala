package uvm.ir.textoutput

import uvm.UvmException

class BundleSerializerException(message: String = null, cause: Throwable = null) extends UvmException(message, cause)