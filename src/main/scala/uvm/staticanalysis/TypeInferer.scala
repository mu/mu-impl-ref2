package uvm.staticanalysis

import uvm._
import uvm.types._
import uvm.ssavariables._


class TypeInferer(
    predefinedEntities: PredefinedEntities,
    internalEntityPool: InternalEntityPool) {
  import predefinedEntities._
  import internalEntityPool._

  def ptrOrIRefOf(ptr: Boolean, ty: Type): Type = {
    if (ptr) ptrOf(ty) else irefOf(ty)
  }
  
  /**
   * Perform type inference on all entities, global or local, in the bundle b.
   */
  def inferBundle(b: Bundle): Unit = {
    for (v <- b.globalVarNs.all) {
      inferAndSet(v)
    }
    
    for (fv <- b.funcVerNs.all) {
      for (bb <- fv.bbs) {
        for (v <- bb.localVarNs.all) {
          inferAndSet(v)
        }
      }
    }
  }
  
  private def inferAndSet(v: SSAVariable): Unit = {
    val inferredType = inferType(v)
    v.inferredType = inferredType
  }

  def inferType(v: SSAVariable): Type = v match {
    case c: Constant => c.constTy
    case g: GlobalCell => irefOf(g.cellTy)
    case f: Function => funcOf(f.sig)
    case e: ExposedFunc => funcPtrOf(e.func.sig)
    case p: NorParam => p.ty
    case p: ExcParam => REF_VOID
    case r: InstResult => {
      val resTys = inferInstResultTypes(r.inst)
      try {
        resTys(r.index)
      } catch {
        case e: IndexOutOfBoundsException => throw new StaticAnalyserException(
          s"Instruction ${r.inst} produces only ${resTys.size} results, but result index ${r.index} is requested")
      }
    }
  }

  def inferInstResultTypes(inst: Instruction): Seq[Type] = inst match {
    case i: InstBinOp => Seq(i.opndTy) ++ Seq.fill(BinOpStatus.numOfFlags(i.flags))(I1)
    case i: InstCmp => i.opndTy match {
      case TypeVector(_, l) => Seq(vecOf(I1, l))
      case _ => Seq(I1)
    }
    case i: InstConv => Seq(i.toTy)
    case i: InstSelect => Seq(i.opndTy)
    case i: InstBranch => Seq()
    case i: InstBranch2 => Seq()
    case i: InstSwitch => Seq()
    case i: InstCall => i.sig.retTys
    case i: InstTailCall => Seq()
    case i: InstRet => Seq()
    case i: InstThrow => Seq()
    case i: InstExtractValue => Seq(i.strTy.fieldTys(i.index))
    case i: InstInsertValue => Seq(i.strTy)
    case i: InstExtractElement => Seq(i.seqTy.elemTy)
    case i: InstInsertElement => Seq(i.seqTy)
    case i: InstShuffleVector => Seq(vecOf((i.vecTy.elemTy, i.maskTy.len)))
    case i: InstNew => Seq(refOf(i.allocTy))
    case i: InstNewHybrid => Seq(refOf(i.allocTy))
    case i: InstAlloca => Seq(irefOf(i.allocTy))
    case i: InstAllocaHybrid => Seq(irefOf(i.allocTy))
    case i: InstGetIRef => Seq(irefOf(i.referentTy))
    case i: InstGetFieldIRef => Seq(ptrOrIRefOf(i.ptr, i.referentTy.fieldTys(i.index)))
    case i: InstGetElemIRef => Seq(ptrOrIRefOf(i.ptr, i.referentTy.elemTy))
    case i: InstShiftIRef => Seq(ptrOrIRefOf(i.ptr, i.referentTy))
    case i: InstGetVarPartIRef => Seq(ptrOrIRefOf(i.ptr, i.referentTy.varTy))
    case i: InstLoad => Seq(unmarkedOf(i.referentTy))
    case i: InstStore => Seq()
    case i: InstCmpXchg => Seq(unmarkedOf(i.referentTy), I1)
    case i: InstAtomicRMW => Seq(unmarkedOf(i.referentTy))
    case i: InstFence => Seq()
    case i: InstTrap => i.retTys
    case i: InstWatchPoint => i.retTys
    case i: InstCCall => i.sig.retTys
    case i: InstNewThread => Seq(THREADREF)
    case i: InstSwapStack => i.curStackAction match {
      case RetWith(t) => t
      case _: KillOld => Seq()
    }
    case i: InstCommInst => i.inst.name.get match {
      case "@uvm.new_stack" => Seq(STACKREF)
      case "@uvm.kill_stack" => Seq()
      case "@uvm.thread_exit" => Seq()
      case "@uvm.current_stack" => Seq(STACKREF)
      case "@uvm.set_threadlocal" => Seq()
      case "@uvm.get_threadlocal" => Seq(REF_VOID)
      case "@uvm.tr64.is_fp" => Seq(I1)
      case "@uvm.tr64.is_int" => Seq(I1)
      case "@uvm.tr64.is_ref" => Seq(I1)
      case "@uvm.tr64.from_fp" => Seq(TAGREF64)
      case "@uvm.tr64.from_int" => Seq(TAGREF64)
      case "@uvm.tr64.from_ref" => Seq(TAGREF64)
      case "@uvm.tr64.to_fp" => Seq(DOUBLE)
      case "@uvm.tr64.to_int" => Seq(I52)
      case "@uvm.tr64.to_ref" => Seq(REF_VOID)
      case "@uvm.tr64.to_tag" => Seq(I6)
      case "@uvm.futex.wait" => Seq(I32)
      case "@uvm.futex.wait_timeout" => Seq(I32)
      case "@uvm.futex.wake" => Seq(I32)
      case "@uvm.futex.cmp_requeue" => Seq(I32)
      case "@uvm.kill_dependency" => Seq(i.typeList(0))
      case "@uvm.native.pin" => i.typeList(0) match {
        case TypeRef(t) => Seq(ptrOf(t))
        case TypeIRef(t) => Seq(ptrOf(t))
      }
      case "@uvm.native.unpin" => Seq()
      case "@uvm.native.expose" => Seq(funcPtrOf(i.funcSigList(0)))
      case "@uvm.native.unexpose" => Seq()
      case "@uvm.native.get_cookie" => Seq(I64)

      case "@uvm.meta.id_of" => Seq(I32)
      case "@uvm.meta.name_of" => Seq(BYTES_R)
      case "@uvm.meta.load_bundle" => Seq(BYTES_R)
      case "@uvm.meta.load_hail" => Seq(BYTES_R)

      case "@uvm.meta.new_cursor" => Seq(FRAMECURSORREF)
      case "@uvm.meta.next_frame" => Seq()
      case "@uvm.meta.copy_cursor" => Seq(FRAMECURSORREF)
      case "@uvm.meta.close_cursor" => Seq()
      
      case "@uvm.meta.cur_func" => Seq(I32)
      case "@uvm.meta.cur_func_ver" => Seq(I32)
      case "@uvm.meta.cur_inst" => Seq(I32)
      case "@uvm.meta.dump_keepalives" => Seq(REFS_R)

      case "@uvm.meta.pop_frames_to" => Seq()
      case "@uvm.meta.push_frame" => Seq()

      case "@uvm.meta.enable_watchPoint" => Seq()
      case "@uvm.meta.disable_watchPoint" => Seq()

      case "@uvm.meta.set_trap_handler" => Seq()

      case "@uvm.meta.constant_by_id" => Seq(i.typeList(0))
      case "@uvm.meta.global_by_id" => Seq(i.typeList(0))
      case "@uvm.meta.func_by_id" => Seq(i.typeList(0))
      case "@uvm.meta.expfunc_by_id" => Seq(i.typeList(0))

      case "@uvm.irbuilder.new_ir_builder" => Seq(IRBUILDERREF)
      
      /// GEN:BEGIN:IRBUILDER_RETVALS
      case "@uvm.irbuilder.load" => Seq()
      case "@uvm.irbuilder.abort" => Seq()
      case "@uvm.irbuilder.gen_sym" => Seq(I32)
      case "@uvm.irbuilder.new_type_int" => Seq()
      case "@uvm.irbuilder.new_type_float" => Seq()
      case "@uvm.irbuilder.new_type_double" => Seq()
      case "@uvm.irbuilder.new_type_uptr" => Seq()
      case "@uvm.irbuilder.new_type_ufuncptr" => Seq()
      case "@uvm.irbuilder.new_type_struct" => Seq()
      case "@uvm.irbuilder.new_type_hybrid" => Seq()
      case "@uvm.irbuilder.new_type_array" => Seq()
      case "@uvm.irbuilder.new_type_vector" => Seq()
      case "@uvm.irbuilder.new_type_void" => Seq()
      case "@uvm.irbuilder.new_type_ref" => Seq()
      case "@uvm.irbuilder.new_type_iref" => Seq()
      case "@uvm.irbuilder.new_type_weakref" => Seq()
      case "@uvm.irbuilder.new_type_funcref" => Seq()
      case "@uvm.irbuilder.new_type_tagref64" => Seq()
      case "@uvm.irbuilder.new_type_threadref" => Seq()
      case "@uvm.irbuilder.new_type_stackref" => Seq()
      case "@uvm.irbuilder.new_type_framecursorref" => Seq()
      case "@uvm.irbuilder.new_type_irbuilderref" => Seq()
      case "@uvm.irbuilder.new_funcsig" => Seq()
      case "@uvm.irbuilder.new_const_int" => Seq()
      case "@uvm.irbuilder.new_const_int_ex" => Seq()
      case "@uvm.irbuilder.new_const_float" => Seq()
      case "@uvm.irbuilder.new_const_double" => Seq()
      case "@uvm.irbuilder.new_const_null" => Seq()
      case "@uvm.irbuilder.new_const_seq" => Seq()
      case "@uvm.irbuilder.new_const_extern" => Seq()
      case "@uvm.irbuilder.new_global_cell" => Seq()
      case "@uvm.irbuilder.new_func" => Seq()
      case "@uvm.irbuilder.new_exp_func" => Seq()
      case "@uvm.irbuilder.new_func_ver" => Seq()
      case "@uvm.irbuilder.new_bb" => Seq()
      case "@uvm.irbuilder.new_dest_clause" => Seq()
      case "@uvm.irbuilder.new_exc_clause" => Seq()
      case "@uvm.irbuilder.new_keepalive_clause" => Seq()
      case "@uvm.irbuilder.new_csc_ret_with" => Seq()
      case "@uvm.irbuilder.new_csc_kill_old" => Seq()
      case "@uvm.irbuilder.new_nsc_pass_values" => Seq()
      case "@uvm.irbuilder.new_nsc_throw_exc" => Seq()
      case "@uvm.irbuilder.new_binop" => Seq()
      case "@uvm.irbuilder.new_binop_with_status" => Seq()
      case "@uvm.irbuilder.new_cmp" => Seq()
      case "@uvm.irbuilder.new_conv" => Seq()
      case "@uvm.irbuilder.new_select" => Seq()
      case "@uvm.irbuilder.new_branch" => Seq()
      case "@uvm.irbuilder.new_branch2" => Seq()
      case "@uvm.irbuilder.new_switch" => Seq()
      case "@uvm.irbuilder.new_call" => Seq()
      case "@uvm.irbuilder.new_tailcall" => Seq()
      case "@uvm.irbuilder.new_ret" => Seq()
      case "@uvm.irbuilder.new_throw" => Seq()
      case "@uvm.irbuilder.new_extractvalue" => Seq()
      case "@uvm.irbuilder.new_insertvalue" => Seq()
      case "@uvm.irbuilder.new_extractelement" => Seq()
      case "@uvm.irbuilder.new_insertelement" => Seq()
      case "@uvm.irbuilder.new_shufflevector" => Seq()
      case "@uvm.irbuilder.new_new" => Seq()
      case "@uvm.irbuilder.new_newhybrid" => Seq()
      case "@uvm.irbuilder.new_alloca" => Seq()
      case "@uvm.irbuilder.new_allocahybrid" => Seq()
      case "@uvm.irbuilder.new_getiref" => Seq()
      case "@uvm.irbuilder.new_getfieldiref" => Seq()
      case "@uvm.irbuilder.new_getelemiref" => Seq()
      case "@uvm.irbuilder.new_shiftiref" => Seq()
      case "@uvm.irbuilder.new_getvarpartiref" => Seq()
      case "@uvm.irbuilder.new_load" => Seq()
      case "@uvm.irbuilder.new_store" => Seq()
      case "@uvm.irbuilder.new_cmpxchg" => Seq()
      case "@uvm.irbuilder.new_atomicrmw" => Seq()
      case "@uvm.irbuilder.new_fence" => Seq()
      case "@uvm.irbuilder.new_trap" => Seq()
      case "@uvm.irbuilder.new_watchpoint" => Seq()
      case "@uvm.irbuilder.new_wpbranch" => Seq()
      case "@uvm.irbuilder.new_ccall" => Seq()
      case "@uvm.irbuilder.new_newthread" => Seq()
      case "@uvm.irbuilder.new_swapstack" => Seq()
      case "@uvm.irbuilder.new_comminst" => Seq()
      /// GEN:END:IRBUILDER_RETVALS
    }
  }
}
