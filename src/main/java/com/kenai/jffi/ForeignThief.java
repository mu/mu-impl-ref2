package com.kenai.jffi;

/**
 * Work-around the problem that com.kenai.jffi.Library sometimes get prematurely
 * finalized. So we open and close libs without relying on GC.
 */
public class ForeignThief {
	public static final int LAZY = Foreign.RTLD_LAZY;
	public static final int NOW = Foreign.RTLD_NOW;
	public static final int LOCAL = Foreign.RTLD_LOCAL;
	public static final int GLOBAL = Foreign.RTLD_GLOBAL;

	public static long dlopen(String name, int flags) {
		return Foreign.dlopen(name, flags);
	}
	
    public static void dlclose(long handle) {
    	Foreign.dlclose(handle);
    }

    public static long dlsym(long handle, String name) {
    	return Foreign.dlsym(handle, name);
    }
}