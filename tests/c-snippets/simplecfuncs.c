#include<stdio.h>
#include<errno.h>

int hello_world(int n) {
    printf("Hello world from C!\n");
    return n + 1;
}

void set_errno(int n) {
    printf("Hi! I am about to set errno to %d\n", n);
    errno = n;
}

int get_errno() {
    return errno;
}
