"""
IRBuilder is no longer part of MuCtx. Don't use this.
"""

import re
import sys, os.path, io

import injecttools
from refimpl2injectablefiles import injectable_files

begin = "SCRIPT: BEGIN HERE"
end = "SCRIPT: END HERE"

replaces = [(re.compile(x), y) for (x,y) in [
    (r'TrantientBundle', 'MuBundleNode'),
    (r'TB', 'MuBundleNode'),
    (r'Bundle', 'MuBundleNode'),
    (r'TopLevel', 'MuChildNode'),
    (r'ChildNode', 'MuChildNode'),
    (r'Type\w*', 'MuTypeNode'),
    (r'Abstract\w+Type', 'MuTypeNode'),
    (r'FuncSig', 'MuFuncSigNode'),
    (r'Const\w+', 'MuConstNode'),
    (r'GlobalCell', 'MuGlobalNode'),
    (r'Function', 'MuFuncNode'),
    (r'ExposedFunc', 'MuExpFuncNode'),
    (r'FuncVer', 'MuFuncVerNode'),
    (r'BasicBlock', 'MuBBNode'),
    (r'BB', 'MuBBNode'),
    (r'SSAVariable', 'MuVarNode'),
    (r'Var', 'MuVarNode'),
    (r'LocalVariable', 'MuLocalVarNode'),
    (r'NorParam', 'MuNorParamNode'),
    (r'ExcParam', 'MuExcParamNode'),
    (r'InstResult', 'MuInstResNode'),
    (r'Inst\w+', 'MuInstNode'),
    (r'HasKeepaliveClause', 'MuInstNode'),
    ]]

sig = re.compile(r'^(  def\s+(\w+)\s*\(([^)]*)\):\s+(\w+)\s+=)', re.MULTILINE)
arg = re.compile(r'(\w*):\s*([a-zA-Z0-9\[\].]+)')
node_like = re.compile(r'Mu\w+Node')
node_seq_like = re.compile(r'Seq\[(Mu\w+Node)\]')

_my_dir = os.path.dirname(__file__)
_refimpl2_root = os.path.join(_my_dir, "..")
irbuilder_path = os.path.join(_refimpl2_root, "src/main/scala/uvm/ir/irbuilder/IRBuilder.scala")

## No longer replace names, but just use MuIRNode[...]

#def rewrite_type(ty):
#    for p, t in replaces:
#        oldty = ty
#        ty = p.sub(t, ty)
#        if oldty != ty:
#            break
#    return ty

container_ty_r = re.compile(r'(Seq|Option)\[(.+)\]')

def rewrite_type(ty):
    m = container_ty_r.match(ty)
    if m is not None:
        t1, t2 = m.groups()
        return "{}[{}]".format(t1, rewrite_type(t2))
    for p, t in replaces:
        if p.match(ty) is not None:
            return "MuIRNode[{}]".format(ty)

    return ty

def main():
    with open(irbuilder_path) as f:
        lines = f.read()

    text = injecttools.extract_lines(lines, begin, end)

    #for p, t in replaces:
        #text = p.sub(t, text)

    output = io.StringIO()

    for whole, name, arglist, retty in sig.findall(text):
        argnames = []
        argtypes_muctx = []
        sanitisers = []

        retty = rewrite_type(retty)

        for an,at in arg.findall(arglist):
            rwt = rewrite_type(at)
            print(an, at, rwt)
            argnames.append(an)
            argtypes_muctx.append(rwt)
            m = node_seq_like.match(rwt)
            if m is not None:
                sanitisers.append('    for((n,i) <- {}.zipWithIndex) require(!n.isNull, "{}[%d] must not be NULL".format(i))'.format(an, an))
            elif node_like.match(rwt) is not None:
                sanitisers.append('    require(!{}.isNull, "{} must not be NULL")'.format(an, an))

        new_arglist = ", ".join("{}: {}".format(n,t) for n,t in zip(argnames, argtypes_muctx))

        print("  def {}({}): {} = {{".format(name, new_arglist, retty), file=output)
        for s in sanitisers:
            print(s, file=output)
        if name.startswith("new") or name in["getNode", "getInstRes"]:
            print('    nodeToHandle(irBuilder.{}({}))'.format(name, ", ".join(argnames)), file=output)
        else:
            print('    irBuilder.{}({})'.format(name, ", ".join(argnames)), file=output)
        print("  }", file=output)
        print(file=output)
        #print(whole, name, args)
        #for n,a in sig.findall(line):
            #args = arg_name.findall(a)
            #print("    addHandle(irBuilder.{}({}))".format(n, ", ".join(args)))

    #print(output.getvalue())

    #return

    injectable_files["MuCtxIRBuilderPart.scala"].inject_many({
        "METHODS": output.getvalue(),
        })
            
if __name__=='__main__':
    main()

