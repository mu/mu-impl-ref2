import injecttools
import os.path

_my_dir = os.path.dirname(__file__)
_refimpl2_root = os.path.join(_my_dir, "..")

def _make_injectable_file_set(m):
    m2 = {os.path.join(_refimpl2_root, k): v for k,v in m.items()}
    return InjectableFileSet(m2)

muapi_h_path = os.path.join(_refimpl2_root, "cbinding/muapi.h")
irbuilder_nodes_path = os.path.join(_refimpl2_root, "src/main/scala/uvm/ir/irbuilder/irBuilderNodes.scala")

injectable_files = injecttools.make_injectable_file_set(_refimpl2_root, [
    ("cStubs.scala", "src/main/scala/uvm/refimpl/nat/cStubs.scala",
        ["STUBS"]),
    ("libmu.py", "pythonbinding/libmu.py",
        ["CTYPES", "CENUMS", "MUVALUE", "MuVM", "MuCtx", "MuIRBuilder",
            "wrp_MuIRBuilder"]),
    ("comminsts.scala", "src/main/scala/uvm/comminsts/comminsts.scala",
        ["IRBUILDER_COMMINSTS"]),
    ("TypeInferer.scala", "src/main/scala/uvm/staticanalysis/TypeInferer.scala",
        ["IRBUILDER_RETVALS"]),
    ("InstructionResultInferer.scala", "src/main/scala/uvm/staticanalysis/InstructionResultInferer.scala",
        ["IRBUILDER_RETVAL_NUMS"]),
    ("ir-ci-exec", "src/main/scala/uvm/refimpl/itpr/IRBuilderCommInstExecutor.scala",
        ["IRBUILDER_IMPL"]),
    ("MuCtxIRBuilderPart.scala", "src/main/scala/uvm/refimpl/MuCtxIRBuilderPart.scala",
        ["METHODS"]),
    ("IRBuilder.scala", "src/main/scala/uvm/ir/irbuilder/IRBuilder.scala",
        ["IRBUILDERNODE_CONSTRUCTORS"]),
    ])
