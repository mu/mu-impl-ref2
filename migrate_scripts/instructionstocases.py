"""
Generate match cases to match against instructions.
"""

from refimpl2injectablefiles import _refimpl2_root
import injecttools

import os.path, sys
import re

case_class_r = re.compile(r'case class (\w+)\(([^)]*)\)', re.MULTILINE)

param_r = re.compile(r'va[lr]\s+(\w+)\s*:\s*([a-zA-Z0-9\[\]_]+)', re.MULTILINE)

ssavariables_path = os.path.join(_refimpl2_root, "src/main/scala/uvm/ssavariables/ssavariables.scala")

def main():
    with open(ssavariables_path) as f:
        txt = f.read()

    txt = injecttools.extract_lines(txt, "EXTRACT:BEGIN:INSTS", "EXTRACT:END:INSTS")

    for name, params in case_class_r.findall(txt):
        pns = []
        for pn, pt in param_r.findall(params):
            pns.append(pn)

        print("case {}({}) => ".format(name, ", ".join(pns)))

if __name__=="__main__":
    main()
