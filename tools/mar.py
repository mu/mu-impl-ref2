#!/usr/bin/env python3

import argparse
import zipfile
import tempfile
import shutil


def mu_meta_set(bootimg, extra_libraries=None):
    with tempfile.NamedTemporaryFile(delete=False) as tf:
        tmpfilename = tf.name
        with zipfile.ZipFile(tf, 'w', compression=zipfile.ZIP_DEFLATED) as zo:
            with zipfile.ZipFile(bootimg, "a") as zi:
                il = zi.infolist()

                metainfo = ""

                for info in il:
                    name = info.filename
                    if name == 'metainfo':
                        metainfo = zi.read(info).decode('utf-8')
                    else:
                        oldcontent = zi.read(info)
                        zo.writestr(name, oldcontent)

                dic = {}
                for line in metainfo.splitlines():
                    if '=' in line:
                        key, val = line.split('=', 1)
                        dic[key] = val

                if extra_libraries is not None:
                    dic['extralibs'] = extra_libraries

                new_metainfo = '\n'.join("{}={}".format(k,v) for k,v in dic.items()).encode('utf8')

                zo.writestr("metainfo", new_metainfo)
    
    shutil.move(tmpfilename, bootimg)

        
if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='''Set additional boot-image metadata.''',
        epilog='''Example:
    ./tools/mar.py -l libm.so:/path/to/mylib.so my-boot-img.muref
    '''
    )

    entry_point_group = parser.add_mutually_exclusive_group()

    parser.add_argument('-l', '--extra-libraries',
                        metavar="colon-separated-libs",
                        help='''extra libraries to load at boot-image loading time. It is a
            colon-separated list of libraries.''')

    parser.add_argument('bootimg', help='path to the boot image')

    args = parser.parse_args()
    
    mu_meta_set(args.bootimg, args.extra_libraries)